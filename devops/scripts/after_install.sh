#!/bin/bash

echo 'Nothing to do'

sudo find /home/ubuntu/bisavoto -type f -exec chmod 664 {} \;   
sudo find /home/ubuntu/bisavoto -type d -exec chmod 775 {} \;
sudo chmod -R 777 /home/ubuntu/bisavoto/public/uploads
sudo chown -R ubuntu:ubuntu /home/ubuntu/bisavoto
sudo find /home/ubuntu/bisavoto -type f -exec touch {} +

cd /home/ubuntu/nginx
sudo docker compose down
sudo docker compose up -d --build