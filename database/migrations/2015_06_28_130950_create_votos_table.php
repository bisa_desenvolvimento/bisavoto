<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('votos', function(Blueprint $table)
		{
            $table->increments('vot_sequencial');
            $table->increments('zon_id');
            $table->increments('ele_id');
            $table->increments('cdt_id');
            $table->increments('car_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('votos');
	}

}
