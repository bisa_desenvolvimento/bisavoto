@extends(Session::get('dataUser')->profile_id == 2 ? 'comissao' : 'admin')
@section('content')
<!-- @if(Session::get('dataUser')->profile_id == 2)
    <a href="{{route('add.candidato.comissao',[ $car_id] )}}" class="btn btn-primary right" style="margin-right: 20px">Adicionar Candidato</a>
@else
    <a href="{{route('add.candidato',[$car_id, $ele_id] )}}" class="btn btn-primary right" style="margin-right: 20px">Adicionar Candidato</a>
@endif -->

@if(Session::get('dataUser')->profile_id == 1)
    <a href="{{route('add.candidato',[$car_id, $ele_id] )}}" class="btn btn-primary right" style="margin-right: 20px">Adicionar Candidato</a>
@endif

<table class="table table-striped" style="margin: 0 50px 0 20px; width: 96%">
    <thead>
        <tr>
            <th>Nome</th>
            @if(Session::get('dataUser')->profile_id == 1)
                <th>Ações</th>
            @endif
        </tr>
    </thead>
    @if (!$listaCandidatos->count())
    <tr>
        <td colspan="3">Nenhum registro cadastrado</td>
    </tr>
  @else
      @foreach( $listaCandidatos as $objCandidato )
          <tr>
            <td>{{$objCandidato->cdt_nome}}</td>
            <!-- <td>
                @if(Session::get('dataUser')->profile_id == 2)
                    <a href="{{ route('edit.candidato.comissao',[$car_id, $objCandidato->cdt_id])}}" class="glyphicon glyphicon-pencil"></a>
                    <a href="{{ route('delete.candidato.comissao',[$car_id,$objCandidato->cdt_id])}}" class="glyphicon glyphicon-remove"></a>
                @else
                    <a href="{{ route('edit.candidato',[$car_id, $objCandidato->cdt_id, $ele_id])}}" class="glyphicon glyphicon-pencil"> </a>
                    <a href="{{ route('delete.candidato',[$car_id, $objCandidato->cdt_id, $ele_id])}}" class="glyphicon glyphicon-remove"></a>
                @endif
            </td> -->
            @if(Session::get('dataUser')->profile_id == 1)
                <td>
                    <a href="{{ route('edit.candidato',[$car_id, $objCandidato->cdt_id, $ele_id])}}" class="glyphicon glyphicon-pencil"> </a>
                    <a href="{{ route('delete.candidato',[$car_id, $objCandidato->cdt_id, $ele_id])}}" class="glyphicon glyphicon-remove"></a>
                </td>
            @endif
            
          </tr>
     @endforeach
 @endif
</table>

@endsection