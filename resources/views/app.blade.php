@include('templates.partials.headerhome')

<noscript>
    <style>
        .content-wrapper { display: none; }
    </style>
    <div style="background: #f8d7da; color: #721c24; padding: 20px; text-align: center; font-weight: bold; font-size: 1rem;">
        <p>O JavaScript precisa estar ativado para você usar este site. Por favor, habilite o JavaScript em seu navegador.</p>
    </div>
</noscript>

<div class="content-wrapper">
    @yield('content')
</div>

@include('templates.partials.footerhome')
