@extends(!is_null($ele_id) ? 'comissao' : 'admin')
@section('content')
<div class="content-admin">
    <h3>Adicionar Comissão Eleitoral / Mesa</h3>
    <form class="form-vertical" method="post" action="{{route(empty($id) ? 'salvar.comissao.comissao' : 'update.comissao.comissao')}}">
      <input type="hidden" name="_token" value="{{csrf_token()}}">
      <input type="hidden" name="ele_id" value="{{$ele_id}}">
      <input type="hidden" name="perfil_id" value="2">
      <input type="hidden" name="id" value="{{$id}}">

      @if(Session::has('msgErro'))
        <div class="alert alert-danger">
            {{Session::get('msgErro')}}
       </div>
      @endif

      <div class="form-group">
          
        <div class="col-sm-10">
            <input type="text" class="form-control" id="name" name="name" value="{{$name}}" placeholder="Nome*" required="required">
        </div>
      </div>
<br class="clear" />

<div class="glyphicon glyphicon-info-sign"> <b>Sugestão para o login</b></div>

      <div class="form-group">
        
        <div class="col-sm-10">
          <input type="text" id="login" class="form-control" name="login" value="{{$login}}" placeholder="Login* (Sugestão: CPF)" required="required">
        </div>
      </div>

<br class="clear" />

   <div class="form-group">    
        <div class="col-sm-10">
          <input  type="password" id="senha" class="form-control" name="senha" value="" placeholder="Senha*" required="required" maxlength="6">
        </div>
      </div>

      <div class="col-sm-10">
        <br class="clear" />
        <button type="submit" class="btn btn-default">Salvar</button>
      </div>

    </form>

</div>
@endsection