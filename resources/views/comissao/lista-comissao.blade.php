@extends(Session::get('dataUser')->profile_id == 2 ? 'comissao' : 'admin')
@section('content')

{{-- Session::get('dataUser')->profile_id --}}
<?php 
$idEleicao = $ele_id;
?>

@if( Session::get('dataUser')->profile_id == 2 )
    <a href="{{ route('add.comissao.comissao') }}" class="btn btn-primary right" style="margin-right: 20px">Adicionar Comissao / Mesa</a>
@else
    <a href="{{ route('add.comissao', [$idEleicao]) }}" class="btn btn-primary right" style="margin-right: 20px">Adicionar Comissao / Mesa</a>
@endif

<table class="table table-striped" style="margin: 0 50px 0 20px; width: 96%">
    <thead>
        <tr>
            <th>Nome</th>
            <th>Ações</th>
        </tr>
    </thead>
     {{-- This comment will not be in the rendered HTML --}}
  @if ( !$listaComissao)
    <tr>
        <td colspan="3">Nenhum Usuário da Comissão eleitoral cadastrada</td>
    </tr>
  @else
      @foreach( $listaComissao as $objComissao )
          <tr>
            <td>{{$objComissao->name}}</td>
            <td>
                @if(Session::get('dataUser')->profile_id == 2)
                    <a href="{{ route('edit.comissao.comissao',[$idEleicao])}}" class="glyphicon glyphicon-pencil"></a>
                    <a href="{{ route('delete.comissao.comissao',[$idEleicao])}}" class="glyphicon glyphicon-remove"></a>
                @else
                    <a href="{{ route('edit.comissao',[$idEleicao, $objComissao->id])}}" class="glyphicon glyphicon-pencil"></a>
                    <a href="{{ route('delete.comissao',[$idEleicao, $objComissao->id])}}" class="glyphicon glyphicon-remove"></a>
                @endif
            </td>
          </tr>
     @endforeach
 @endif
</table>

@endsection