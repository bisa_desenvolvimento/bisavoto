@extends(Session::get('dataUser')->profile_id == 2 ? 'comissao' : 'admin')
@section('content')

<?php
	$today = strtotime(date('Y-m-d H:i:s'));
	$ele_horaTermino = strtotime(Carbon::createFromFormat('Y-m-d H:i:s', $objEleicao->ele_horaTermino));
    $diff = ($today - $ele_horaTermino) /(3600*24);
?>

    <div>

        <ul class="apuracaofinal-lista nav nav-pills">
        	
        	@if(strtotime($objEleicao->ele_horaInicio) > $today && $diff < 0)
            	<li role="presentation"></span><a target="_blank" href="{{route('eleicao.emitir.zeresima',[$objEleicao->ele_id])}}">Emitir Zerésima</a></li>
            @else
                @if($today > strtotime($objEleicao->ele_horaInicio) && $objEleicao->zeresima == 0 && $diff < 0)
                    <li id="linkEmissaoZeresima" role="presentation"></span><a target="_blank" href="{{route('eleicao.emitir.zeresima',[$objEleicao->ele_id])}}" onclick="ocultarLink()">Emitir Zerésima</a></li>
                @endif
            @endif

            <li role="presentation"></span><a target="_blank" href="{{route('resumo.relatorio.aptos',[$objEleicao->ele_id])}}">Emitir relatório de Aptos a Votar</a></li>
            <li role="presentation"></span><a target="_blank" href="{{route('resumo.relatorio.aptos.zona',[$objEleicao->ele_id])}}">Emitir relatório de Aptos a Votar por Zona</a></li>

            @if($diff >= 0)
            	<li role="presentation"></span><a target="_blank" href="{{route('resumo.apuracao.eleicao.comissao')}}/idEleicao/{{ $objEleicao->ele_id }}">Emitir relatório final da eleição</a></li>

                <li role="presentation"></span><a target="_blank" href="{{route('resumo.relatorio.apuracaofinalporzona',[$objEleicao->ele_id])}}">Emitir relatório final da eleição por Zona</a></li>

                <li role="presentation"></span><a target="_blank" href="{{route('resumo.relatorio.votantes.final',[$objEleicao->ele_id])}}">Emitir relatório de votantes</a></li>

                <li role="presentation"></span><a target="_blank" href="{{route('resumo.relatorio.votantes.zona',[$objEleicao->ele_id])}}">Emitir relatório de votantes por Zona</a></li>
                <li role="presentation"></span><a target="_blank" href="{{route('resumo.relatorio.ocorrencias',[$objEleicao->ele_id])}}">Emitir relatório de ocorrências</a></li>
                <li role="presentation"></span><a target="_blank" href="{{route('resumo.relatorio.email_alterados',[$objEleicao->ele_id])}}">Exportar eleitores com e-mails alterados</a></li>

            @endif

        	@if(strtotime($objEleicao->ele_horaInicio) < $today && $diff < 0)
            	<li role="presentation"></span><a target="_blank" href="{{route('resumo.relatorio.votantes',[$objEleicao->ele_id])}}">Emitir relatório parcial de votantes</a></li>
                <li role="presentation"></span><a target="_blank" href="{{ route('resumo.relatorio.resultadoparcial', [$objEleicao->ele_nome, $objEleicao->ele_id]) }}">Painel de Votação</a></li>
            @endif
        </ul>

    </div>

@endsection