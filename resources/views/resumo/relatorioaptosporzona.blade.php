@extends('resumo')
@section('content')
<style>
	body {
		font-family: Helvetica, sans-serif;
		font-weight: normal;
    	font-style: normal;
	}

	.text-center {
		text-align: center;
		margin-bottom: 30px;
	}

	.table {
		width: 100%;
	}

	.table .col {
		padding: 5px;
	}

	.table thead {
		background: #ccc;
	}

	tr:nth-child(even) {
		background-color: #f2f2f2;
	}

</style>

<div class="text-center">
	<h1>Lista de Aptos para {{ $eleicao->ele_nomenclatura }} por Zona</h1>

	<div id="logoPrincipal2">
		<?php
			$url = url('img/logo/nova-marca-cremepe.png');
			if(isset($eleicao) && $eleicao->ele_logo) {
				$url = url($eleicao->ele_logo);
			}

		?>
		<img src="{{ $url }}" width="180" alt=""  />

	</div><!-- /logo -->
</div>

@foreach ($zonas as $zona)
	<label><b>ZONA: {{ $zona->zon_nome }}</b></label>
	<div class="text-center">
		<table class="table table-striped" cellspacing="0" cellpadding="0">
			<thead>
				<tr>
				<th class="col">Matrícula</th>
				<th class="col" style="text-align: left;">Nome</th>
				<th class="col" style="text-align: left;">Email</th>
				</tr>
			</thead>
			<tbody>
				@foreach($zona->aptos as $apto)
					<tr>
						<th class="row">{{ $apto->matricula }}</th>
						<td style="text-align: left;">{{ $apto->nome }}</td>
						<td style="text-align: left;">{{ $apto->email }}</td>
					</tr>
				@endforeach
				<tr>
					<td></td>
					<th class="row" style="text-align: left;">TOTAL DE APTOS</th>
					<th class="text-center" style="text-align: left;">{{ count($zona->aptos) }}</th>
				</tr>
			</tbody>
		</table>
	</div>
@endforeach
@endsection