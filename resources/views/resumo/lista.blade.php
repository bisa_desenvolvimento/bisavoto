@extends(Session::get('dataUser')->profile_id == 2 ? 'comissao' : 'admin')
@section('content')

    @if($data)
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
        <div class="row" style="text-align: center">
        <?php
          $geral_eleitores = 0;
          $geral_votos = 0;
        ?>
        @foreach($data as $id => $votos)
              <?php
                  $geral_eleitores += $votos->eleitores;
                  $geral_votos += $votos->votos;
              ?>
              <div class="col-xs-6 col-md-3" style="width: 250px;">
                <div class="caption">{{$votos->zona}}</div>
                <div id="zona{{$id}}" class="zona" style="height: 200px; margin-bottom: 30px; padding: 0 20px"></div>
              </div>
              <script type="text/javascript">
                new Morris.Donut({
                  // ID of the element in which to draw the chart.
                  element: 'zona<?php echo $id;?>',
                  // Chart data records -- each entry in this array corresponds to a point on
                  // the chart.
                  data: [
                      {value: '<?php echo $votos->eleitores;?>', label: 'Eleitores', formatted: '<?php echo $votos->eleitores;?>'},
                      {value: '<?php echo $votos->votos;?>', label: 'Votos', formatted: '<?php echo $votos->votos.'('. round((($votos->votos / $votos->eleitores) * 100), 2);?>%)'},
                    ],
                    backgroundColor: '#ccc',
                    labelColor: '#060',
                    colors: [
                      '#ec3c01',
                      '#f66838'
                    ],
                    formatter: function (x, data) { return data.formatted; }
                });
            </script>
            @if($id % 3 == 0 && $id > 0)
                <br class="clear" />
            @endif
        @endforeach
             <div class="col-xs-6 col-md-3" style="width: 250px;">
               <div class="caption">Total Zonas</div>
               <div id="zona_geral" class="zona" style="height: 200px; margin-bottom: 30px; padding: 0 20px"></div>
             </div>
                <script type="text/javascript">
                new Morris.Donut({
                  // ID of the element in which to draw the chart.
                  element: 'zona_geral',
                  // Chart data records -- each entry in this array corresponds to a point on
                  // the chart.
                  data: [
                      {value: '<?php echo $geral_eleitores;?>', label: 'Eleitores', formatted: '<?php echo $geral_eleitores;?>'},
                      {value: '<?php echo $geral_votos;?>', label: 'Votos', formatted: '<?php echo $geral_votos.'('. round((($geral_votos / $geral_eleitores) * 100), 2);?>%)'},
                    ],
                    backgroundColor: '#ccc',
                    labelColor: '#060',
                    colors: [
                      '#ec3c01',
                      '#f66838'
                    ],
                    formatter: function (x, data) { return data.formatted; }
                });
            </script>
        </div>
        <script type="text/javascript">
            setTimeout("reloadPage()", 600000);
        </script>
    @endif
@endsection