
<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <title>
        @if(isset($eleicao->ele_nome))
            {{"Painel de Votação - " . $eleicao->ele_nome }}
        @else
            {{ "Painel de Votação - " . config('messages.app_title_default') }}
        @endif
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="icon" type="image/png" href="{{ secure_asset('img/logo/logoBisa.png') }}">
    <script type="text/javascript" src="{{secure_asset('js/jquery-1.8.0.min.js')}}"></script>
	<script type="text/javascript" src="{{secure_asset('js/sawpf.1.0.js')}}"></script>
    <script type="text/javascript" src="{{secure_asset('js/jquery-ui-1.11.4/jquery-ui.min.js')}}"></script>
    <script type="text/javascript" src="{{secure_asset('js/lightbox/ekko-lightbox.min.js')}}"></script>

     <script type="text/javascript" src="{{secure_asset('js/actions.js')}}"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
</head>
<body>

    <section class="container-fluid text-center p-4 d-flex justify-content-center align-items-center" style="flex-direction: column; background-color: #E6E5ED; min-height: 28vh;">
        <h3>{{ $eleicao->ele_nomenclatura }}</h3><br><br>
        <div id="logoPrincipal2">
            <?php
                $url = url('img/logo/nova-marca-cremepe.png');
                if(isset($eleicao) && $eleicao->ele_logo) {
                    $url = url($eleicao->ele_logo);
                }

            ?>
            <img src="{{ $url }}" width="180" height="180" style="margin-bottom: 20px; object-fit: contain;" alt="logo" />
        </div>
    </section>

    <section class="container-fluid  text-justify p-5 " style="background-color:#9eabac">
        <div class="d-flex justify-content-center align-items-center h-100 ">
            <div class="text-center col-4">
                <div class="row mb-3 align-items-center">
                    <label class="col-sm-8 col-form-label text-uppercase fs-3 fw-bold">Aptos a Votar</label>
                    <div class="col-sm-4 d-flex align-items-center">
                    <span class="form-control-sm col-8" style="background-color:#FFFFFF; padding: 6px;">
                {{$totalAptos }}
            </span>
                    </div>
                </div>
                <div class="row mb-3 align-items-center">
                    <label class="col-sm-8 col-form-label text-end text-uppercase fs-3 fw-bold">Total de Votantes</label>
                    <div class="col-sm-4 d-flex align-items-center">
                        <span class="form-control-sm col-8" style="background-color:#FFFFFF; padding: 6px;">
                            {{$totalVotantes}}
                        </span>
                    </div>
                </div>

                <div class="row mb-3 align-items-center justify-content-center">
                    <label for="inputPassword3" class="col-sm-12 col-sm-offset-2 col-form-label text-center fs-5">Posição em {{$dataAtual}} às {{$horaAtual}}</label>
                </div>
            </div>
        </div>
    </section>
    <section class="container-fluid  text-justify p-5" style="background-color: #E6E5ED;  flex: 1 0 auto;">
        <div class="d-flex justify-content-center align-items-center flex-column">
            <button type="button" class="btn btn-danger mb-3 btn-lg  fs-5" id="btnAtualizar">ATUALIZAR</button>
        </div>
        <div style="display: flex; justify-content: flex-end; margin-right: 60px; margin-top: -60px;">
            <img src="{{ asset('img/logoBisa1.png') }}" style="max-width: 150px; margin-top: 20px">
        </div>
    </section>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>

    <script>
        $("#btnAtualizar").click(
            function()
            {
               
                location.reload();
            }
        )
    </script>
</body>
</html>
