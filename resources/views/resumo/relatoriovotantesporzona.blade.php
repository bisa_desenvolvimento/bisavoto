@extends('resumo')
@section('content')
<style>
	body {
		font-family: Helvetica, sans-serif;
		font-weight: normal;
    	font-style: normal;
	}

	.text-center {
		text-align: center
	}

	.table {
		width: 100%;
		margin-bottom: 50px;
	}

	.table .col {
		padding: 5px;
	}

	.table thead {
		background: #ccc;
	}

	tr:nth-child(even) {
		background-color: #f2f2f2;
	}

</style>

<div class="text-center">
	<h1>Lista de votantes da {{ $eleicao->ele_nomenclatura }}</h1>

	<div id="logoPrincipal2">
		<?php
			$url = url('img/logo/nova-marca-cremepe.png');
			if(isset($eleicao) && $eleicao->ele_logo) {
				$url = url($eleicao->ele_logo);
			}

		?>
		<img src="{{ $url }}" width="180" alt=""  />

	</div><br><!-- /logo -->
</div>

@foreach($votantesPorZona as $zona => $votantes)
	<div class="text-center">
		<h3 class="text-center"></h3>
		<table class="table table-striped" cellspacing="0" cellpadding="0">
		  <thead>
		    <tr>
		      <th class="col" colspan="2" style="text-align: left;">{{ $zona }}</th>
		    </tr>
		    <tr>
		      <th class="col" style="text-align: left;">Nome</th>
		      <th class="col" style="text-align: left;">Zona</th>
		    </tr>
		  </thead>
		  <tbody>
		  	@foreach($votantes as $votante)
		    <tr>
		      <td class="col" style="text-align: left;">{{ $votante->Nome }}</td>
		      <td class="col text-center" style="text-align: left;">{{ $votante->Zona }}</td>
		    </tr>
		    @endforeach
		    <tr>
		    	<th class="row" style="text-align: left;">TOTAL DE VOTANTES DESSA ZONA</th>
		    	<th class="text-center" style="text-align: left;">{{ count($votantes) }}</th>
		    </tr>
		  </tbody>
		</table>
	</div>
@endforeach
@endsection