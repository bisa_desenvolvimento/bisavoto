@extends('resumo')
@section('content')
<style>
	body {
		font-family: Helvetica, sans-serif;
		font-weight: normal;
    	font-style: normal;
	}

	.text-center {
		text-align: center
	}

	.table {
		width: 100%;
	}

	.table .col {
		padding: 5px;
	}

	.table thead {
		background: #ccc;
	}

	tr:nth-child(even) {
		background-color: #f2f2f2;
	}

</style>

<?php //dd($votantes); ?>

<div class="text-center">
	<h1>Lista de votantes da {{ $eleicao->ele_nomenclatura }}</h1>
	<!-- <h2>{{ $eleicao->ele_nome }}</h2> -->

	<div id="logoPrincipal2">
		<?php
			$url = url('img/logo/nova-marca-cremepe.png');
			if(isset($eleicao) && $eleicao->ele_logo) {
				$url = url($eleicao->ele_logo);
			}

		?>
		<img src="{{ $url }}" width="180" alt=""  />

	</div><br><!-- /logo -->
</div>

<div class="text-center">
	<table class="table table-striped" cellspacing="0" cellpadding="0">
	  <thead>
	    <tr>
	      <th class="col" style="text-align: left;">Nome</th>
	      <th class="col" style="text-align: left;">Zona</th>
	    </tr>
	  </thead>
	  <tbody>
	  	@foreach($votantes as $votante)
	    <tr>
	      <td style="text-align: left;">{{ $votante->nome }}</td>
	      <td style="text-align: left;">{{ $votante->zona }}</td>
	    </tr>
	    @endforeach
	    <tr>
	    	<th class="row" style="text-align: left;">TOTAL DE VOTANTES</th>
	    	<th class="text-center" style="text-align: left;">{{ count($votantes) }}</th>
	    </tr>
	  </tbody>
	</table>
</div>
@endsection