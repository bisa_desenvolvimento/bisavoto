@extends(Session::get('dataUser')->profile_id == 2 ? 'comissao' : 'admin')

@section('content')

    <div id="divImportarArquivo" class="fileupload" style="">

        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <script type="text/javascript">
                    $(document).ready(function() {
                        modalErro('alert', '{{ $error }}');
                    });
                </script>
            @endforeach
        @endif

        <fieldset>
            <div class="col-md-12">
                <div class="col-md-4">
                    <div class="col-md-12">
                        <legend>Importar Eleitores</legend>
                    </div>
                    <form action="{{ route('importar2.arquivo.eleitores') }}" files="true" method="POST"
                        enctype="multipart/form-data">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="profile_id" value="3">
                        <input type="hidden" name="ele_id" value="{{ $ele_id }}">

                        <div class="form-group">
                            <label for="arquivoCsv">Selecione o arquivo de importação</label>
                            <input type="file" required="required" class="form-control" name="arquivoCsv" id="arquivoCsv"
                                accept=".csv" />
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary" type="submit" id="fazer_consulta">Enviar</button>
                        </div>
                    </form>
                    <p class="text-primary">*Ordem dos cabeçalhos para importação: </p>
                    <p> zona eleitoral | nome do eleitor | cpf | matricula | email | telefone </p>
                </div>
                <div class="col-md-8">
                    <fieldset>
                        <legend>Arquivos Importados</legend>
                        <div class="col-md-12" style="height: 200px; overflow-y: scroll;">
                            <table class="table table-hover">
                                <thead>
                                    <th style="width: 30%">Nome</th>
                                    <th class="text-center">Data</th>
                                    <th class="text-center">Status</th>
                                    <th width="10%"></th>
                                </thead>
                                <tbody>
                                    @foreach ($arquivos as $arquivo)
                                        <tr>
                                            <td>{{ $arquivo->nome }}</td>
                                            <td class="text-center">{{ $arquivo->created_at->format('d/m/Y H:i:s') }}</td>
                                            <td class="{{ $arquivo->getStatusLabelColor() }} text-center">
                                                {{ $arquivo->getStatusLabel() }}
                                            </td>
                                            <td class="text-right">
                                                @if ($arquivo->status == 'erro')
                                                    <a href="#" class="btn btn-sm btn-primary"
                                                        onclick="getErrors({{ $arquivo->id }})">
                                                        <span>
                                                            <i class="glyphicon glyphicon-search"></i>
                                                        </span>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </fieldset>
                </div>
            </div>
        </fieldset>



    </div>

    <div id="modalErro" class="novoModal" style="display: none;">
        <div class="sombraErro"></div>
        <div id="alertErro">
            <div id="confirmarErro">
                <p class="nome" id="msg" style="font-size: 30px;margin-bottom: -68px;"></p>
                <div id="erros_list" style="height:200px; overflow-y: scroll;"></div>
                <a href="{{ route('importar.eleitores', [$ele_id]) }}" id="btOK" class="modOkErro modAlertErro"
                    style="display: none;">ok</a>
            </div><!-- /confirmar -->
        </div><!--/alert-->
    </div>

    <script>
        function getErrors(id) {
            $.ajax({
                url: "{{ route('eleitor.importacao.erros') }}",
                data: {
                    file_id: id
                },
                dataType: 'json',
                async: false,
                method: 'GET',
            }).done(function(response) {
                response.data.forEach(element => {
                    console.log(element);
                    $('#erros_list').append(`<p>${element}</p>`);
                });

                $('.novoModal').css('display', 'block');
                $('#msg').text("Lista de Erros");
                $('#btOK').show();
            });
        }

        $('#fazer_consulta').click(function() {
            if ($('input[name=arquivoCsv]').val() != '') {
                $('.novoModal').css('display', 'block');
                $('#msg').text("Estamos importando o arquivo, aguarde.");
            }
        });

        function processarArquivo(processar = true) {
            if (processar == true) {
                $.ajax({
                        url: "{{ route('importar2.arquivo.eleitores.processararquivojax') }}",
                        dataType: 'json',
                        async: false,
                    })
                    .always(function(response) {
                        $('.novoModal').css('display', 'block');
                        $('#msg').text(response.mensagem);
                        $('#erros_list').html('');

                        console.log(response.mensagem);
                        if (response.status == false) {
                            if (response.status == false) {
                                if (response.erro) {
                                    $('#loading').hide();
                                    $('#btOK').show();
                                } else {
                                    window.location = "{{ route('importar2.arquivo.eleitores.resultado') }}";
                                }
                            } else {
                                $('#msg').text(response.mensagem);
                            }
                        } else {
                            processarArquivo(response.status);
                        }

                    });
            }
        }
    </script>


    <?php 
        if(Session::get('leitura_sucesso')) { ?>
    <script>
        $('.novoModal').css('display', 'block');
        $('#msg').text("<?php echo Session::get('leitura_sucesso'); ?>");
        processarArquivo();
    </script>
    <?php   
            Session::forget('leitura_sucesso');
        } ?>

@endsection
