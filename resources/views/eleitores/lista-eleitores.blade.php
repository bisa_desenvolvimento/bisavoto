@extends(Session::get('dataUser')->profile_id == 2 ? 'comissao' : 'admin')
@section('content')
<?php
//$idEleicao = Request::segment(3);
Session::get('dataUser')->profile_id == 2 ? $idEleicao = Session::get('dataUser')->ele_id : $idEleicao = Request::segment(3);
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/1.3.8/FileSaver.js"></script>
<script type="text/javascript" src="{{ url('/js/actionsEleitor.js') }}"></script>

<!-- @if(Session::get('dataUser')->profile_id == 2)
    <a href="javascript:;" url="{{route('notificar.eleitor.comissao', 0)}}" class="btn btn-primary right mail-generate" style="margin-right: 20px">Notificar Eleitores</a>
@else
    <a href="javascript:;" url="{{route('notificar.eleitor', [0, $idEleicao])}}" class="btn btn-primary right mail-generate" style="margin-right: 20px">Notificar Eleitores</a>
@endif -->

<button type="button" class="btn btn-primary right" id="exportacaoEleitores" style="margin-right: 20px" >Exportar Eleitores</button>

<div id="modalErro" class="modalExportacaoEleitores modalTeste" style="display: none;">
    <div class="sombraErro"></div>
    <div id="alertErro">
        <div id="confirmarErro" style="margin-top: 80px">
			<form id="formExportacaoEleitores" action="{{route('exportar.eleitores')}}" method="POST">
				<h2>Exportar Eleitores</h2>
				<hr>
				<p><strong>Escolha os eleitores que deseja exportar entre os que votaram, não votaram ou ambos</strong></p>
				<input type="text" name="ele_id" class="none" value="{{$idEleicao}}"/>
				<input type="text" name="_token" id="_token" class="none" value="{{ csrf_token() }}"/>
				<input type="radio" name="situacaoVotoEleitor" id="situacaoVotoEleitorAmbos" value="ambos" checked >
				<label for="situacaoVotoEleitorAmbos"> Ambos  </label>
				<input type="radio" name="situacaoVotoEleitor" id="situacaoVotoEleitorSim" value="sim">
				<label for="situacaoVotoEleitorSim"> Votaram  </label>
				<input type="radio" name="situacaoVotoEleitor" id="situacaoVotoEleitorNao" value="nao" >
				<label for="situacaoVotoEleitorNao">  Não Votaram</label>
				<div class="modal-footer">
					<input type="submit" class="btn btn-primary center" style="margin-right: 20px" value="Exportar"/>
					<a href="javascript:;" id="cancelarExportacaoEleitores" class="btn btn-default" style="margin-right: 20px">Cancelar</a>
				</div>
			</form>

        </div><!-- /confirmar -->
    </div><!--/alert-->
</div>

@if(Session::get('dataUser')->profile_id == 1)

<button type="button" class="btn btn-primary right" id="mailNotification" style="margin-right: 20px" >Notificar Eleitores</button>

<input type="text" id="url-percentual" class="none" value="{{route('percentual.eleitores.notificados', [$idEleicao])}}"/>
<input type="text" id="url-nao-notificados" class="none" value="{{route('notificar.naonotificados', [$idEleicao])}}"/>
<input type="text" name="_token" id="_token" class="none" value="{{ csrf_token() }}"/>
@endif

<div class="enviarEmail right progress none" style="width:120px; margin-top:7px; margin-right:20px">
<div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
</div>

<script>
	$('#mailNotification').click(()=>{
		$('.modalTeste').show();
	})
	$('#cancelNotification').click(()=>{
		$('.modalTeste').hide();
	})
</script>

<div id="modalErro" class="modalTeste" style="display: none;">
    <div class="sombraErro"></div>
    <div id="alertErro">
        <div id="confirmarErro" style="margin-top: 80px">
			<form >
				<h2>Notificar Eleitores</h2>
				<input id="url" type="hidden" value="{{route('notificar.eleitor', [$idEleicao])}}">
				<hr>
				<p><strong>Seleção do tipo de senha de votação</strong></p>
				<input type="radio" name="optionPassword" id="senhaAlfanumerica" value="senhaAlfanumerica" checked >
				<label for="senhaAlfanumerica">  Senha alfanumérica  </label>
				<input type="radio" name="optionPassword" id="senhaNumerica" value="senhaNumerica" >
				<label for="senhaNumerica">  Senha apenas numérica  </label>

				<hr>

				<p> <strong>Envio de emails</strong> </p>
				<input type="radio" name="optionEmail" id="gerarSenha" value="enviarEmails" checked >
				<label for="enviarEmails">  Gerar senha e despachar e-mails </label>
				<input type="radio" name="optionEmail" id="gerarSenha" value="gerarSenha" >
				<label for="gerarSenha">  Gerar senha e NÃO DESPACHAR e-mails </label>
				<input type="radio" name="optionEmail" id="gerarSenha" value="ambos" >
				<label for="gerarSenha">  Ambos </label>

				<hr>
				
				<p><strong>Envio de SMS</strong></p>
				<input type="radio" name="optionSMS" id="enviarSMS" value="enviarSMS">
				<label for="enviarSMS">Gerar senha e baixar Excel</label>
				<div class="modal-footer">

					<a href="javascript:;" id="confirmarGeracao" class="btn btn-primary center mail-generate" style="margin-right: 20px">Ok</a>
					<button class="btn btn-default" id="cancelNotification">Cancelar</button>

				</div>
			</form>
        </div><!-- /confirmar -->
    </div><!--/alert-->
</div>

<div id="modalErro" class="modalGeracaoNovaSenha" style="display: none;">
    <div class="sombraErro"></div>
    <div id="alertErro">
        <div id="confirmarErro" style="margin-top: 80px">
			<form id="formGeracaoNovaSenha">
				<h2>Gerar Nova Senha e Copiar para Área de Transferência</h2>
				<input id="idEleitor" type="hidden">
				<input type="text" name="_token" id="_token" class="none" value="{{ csrf_token() }}"/>
				<hr>
				<p><strong>Seleção do tipo de senha de votação</strong></p>
				<input type="radio" name="optionPassword" id="senhaAlfanumerica" value="senhaAlfanumerica" checked >
				<label for="senhaAlfanumerica">  Senha alfanumérica  </label>
				<input type="radio" name="optionPassword" id="senhaNumerica" value="senhaNumerica" >
				<label for="senhaNumerica">  Senha apenas numérica  </label>

				<div class="modal-footer">
					<button id="confirmarGeracaoNovaSenhaTextoCorrigido" class="btn btn-primary center" style="margin-right: 20px">Gerar senha e copiar (sem quebra de linha)</button>
					<button id="confirmarGeracaoNovaSenha" class="btn btn-primary center" style="margin-right: 20px">Gerar senha e copiar</button>
					<button class="btn btn-default" id="fecharModalGeracaoNovaSenha">Cancelar</button>
				</div>
			</form>
        </div><!-- /confirmar -->
    </div><!--/alert-->
</div>

<!--div class="enviarEmail right none">
	<img src="{{url('img/loaderPesquisaEleitor.gif')}}" /> Enviando emails de eleitores com a letra <span class="letter_mail">A</span>, aguarde...
</div -->

@if(Session::get('dataUser')->profile_id == 1)
<a href="{{route('importar.eleitores', [$idEleicao])}}" id="importarCsv" class="btn btn-primary right" style="margin-right: 20px">Importar</a>
@endif

<!-- @if(Session::get('dataUser')->profile_id == 2)
    <a href="{{route('add.eleitor.comissao')}}" class="btn btn-primary right" style="margin-right: 20px">Adicionar Eleitor</a>
@else
    <a href="{{route('add.eleitor', [$idEleicao])}}" class="btn btn-primary right" style="margin-right: 20px">Adicionar Eleitor</a>
@endif -->

@if(Session::get('dataUser')->profile_id == 1)
<a href="{{route('add.eleitor', [$idEleicao])}}" class="btn btn-primary right" style="margin-right: 20px">Adicionar Eleitor</a>
@endif

<!-- @if(Session::get('dataUser')->profile_id == 2)
    <a href="javascript:;" url="{{route('carta.eleitor.comissao', 0)}}" class="btn btn-primary right letter-generate" style="margin-right: 20px">Gerar Carta</a>
@else
    <a href="javascript:;" url="{{route('carta.eleitor', [0, $idEleicao])}}" class="btn btn-primary right letter-generate" style="margin-right: 20px">Gerar Carta</a>
@endif -->

<!-- @if(Session::get('dataUser')->profile_id == 1)
<a href="javascript:;" url="{{route('carta.eleitor', [0, $idEleicao])}}" class="btn btn-primary right letter-generate" style="margin-right: 20px">Gerar Carta</a>
@endif

<div class="gerandoCarta right none">
	<img src="{{url('img/loaderPesquisaEleitor.gif')}}" /> Gerando cartas de eleitores com a inicial <span class="letter_pdf">A</span>, aguarde...
</div> -->
@if(Session::get('dataUser')->profile_id == 1 OR Session::get('dataUser')->profile_id == 2)
<form action="" method="get">
	<table class="table table-striped" style="margin: 0 50px 0 20px; width: 96%">
		<thead>
			<tr>
				<td><input type="text" name="busca" class="form-control" value="<?php echo isset($_GET['busca']) ? $_GET['busca'] : ''; ?>" placeholder="Buscar eleitor" /></td>
				<td><button class="btn btn-primary">Buscar</button></td>
			</tr>
		</thead>
	</table>
</form>
@endif
<table class="table table-striped" style="margin: 0 50px 0 20px; width: 96%">
	<thead>
		<tr>
			<th>Nome</th>
			<th>Matrícula</th>
			<th>Votou?</th>
			@if(Session::get('dataUser')->profile_id == 1 OR Session::get('dataUser')->profile_id == 2)
			<th>Ações</th>
			@endif
		</tr>
	</thead>
	@if( !$listaEleitores->count() )
	<tr>
		<td colspan="3">Nenhum Registro cadastrado</td>
	</tr>
	@else
	@foreach( $listaEleitores as $objEleitor )
	<tr>
		<td>{{$objEleitor->name}}</td>
		<td>{{$objEleitor->matricula}}</td>
		<td>{{$objEleitor->vote == 0 ? 'Não' : 'Sim'}}</td>
		<!-- <td>
					<a href="{{route('edit.eleitor.comissao', [$idEleicao, $objEleitor->id])}}" class="glyphicon glyphicon-pencil" title="Editar"></a>
					<a href="{{route('delete.eleitor.comissao',[$idEleicao, $objEleitor->id])}}" class="glyphicon glyphicon-remove" title="Excluir"></a>
					<a href="javascript:" link="{{route('user.resetar.acesso',[$objEleitor->id, $idEleicao])}}" class="glyphicon glyphicon-refresh updateAcess" title="Alterar senha"></a> -->
		<!-- </td> -->
		@if(Session::get('dataUser')->profile_id == 1 OR Session::get('dataUser')->profile_id == 2)
		<td>
			<a href="{{route('edit.eleitor.comissao', [$idEleicao, $objEleitor->id])}}" class="glyphicon glyphicon-pencil" title="Editar"></a>

			<?php
    $valor_banco = DB::table('eleicao')->select('habilitar_exclusao', 'ele_horaInicio')->where('ele_id', $idEleicao)->first();
    
    if (isset($valor_banco)) {
        $habilitar_exclusao = $valor_banco->habilitar_exclusao;
        $hora_inicio_eleicao = $valor_banco->ele_horaInicio;
        $data_hora_atual = date('Y-m-d H:i:s');
        
        if ($data_hora_atual < $hora_inicio_eleicao) {
            // Garanta que o valor seja 1 antes do início da eleição
            $habilitar_exclusao = max($habilitar_exclusao, 1);
    
        }
    } else {
        $habilitar_exclusao = 0; // Valor padrão se não houver valor no banco
    }
?>

			<a href="{{ route('delete.eleitor.comissao', [$idEleicao, $objEleitor->id]) }}" class="glyphicon glyphicon-remove" title="Excluir"
			@if ($habilitar_exclusao == 0) style="pointer-events: none; color: lightgray;" @endif></a>

			<a href="#" onclick="abrirModalGerarNovaSenha('{{$objEleitor->id}}')" class="glyphicon glyphicon-copy" title="Gerar nova senha"></a>

			<a href="javascript:" link="{{route('user.resetar.acesso',[$objEleitor->id, $idEleicao])}}" class="updateAcess"  title="Reenviar senha alfanumérica"><img src="{{url('img/abcd.png')}}" style="width:25px;"/></a>

			<a href="javascript:" link="{{route('user.resetar2',[$objEleitor->id, $idEleicao])}}" class="updateAcess2"  title="Reenviar senha numérica"><img src="{{url('img/1234.png')}}" style="width:25px;"/></a>
			<!-- class="glyphicon glyphicon-refresh updateAcess" -->
		</td>
		@endif
	</tr>
	@endforeach
	@endif
	<script type="text/javascript">

		function abrirModalGerarNovaSenha(idEleitor){
			$("#idEleitor").val(idEleitor);
			$('.modalGeracaoNovaSenha').show();
		}

		$('#fecharModalGeracaoNovaSenha').click(()=>{
			$('.modalGeracaoNovaSenha').hide();
		})

		$("#confirmarGeracaoNovaSenha").click(function (event) {
			event.preventDefault(); 
			let formData = $("#formGeracaoNovaSenha").serializeArray(); 
        	formData.push({ name: "idEleitor", value: $("#idEleitor").val() }); 
			showLoading();

			$.ajax({
				url: "{{route('gerar.nova.senha.eleitor')}}", 
				type: "POST",
				data: $.param(formData),
				dataType: "json",
				success: function (response) {
					if (response.resultado == "sucesso") {
						navigator.clipboard.writeText(response.dados.mensagem) 
							.then(() => {
								alert("O link da eleição, login e senha foram copiados para área de transferência.")
							})
							.catch(err => alert("Erro ao copiar para área de transferência."));
					} else {
						alert("Ocorreu um erro ao gerar a senha.");
					}
					$(".modalGeracaoNovaSenha").hide();
					hideLoading();
				},
				error: function (xhr, status, error) {
					hideLoading();
					$(".modalGeracaoNovaSenha").hide();
					console.error("Erro na requisição:", error);
					alert("Ocorreu um erro ao gerar a senha.");
				}
			});
    	});

		$("#confirmarGeracaoNovaSenhaTextoCorrigido").click(function (event) {
			event.preventDefault();
			copiarSenha(true); // Copia sem quebras de linha
		});

		function copiarSenha(removerQuebras) {
			let formData = $("#formGeracaoNovaSenha").serializeArray();
			formData.push({ name: "idEleitor", value: $("#idEleitor").val() });
			showLoading();

			$.ajax({
				url: "{{route('gerar.nova.senha.eleitor')}}",
				type: "POST",
				data: $.param(formData),
				dataType: "json",
				success: function (response) {
					if (response.resultado == "sucesso") {
						let texto = response.dados.mensagem;
						
						if (removerQuebras) {
							texto = texto.replace(/(\r\n|\n|\r)/gm, " ");// Remove quebras de linha
						}

						navigator.clipboard.writeText(texto)
							.then(() => {
								alert("O link da eleição, login e senha foram copiados para área de transferência.");
							})
							.catch(err => alert("Erro ao copiar para área de transferência."));
					} else {
						alert("Ocorreu um erro ao gerar a senha.");
					}
					$(".modalGeracaoNovaSenha").hide();
					hideLoading();
				},
				error: function (xhr, status, error) {
					hideLoading();
					$(".modalGeracaoNovaSenha").hide();
					console.error("Erro na requisição:", error);
					alert("Ocorreu um erro ao gerar a senha.");
				}
			});
		}
	

		$(document).ready(function() {
			$('input[name="optionEmail"]').on('change', function() {
				$('#enviarSMS').prop('checked', false);
				$("#url").val('{{ route('notificar.eleitor', [$idEleicao]) }}');
				$('#confirmarGeracao').removeClass('sms-generate').addClass('mail-generate');

				$('#confirmarGeracao').off('click');
			});

			$('#enviarSMS').on('change', function() {
				$('input[name="optionEmail"]').prop('checked', false);
				
				var optionPassword = $('input[name="optionPassword"]:checked').val();
				var url = '{{ route('notificar.eleitor.sms', [$idEleicao, ':optionPassword']) }}';
								
				url = url.replace(':optionPassword', optionPassword);

				$("#url").val(url);
				$('#confirmarGeracao').removeClass('mail-generate').addClass('sms-generate');

				$('#confirmarGeracao').off('click').on('click', function() {
					gerarExcelSMS(url);
				});
			});

			function gerarExcelSMS(url) {
				var xhr = new XMLHttpRequest();
				xhr.open('GET', url, true); // Configura a requisição para GET

				xhr.responseType = 'blob'; // Define o tipo de resposta como blob (binário)

				xhr.onload = function() {
					if (xhr.status === 200) {
						var disposition = xhr.getResponseHeader('Content-Disposition');
						var filename = "eleitoresSMS.xlsx"; // Nome padrão do arquivo

						if (disposition && disposition.indexOf('attachment') !== -1) {
							var matches = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/.exec(disposition);
							if (matches != null && matches[1]) {
								filename = matches[1].replace(/['"]/g, '');
							}
						}

						// Criar o blob e salvar o arquivo
						var blob = new Blob([xhr.response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
						saveAs(blob, filename); // Usar a função saveAs para baixar o arquivo

						alert("{{ config('messages.voters_export.success') }}");
					} else {
						alert("{{ config('messages.voters_export.error') }}");
					}
				};

				xhr.onerror = function() {
					alert("{{ config('messages.voters_export.error') }}");
				};

				xhr.send(); // Envia a requisição
			}

		});


	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#formExportacaoEleitores").submit((e)=>{
				e.preventDefault();

				$.ajax({
                    url: "{{route('exportar.eleitores')}}", 
                    method: 'POST',
					data: $("#formExportacaoEleitores").serialize(),
					xhrFields: { responseType: 'blob' }
                    success: function(data){
                        var blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                		saveAs(blob, "eleitores.xlsx");

                        alert("{{ config('messages.voters_export.sucess')}}");
                    },
                    error: function(){
						alert("{{ config('messages.voters_export.error')}}");
                    },
					complete: function(){
						$(".modalExportacaoEleitores").hide();
					}
                });
			});

			
		})
	</script>

	<script type="text/javascript">
		$(document).ready(function() {
			$("a.updateAcess").click(function(e) {
				let url = $(this).attr('link');

				$.ajax({
					url: url,
					method: 'get',
					dataType: 'json'
				}).done(function(resp) {
					if (resp) {
						alert("Nova senha enviada com sucesso !!!");
					} else {
						alert("Falha no envio dos dados, favor entrar em contato com o suporte");
					}
				})
			});
			$("a.updateAcess2").click(function(e) {
				let url = $(this).attr('link');

				$.ajax({
					url: url,
					method: 'get',
					dataType: 'json'
				}).done(function(resp) {
					if (resp) {
						alert("Nova senha enviada com sucesso !!!");
					} else {
						alert("Falha no envio dos dados, favor entrar em contato com o suporte");
					}
				})
			});
		})
	</script>
</table>
{!! $listaEleitores->render()!!}
@endsection

@if(isset($emissaoZeresima) && isset($tempoParaIniciarAEleicaoEmMinutos) && count($errors) == 0)
    @if(!$emissaoZeresima && $tempoParaIniciarAEleicaoEmMinutos <= 30)
    <script>
        function startTimer(duration, display) {
			var timer = duration, minutes, seconds;
			setInterval(function () {
				minutes = parseInt(timer / 60, 10);
				seconds = parseInt(timer % 60, 10);
				minutes = minutes < 10 ? "0" + minutes : minutes;
				seconds = seconds < 10 ? "0" + seconds : seconds;
				display.html(minutes + ":" + seconds);
				if (--timer < 0) {
					timer = duration;
				}
			}, 1000);
    	}

    	window.onload = function () {
			var tempoParaIniciarAEleicaoEmMinutos = $("#tempoParaIniciarAEleicaoEmMinutos").val();
			var duration = 60 * "{{$tempoParaIniciarAEleicaoEmMinutos}}"; // Converter para segundos

			var minutes = parseInt(duration / 60, 10);
			var seconds = parseInt(duration % 60, 10);
			minutes = minutes < 10 ? "0" + minutes : minutes;
			seconds = seconds < 10 ? "0" + seconds : seconds;
			
			modal('alert','Início da votação em <span id="cronometro">'+minutes+':'+seconds+'</span>, necessário emissão da zerésima!');
			
			$('#modal #alert #modAlert a.modOk').attr("href", "{{route('eleicao.emitir.zeresima', Session::get('dataEleicao')[0]->ele_id)}}");

			display = $("#cronometro"); // selecionando o timer
			startTimer(duration, display); // iniciando o timer
    	};

    </script>
	<style>
		.modMensagem { font-size: 3em !important; height: 180px !important; }
	</style>
    @endif
@endif
