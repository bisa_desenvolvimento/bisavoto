
<?php 
/**
    			 * indices dos arrays 
    			 *       [0] - zona 	
    			 * 		 [1] - nome	
    			 *	     [2] - cpf 
    			 *       [3] - matricula
    			 *       [4] - email
    			 *       [5] - endereco
    			 *       [6] - cep
    			 *       [7] - cidade 
    			 *       [8] - estado	
				*/
$tamEleitoresDuplicados           = count($arrEleitoresDuplicados); 
$tamEleitorCampoObrigatorioBranco = count($arrEleitorCampoObrigatorioBranco);
$tamEleitorCampoEmailInvalido     = count($arrEleitorCampoEmailInvalido);
$tamEleitoresJaCadastrados        = count($arrEleitoresJaCadastrados);
?>
<div id="listaResultados">
<label>Resultados da importação</label>
<p>Eleitores:</p>
<ul>
	<li>Com registros duplicados na listagem - {{ $tamEleitoresDuplicados }}</li>
	<li>Com campos obrigatórios em branco - {{ $tamEleitorCampoObrigatorioBranco }} </li>
	<li>Com emails inválidos - {{ $tamEleitorCampoEmailInvalido }}</li>
	<li>Com registros já cadastrados na eleição - {{ $tamEleitoresJaCadastrados }}</li>
</ul>
</div>

@if($tamEleitoresDuplicados)
<label><h3>Lista de eleitores duplicados na listagem</h3></label>
<table border="1" width="600">
	<thead>
    <tr>
      <th>Nome</th>
      <th>Matrícula</th>
    </tr>
  </thead>
	<tr>
	  <td></td>
      <td></td>
	</tr>
</table>
@endif

@if($tamEleitorCampoObrigatorioBranco)
<label><h3>Lista de eleitores com campos obrigatórios em branco</h3></label>
<table border="1" width="600">
	<thead>
    <tr>
      <th>#</th>	
      <th>Nome</th>
      <th>Matrícula</th>
    </tr>
  </thead>
 	@for($i=0; $i < $tamEleitorCampoObrigatorioBranco; $i++)
	<tr>
	  <td>{{$i + 1}}</td>	
	  <td>{{utf8_encode($arrEleitorCampoObrigatorioBranco[$i][1])}}</td>
      <td>{{$arrEleitorCampoObrigatorioBranco[$i][3]}}</td>
	</tr>
    @endfor
</table>
@endif


@if($tamEleitorCampoEmailInvalido)
<label><h3>Lista de eleitores com emails inválidos</h3></label>
<table border="1" width="600">
	<thead>
    <tr>
      <th>#</th>	
      <th>Nome</th>
      <th>Matrícula</th>
    </tr>
  </thead>
 	@for($i=0; $i < $tamEleitorCampoEmailInvalido; $i++)
	<tr>
	  <td>{{$i + 1}}</td>	
	  <td>{{$arrEleitorCampoObrigatorioBranco[$i][1]}}</td>
      <td>{{$arrEleitorCampoObrigatorioBranco[$i][3]}}</td>
	</tr>
    @endfor
</table>
@endif


@if($tamEleitorCampoEmailInvalido)
<label><h3>Lista de eleitores com emails inválidos</h3></label>
<table border="1" width="600">
	<thead>
    <tr>
      <th>#</th>	
      <th>Nome</th>
      <th>Matrícula</th>
    </tr>
  </thead>
 	@for($i=0; $i < $tamEleitorCampoEmailInvalido; $i++)
	<tr>
	  <td>{{$i + 1}}</td>	
	  <td>{{$arrEleitorCampoObrigatorioBranco[$i][1]}}</td>
      <td>{{$arrEleitorCampoObrigatorioBranco[$i][3]}}</td>
	</tr>
    @endfor
</table>
@endif


