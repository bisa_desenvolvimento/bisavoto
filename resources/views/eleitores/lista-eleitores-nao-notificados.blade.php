@extends(Session::get('dataUser')->profile_id == 2 ? 'comissao' : 'admin')
@section('content')

<h3>Lista de eleitores não notificados</h3>
<p>Verifique se há algum e-mail incorreto e tente novamente.</p>
<p>OBS: Os demais eleitores foram notificados. 
Caso o botão "Notificar eleitores" seja clicado novamente, apenas os pendentes serão notificados.</p>
<table class="table table-striped" style="margin: 0 50px 0 20px; width: 96%">
	<thead>
		<tr>
			<th>Nome</th>
			<th>E-mail</th>
		</tr>
	</thead>
	
	@foreach( $naoNotificados as $eleitor )
	<tr>
		<td>{{$eleitor->nome}}</td>
		<td>{{$eleitor->email}}</td>
	</tr>
	@endforeach

</table>

@endsection