@extends(Session::get('dataUser')->profile_id == 2 ? 'comissao' : 'admin')
@section('content')
<div class="content-admin">
    <h3>Adicionar Eleitor</h3>
    <form class="form-horizontal" method="post" action="{{route(empty($id) ? 'salvar.eleitor' : 'editar.eleitor') }}">
      <input type="hidden" name="_token" value="{{csrf_token()}}">
      <input type="hidden" name="profile_id" value="3">
      <input type="hidden" name="ele_id" value="{{$ele_id}}">
      <input type="hidden" name="id" value="{{$id}}">

      {{Session::get('msgErro')}}

      @if(Session::has('msgErro'))
        <div class="alert alert-danger">
            {{Session::get('msgErro')}}
       </div>
      @endif

      @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
        <div class="alert alert-danger">
        {{$error}}
        </div>   
        @endforeach
      @endif

      <div class="form-group">
        <div class="col-sm-10">
            <label for="name">Nome do Eleitor *</label>
            <input type="text" value="{{$name}}" class="form-control" id="name" name="name" placeholder="Nome do Eleitor *" required="required">
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-10">
            <label for="email">Email</label>
            <input type="email" class="form-control" value="{{$email}}" class="form-control" id="email" name="email" placeholder="Email do Eleitor">
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-10">
            <label for="matricula">Matrícula * </label>
            <input type="text" class="form-control" value="{{$matricula}}" id="matricula" name="matricula" placeholder="Matrícula *" required="required">
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-4">
            <label for="cpf">CPF</label>
            <input type="text" class="form-control" value="{{$cpf}}" id="cpf" name="cpf" placeholder="CPF" required="required">
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-3">
            <label for="cep">CEP</label>
            <input type="text" class="form-control" value="{{$cep}}" id="cep" name="cep" placeholder="cep">
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-10">
            <label for="logradouro">Endereço</label>
            <input type="text" class="form-control" value="{{$logradouro}}" id="logradouro" name="logradouro" placeholder="Endereço">
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-10">
            <label for="cidade">Cidade</label>
            <input type="text" class="form-control" value="{{$cidade}}" id="cidade" name="cidade" placeholder="Cidade">
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-3">
            <label for="uf">Estado</label>
            <input type="text" class="form-control" value="{{$uf}}" maxlength="2" id="uf" name="uf" placeholder="Estado">
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-10">
          <label for="telefone">Telefone @if ( $ele_tipo_envio!=null && $ele_tipo_envio=="SMS" )
            *
          @endif  </label>
          <input type="tel" class="form-control" value="{{$telefone}}"name="telefone" id="telefone" placeholder="Telefone" maxlength="11" 
          @if ( $ele_tipo_envio!=null && $ele_tipo_envio=="SMS" )
            required
          @endif >
        </div>
      </div>
      
      <div class="form-group">
        <div class="col-sm-10">
            <label for="name">Zona Eleitoral *</label>
            <select class="form-control" name="zona_id" id="zona_id" required="required">
              <option value="">Selecione</option>
              @if($listaZonas->count())
                @foreach($listaZonas as $objZona) 
                    <option value="{{$objZona->zon_id}}" <?php echo ($zona_id == $objZona->zon_id) ? ' selected="selected"' : '';?>>{{$objZona->zon_nome}}</option>
                @endforeach
              @endif       
            </select>
        </div>
      
      </div>
      <button type="submit" class="btn btn-default">Salvar</button>
    </form>

</div>
@endsection

@section('script')
<?php
    if (!empty($id)):
?>
    <script>
        $(function() {
          $("#zona_id").val("{{ $zona_id }}").change();
        });


    </script>
<?php
    endif;
?>
  <script>
    $(document).ready(function(){
      $('#telefone').mask('(00) 00000-0000');
    });
  </script>
@endsection