@extends(Session::get('dataUser')->profile_id == 2 ? 'comissao' : 'admin')
@section('content')

<?php 
	$tam_eleitores_campo_obrigatorio = count($eleitores_campo_obrigatorio); 
	$tam_eleitores_duplicado 		 = count($eleitores_duplicado);
	$tam_eleitores_cpf_invalido      = count($eleitores_cpf_invalido);
	$tam_eleitores_email_invalido    = count($eleitores_email_invalido);
	$tam_eleitores_importados        = count($eleitores_importados);
?>
	
<fieldset>
	<legend>Resultado da Importação</legend>
</fieldset>

<div style="text-align: left;padding-left: 50px;">
	<h4>Eleitores:</h4>
	<ul>
		<li>Com registros já cadastrados na eleição - {{ $tam_eleitores_duplicado }}</li>
		<li>Com campos obrigatórios em branco - {{ $tam_eleitores_campo_obrigatorio }} </li>
		<li>Com emails inválidos - {{ $tam_eleitores_email_invalido }}</li>
		<li>Com cpf inválidos - {{ $tam_eleitores_cpf_invalido }}</li>
		<li>Importados com sucesso - {{ $tam_eleitores_importados }}</li>
	</ul>

	<hr>

	@if(count(Session::get('eleitores_com_emails_duplicados'))>0)
		<label><h4>Lista de eleitores com e-mails duplicados no arquivo ou com e-mails já cadastrados na eleição</h4></label>
		<table border="1" width="600">
			<thead>
				<tr style="font-weight: bold;">
					<th><b>Nome</b></th>
					<th><b>Matrícula</b></th>
					<th><b>E-mail</b></th>
				</tr>
			</thead>
			<tbody>
				@foreach(Session::get('eleitores_com_emails_duplicados') as $eleitor)
					<tr>
						<td>{{$eleitor["nome"]}}</td>
						<td>{{$eleitor["matricula"]}}</td>
						<td>{{$eleitor["email"]}}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		<hr>
	@endif

	@if($tam_eleitores_duplicado)
		<label><h4>Lista de eleitores já cadastrados na eleição</h4></label>
		<table border="1" width="600">
			<thead>
				<tr style="font-weight: bold;">
					<th><b>#</b></th>	
					<th><b>Nome</b></th>
					<th><b>Matrícula</b></th>
				</tr>
			</thead>
			<tbody>
				@for($i=0; $i < $tam_eleitores_duplicado; $i++)
					<tr>
						<td>{{$i+1}}</td>	
						<td>{{$eleitores_duplicado[$i]->nome}}</td>
						<td>{{$eleitores_duplicado[$i]->matricula}}</td>
					</tr>
				@endfor
			</tbody>
		</table>
		<hr>
	@endif

	@if($tam_eleitores_campo_obrigatorio)
		<label><h4>Lista de eleitores com campos obrigatórios em branco</h4></label>
		<table border="1" width="600">
			<thead>
				<tr style="font-weight: bold;">
					<th><b>#</b></th>	
					<th><b>Nome</b></th>
					<th><b>Matrícula</b></th>
				</tr>
			</thead>
			<tbody>
				@for($i=0; $i < $tam_eleitores_campo_obrigatorio; $i++)
					<tr>
						<td>{{$i+1}}</td>	
						<td>{{$eleitores_campo_obrigatorio[$i]->nome}}</td>
						<td>{{$eleitores_campo_obrigatorio[$i]->matricula}}</td>
					</tr>
				@endfor
			</tbody>
		</table>
		<hr>
	@endif
	
	@if($tam_eleitores_email_invalido)
		<label><h4>Lista de eleitores com E-mail inválido</h4></label>
		<table border="1" width="600">
			<thead>
				<tr style="font-weight: bold;">
					<th><b>#</b></th>	
					<th><b>Nome</b></th>
					<th><b>Matrícula</b></th>
				</tr>
			</thead>
			<tbody>
				@for($i=0; $i < $tam_eleitores_email_invalido; $i++)
					<tr>
						<td>{{$i+1}}</td>
						<td>{{$eleitores_email_invalido[$i]->nome}}</td>
						<td>{{$eleitores_email_invalido[$i]->matricula}}</td>
					</tr>
				@endfor
			</tbody>
		</table>
		<hr>
	@endif

	@if($tam_eleitores_cpf_invalido)
		<label><h4>Lista de eleitores com CPF duplicado</h4></label>
		<table border="1" width="600">
			<thead>
				<tr style="font-weight: bold;">
					<th><b>#</b></th>	
					<th><b>Nome</b></th>
					<th><b>Matrícula</b></th>
				</tr>
			</thead>
			<tbody>
				@for($i=0; $i < $tam_eleitores_cpf_invalido; $i++)
					<tr>
						<td>{{$i+1}}</td>
						<td>{{$eleitores_cpf_invalido[$i]->nome}}</td>
						<td>{{$eleitores_cpf_invalido[$i]->matricula}}</td>
					</tr>
				@endfor
			</tbody>
		</table>
		<hr>
	@endif
	
</div>
@endsection
