@extends(Session::get('dataUser')->profile_id ?? 2 ? 'eleitor' : 'comissao')
@section('content')
    <div class="content-admin">
        <h3>Adicionar Eleitor</h3>
        <form class="form-horizontal" id="eleitorForm" method="post" action="{{route('salvar.eleitor.durante.votacao') }}">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="profile_id" value="3">
            <input type="hidden" name="ele_id" value="{{$idEleicao}}">
            <input type="hidden" name="zona_id" value="999">

            @if(Session::has('msgErro'))
                <div class="alert alert-danger" style="display: none;" id="errorModalContent">
                    {{Session::get('msgErro')}}
                </div>
            @endif
            @if (count($errors) > 0)
    <div class="modal" id="errorModal" tabindex="-1" role="dialog" style="display: block;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Erro de Validação</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Por favor, corrija os erros abaixo:</p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="redirectAfterClose('{{ $eleicao->alias }}')">Fechar</button>
                </div>
            </div>
        </div>
    </div>
@endif

            <div class="form-group">
                <div class="col-sm-10">
                    <label for="name">Nome do Eleitor *</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Nome do Eleitor *" required="required">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-4">
                    <label for="cpf">CPF *</label>
                    <input type="text" class="form-control" id="cpf" name="cpf" placeholder="CPF" required="required" maxlength="14">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-10">
                    <label for="matricula">Matrícula *</label>
                    <input type="text" class="form-control" id="matricula" name="matricula" placeholder="Matrícula *" required="required">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-10">
                    <label for="email">Email *</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email do Eleitor" required="required">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-4">
                    <label for="telefone">Telefone *</label>
                    <input type="text" class="form-control" id="telefone" name="telefone" placeholder="Telefone" required="required" maxlength="12">
                </div>
            </div>

            <button type="button" id="openModal" class="btn btn-default">Salvar</button>
        </form>

        <div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="successModalLabel">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title font-weight-bold" id="successModalLabel"><strong>Sucesso</strong></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <p>O eleitor foi adicionado com sucesso! A senha de acesso foi enviada ao e-mail cadastrado.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-lg" data-dismiss="modal" style="background-color: #f28a07; color: black; text-transform: uppercase; font-weight: bold;">Fechar</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
            function redirectAfterClose(eleicaoAlias) {
            window.location.href = '{{ url("eleicao") }}/' + eleicaoAlias;
        }
    $(document).ready(function(){
        @if (count($errors) > 0)
            $('#errorModal').modal('show');
        @endif

        var modalJaFoiExibido = false;
        var modalHtml = `
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title font-weight-bold" id="myModalLabel"><strong>Declaração de Veracidade e Compromisso do Eleitor</strong></h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                <p>Ao prosseguir com o processo de cadastro e votação eletrônica oferecido pela Bisa, eu, como eleitor, declaro e confirmo que todas as informações fornecidas durante este processo são verdadeiras, precisas e pertencem exclusivamente a mim. Compreendo que a autenticidade e a propriedade dessas informações são fundamentais para a integridade do processo de votação.</p>
                                <p>Estou ciente de que minha participação neste processo de votação eletrônica está sujeita às legislações aplicáveis, incluindo, mas não se limitando a legislações específicas relacionadas à votação eletrônica, fraude eleitoral, falsidade ideológica, etc, bem como quaisquer outros termos jurídicos e regulamentos que governam este ato eleitoral.</p>
                                <p>Reconheço que a violação destas declarações pode resultar em consequências legais, incluindo penalidades sob as leis aplicáveis. Comprometo-me a cumprir com todos os termos, condições, leis e regulamentos relevantes durante minha participação neste processo de votação.</p>
                                <p>Ao fornecer meu endereço de e-mail e demais informações solicitadas, dou meu consentimento para receber o link e a senha de votação neste endereço eletrônico e aceito que esta será a forma de comunicação oficial para qualquer assunto relacionado à votação.</p>
                                <p>Por este meio, reafirmo meu compromisso com a integridade e a veracidade do processo eleitoral conduzido pela Bisa, garantindo que meu voto e minha participação sejam realizados de maneira ética e responsável.</p>
                                <div class="checkbox">
                                    <label><input type="checkbox" id="concorda" required> Concordo com a Declaração de Veracidade e Compromisso do Eleitor.</label>
                                </div>
                            </div>
                            <div class="modal-footer" style="text-align: center;">
                                <button type="button" class="btn btn-lg" data-dismiss="modal" style="background-color: #ff4d4d; color: black; text-transform: uppercase; font-weight: bold;">Fechar</button>
                                <button type="button" id="avancar" class="btn btn-lg" style="background-color: #f28a07; color: black; text-transform: uppercase; font-weight: bold;">Avançar</button>
                            </div>
                        </div>
                    </div>
                </div>`;
        $('body').append(modalHtml);


        $('#avancar').click(function() {
            
            if ($('#concorda').is(':checked')) {
                modalJaFoiExibido = true;
                $('#myModal').modal('hide');

                $('#eleitorForm').unbind('submit').submit(function(event) {
                    event.preventDefault();
                    $.ajax({
                        url: $(this).attr('action'),
                        method: 'POST',
                        data: $(this).serialize(),
                        success: function(data) {
                            if(data.sucesso){
                                $('#successModal').modal('show');
                                $('#successModal').on('hidden.bs.modal', function () {
                                    redirectAfterClose('{{ $eleicao->alias }}');
                                });
                            }else{
                                alert(data.message.map((msg, index) => `${index + 1}. ${msg}`).join('</br> </br>'));
                            }
                        },
                        error: function() {
                            alert('Ocorreu um erro ao salvar. Tente novamente.');
                        }
                    });
                }).submit();
            } else {
                alert('Por favor, concorde com os termos para continuar.');
            }
        });

        // Verificar se há mensagem de erro
        $('#openModal').click(function(){
            var isValid = true;
            var errorMsg = '';

            $('#eleitorForm input[required]').each(function() {
                if ($(this).val().trim() === '') {
                    isValid = false;
                    errorMsg += 'O campo ' + $(this).attr('name') + ' é obrigatório.<br>';
                }
            });

            if (!isValid) {
                alert(errorMsg);
                return;
            }

            if(!modalJaFoiExibido){
                $('#myModal').modal('show');       
            }else{
                $('#avancar').click();
            }
        });
    });
</script>
@endsection
