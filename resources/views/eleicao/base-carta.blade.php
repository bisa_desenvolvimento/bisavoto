@extends('email')
@section('content')
<style>
.page-break {
    page-break-after: always;
}
</style>
<div class="content-admin">
    <div id="logoEleicao">[logo]</div>
    <h3>[eleicao]</h3>
    Olá [nome],<br /><br />
    Inicio da Eleição: [dataInicio]<br />
    Término da Eleição: [dataTermino]<br /><br />

    Acesse: <a href="http://www.bisaweb.com.br">http://www.bisaweb.com.br</a><br />
    Login: [login]<br />
    Senha: [senha]

</div>
<div class="page-break"></div>
@endsection
