@extends('admin')
@section('content')
        
<?php
    $today = strtotime(date('Y-m-d H:i:s'));
    $horaTerminoFormat = DateTime::createFromFormat('d/m/Y H:i:s', $ele_horaTermino)->format('Y-m-d H:i:s');
    $horaInicioFormat = DateTime::createFromFormat('d/m/Y H:i:s', $ele_horaInicio)->format('Y-m-d H:i:s');
    $horaTermino = strtotime($horaTerminoFormat);
    $horaInicio = strtotime($horaInicioFormat);
    $diff = ($today - $horaTermino) /(3600*24);
?>

<style>
    .input-group {
        margin-bottom: 5px;
    }

    .glyphicon {
        margin-right: 10px;
    }
</style>

<div class="row">
    <div class="col-md-8">
        <div class="col-md-6">
            <img src="{{ secure_asset($ele_logo) }}"  class="img-rounded" style="max-width: 325px;"/>
        </div>
        <div class="col-md-6" style="text-align: left;">
            <address>
                <strong>{{ $ele_nome }}</strong><br>
                {{ $ele_descricao }}<br>
                <b>Início:</b> {{ $ele_horaInicio }}<br>
                <b>Témino:</b> {{ $ele_horaTermino }}<br>
                <b>URL da Eleição:</b> <a href="{{ url('eleicao') }}/{{ $ele_alias }}" target="_blank" rel="noopener noreferrer">Clique e acesse</a><br>
              </address>
        </div>
    </div>
    <div class="col-md-4">
        <div class="input-group">
            <a href="{{route('lista.comissao',[$ele_id])}}" class="btn btn-primary"><span class="glyphicon glyphicon-modal-window"></span>Comissão Eleitoral / Mesas</a>
        </div>
        <div class="input-group">
            <a href="{{route('lista.cargos',[$ele_id])}}" class="btn btn-primary"><span class="glyphicon glyphicon-list"></span>Cargos / Propostas</a>
        </div>
        <div class="input-group">
            <a href="{{route('lista.zonas',[$ele_id])}}" class="btn btn-primary"><span class="glyphicon glyphicon-home"></span>Zonas Eleitorais</a>
        </div>
        <div class="input-group">
            <a href="{{route('lista.eleitores',[$ele_id])}}" class="btn btn-primary"><span class="glyphicon glyphicon-user"></span>Eleitores</a>
        </div>
        <div class="input-group">
            <a href="{{route('resumo.relatorio.aptos',[$ele_id])}}" class="btn btn-primary"><span class="glyphicon glyphicon-print"></span>Imprimir lista de Aptos</a>
        </div>
        @if($horaInicio > $today && $diff < 0)
        <div class="input-group">
            <a href="{{route('eleicao.emitir.zeresima',[$ele_id])}}" class="btn btn-primary"><span class="glyphicon glyphicon-erase"></span>Emitir Zeresima</a>
        </div>
        @endif
        <?php if($diff >= 0): ?>
        <div class="input-group">
            <a href="{{route('apuracaofinal',[$ele_id])}}" class="btn btn-primary"><span class="glyphicon glyphicon-blackboard"></span>Apuração Final</a>
        </div>
        <?php else: ?>
        <div class="input-group">
            <a href="{{route('resumo.relatorio.votantes',[$ele_id])}}" class="btn btn-primary"><span class="glyphicon glyphicon-list-alt"></span>Relatório parcial de votantes</a>
        </div>
        <?php endif; ?>
    </div>
</div>
@endsection