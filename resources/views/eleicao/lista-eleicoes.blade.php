@extends('admin')
@section('content')
<?php
    $today = strtotime(date('Y-m-d H:i:s'));
?>

<div class="form-check" style="text-align:left; margin:20px; margin-left:35px; display:flex;">
    <form action="{{ route('filtro.eleicoes') }}" method="GET" style="display:flex;">

        <label style="margin-right: 18px;">Filtrar por:</label>
        <div style="margin-right: 15px;">
            <input class="form-check-input" type="radio" name="filtro" id="eleicoes-realizadas" value="realizadas"
                <?php echo isset($_GET['filtro']) && $_GET['filtro'] == 'realizadas' ? 'checked' : ''; ?>>
            <label class="form-check-label" for="flexCheckDefault">
                Realizadas
            </label>
        </div>
        <div style="margin-right: 15px;">
            <input class="form-check-input" type="radio" name="filtro" id="eleicoes-nao-realizadas"
                value="nao-realizadas"
                <?php echo isset($_GET['filtro']) && $_GET['filtro'] == 'nao-realizadas' ? 'checked' : ''; ?>>
            <label class="form-check-label" for="flexCheckChecked">
                Não Realizadas
            </label>
        </div>
        <div style="margin-right: 15px;">
            <input class="form-check-input" type="radio" name="filtro" id="ambas" value="ambas"
                <?php echo empty($_GET['filtro']) || $_GET['filtro'] == 'ambas' ? 'checked' : ''; ?>>
            <label class="form-check-label" for="flexCheckChecked">
                Ambas
            </label>
        </div>
        <div>
        <button type="submit" class="btn btn-primary btn-xs" style="margin-top: 3px;"><span class="glyphicon glyphicon-filter"></span></button>
        </div>
    </form>
</div>

<a href="{{route('add.eleicoes')}}" class="btn btn-primary right" style="margin-right: 20px">Adicionar Eleição /
    Assembleia</a>
<table class="table table-striped" style="margin: 0 50px 0 20px; width: 96%">

    <thead>
        <tr>
            <th>Eleição</th>
            <th>Início</th>
            <th>Término</th>
            <th>Status</th>
            <th>URL da Eleição</th>
            <th style="width: 100px;">Ações</th>
            <th style="width: 125px;">Módulos</th>
        </tr>
    </thead>
    <tbody id="tabela-eleicoes">
        {{-- This comment will not be in the rendered HTML --}}
        @if ( !$listaEleicoes->count() )
        <tr>
            <td colspan="3">Nenhuma eleição cadastrada</td>
        </tr>
        @else
        @foreach( $listaEleicoes as $objEleicao )
        <?php
        $ele_horaTermino = strtotime(Carbon::createFromFormat('Y-m-d H:i:s', $objEleicao->ele_horaTermino));
        $diff = ($today - $ele_horaTermino) /(3600*24);

        $dataAtual = Carbon::now();
        $dataInicio = Carbon::parse($objEleicao->ele_horaInicio);
        $dataFim = Carbon::parse($objEleicao->ele_horaTermino);
        
    ?>
        <tr>
            <td>{{$objEleicao->ele_nome}}</td>
            <td><?php echo date('d/m/Y H:i:s', strtotime($objEleicao->ele_horaInicio)); ?></td>
            <td><?php echo date('d/m/Y H:i:s', strtotime($objEleicao->ele_horaTermino)); ?></td>
            <td>{{isset($objEleicao->ele_pausar) && $objEleicao->ele_pausar == 1? 'Pausada' : 'Liberada'}}</td>
            <td><?php echo url('eleicao'); ?>/<?php echo $objEleicao->alias;?></td>
            <td>
                <a href="{{ route('pagina.eleicao',[$objEleicao->ele_id])}}" class="glyphicon glyphicon-search"
                    title="Ver Eleição"></a>
                <a href="{{ route('edit.eleicoes',[$objEleicao->ele_id])}}" class="glyphicon glyphicon-pencil"
                    title="Editar"></a>
                @if($dataAtual->between($dataInicio, $dataFim))
                    <a href="{{ route('pausar.eleicoes', [$objEleicao->ele_id]) }}" class="glyphicon glyphicon-{{ $objEleicao->ele_pausar == 0 ? 'pause' : 'play' }} btn-pausar"
                       title="{{ $objEleicao->ele_pausar == 0? 'Pausar' : 'Despausar' }}"

                       ></a>
                @endif

                <a href="{{ route('delete.eleicao',[$objEleicao->ele_id])}}" id="exclusao"
                    class="glyphicon glyphicon-remove" title="Excluir"></a>
            </td>
            <td>
                <a href="{{route('lista.comissao',[$objEleicao->ele_id])}}" class="glyphicon glyphicon-modal-window"
                    title="Comissão Eleitoral / Mesa"></a>
                <a href="{{route('lista.cargos',[$objEleicao->ele_id])}}" class="glyphicon glyphicon-list"
                    title="Cargos / Propostas"></a>
                <a href="{{route('lista.zonas',[$objEleicao->ele_id])}}" class="glyphicon glyphicon-home"
                    title="Zonas Eleitorais"></a>
                <a href="{{route('lista.eleitores',[$objEleicao->ele_id])}}" class="glyphicon glyphicon-user"
                    title="Eleitores"></a>
                <!-- <a href="{{route('resumo',[$objEleicao->ele_id])}}" class="glyphicon glyphicon-object-align-bottom" title="Resumo da Eleição" ></a> -->
                <a href="{{route('resumo.relatorio.aptos',[$objEleicao->ele_id])}}" class="glyphicon glyphicon-print"
                    title="Imprimir lista de Aptos"></a>

                @if(strtotime($objEleicao->ele_horaInicio) > $today && $diff < 0) <a
                    href="{{route('eleicao.emitir.zeresima',[$objEleicao->ele_id])}}" class="glyphicon glyphicon-erase"
                    title="Emitir Zeresima"></a>
                    @else
                    @if($today > strtotime($objEleicao->ele_horaInicio) && $objEleicao->zeresima == 0 && $diff < 0) <a
                        id="linkEmissaoZeresima" href="{{route('eleicao.emitir.zeresima',[$objEleicao->ele_id])}}"
                        onclick="ocultarLink()" class="glyphicon glyphicon-erase" title="Emitir Zeresima"></a>
                        @endif
                        @endif

                        <?php
            if($diff >= 0):
           ?>
                        <a href="{{route('apuracaofinal',[$objEleicao->ele_id])}}"
                            class="glyphicon glyphicon-blackboard" title="Apuração Final"></a>
                        <?php
            else:
           ?>
                        <a href="{{route('resumo.relatorio.votantes',[$objEleicao->ele_id])}}"
                            class="glyphicon glyphicon-list-alt" title="Relatório parcial de votantes"></a>
                        <a href="javascript:;" class="glyphicon glyphicon-hourglass"
                            title="Aguardando término da eleição para a Apuração Final"></a>

                            <a href="{{ route('resumo.relatorio.resultadoparcial', [$objEleicao->ele_nome, $objEleicao->ele_id]) }}"
                            class="glyphicon glyphicon-time"
                            title="Painel de Votação"
                            target="_blank"></a>

                        <?php





            endif;
           ?>
            </td>
        </tr>
        @endforeach
        @endif
    </tbody>
</table>

{!!$listaEleicoes->render()!!}
@endsection