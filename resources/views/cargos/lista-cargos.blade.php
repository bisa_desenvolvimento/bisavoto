@extends(Session::get('dataUser')->profile_id == 2 ? 'comissao' : 'admin')
@section('content')

<?php 
 $idEleicao = Request::segment(3);
?>

<!-- @if( Session::get('dataUser')->profile_id == 2 )
    <a href="{{ route('add.cargos.comissao') }}" class="btn btn-primary right" style="margin-right: 20px">Adicionar Cargo</a>
@else
    <a href="{{ route('add.cargos', [$idEleicao]) }}" class="btn btn-primary right" style="margin-right: 20px">Adicionar Cargo</a>
@endif -->

@if( Session::get('dataUser')->profile_id == 1 )
    <a href="{{ route('add.cargos', [$idEleicao]) }}" class="btn btn-primary right" style="margin-right: 20px">Adicionar Cargo</a>
@endif

<table class="table table-striped" style="margin: 0 50px 0 20px; width: 96%" id="tabelaCargos">
    <thead>
        <tr>
            <th>Nome</th>

            @if(Session::get('dataUser')->profile_id == 1)
                <th>Ações</th>
            @endif

            <th class="text-center">Módulos</th>
        </tr>
    </thead>
     {{-- This comment will not be in the rendered HTML --}}
  @if ( !$listaCargos->count() )
    <tr>
        <td colspan="3">Nenhum Registro cadastrado</td>
    </tr>
  @else
    <tbody>
      @foreach( $listaCargos as $objCargo )

          <tr id="ordem-ele{{$idEleicao}}#car{{$objCargo->car_id}}" >
            <td>{{$objCargo->car_nome}}</td>
            <!-- <td>
                @if(Session::get('dataUser')->profile_id == 2)
                    <a  href="{{ route('edit.cargos.comissao',[$objCargo->car_id])}}" class="glyphicon glyphicon-pencil"></a>
                    <a  href="{{ route('delete.cargos.comissao',[$objCargo->car_id])}}" class="glyphicon glyphicon-remove"></a>
                @else
                    <a  href="{{ route('edit.cargos',[$objCargo->car_id, $idEleicao])}}" class="glyphicon glyphicon-pencil"></a>
                    <a  href="{{ route('delete.cargos',[$objCargo->car_id, $idEleicao])}}" class="glyphicon glyphicon-remove"></a>
                @endif
            </td> -->
            @if(Session::get('dataUser')->profile_id == 1)
                <td>
                    <a  href="{{ route('edit.cargos',[$objCargo->car_id, $idEleicao])}}" class="glyphicon glyphicon-pencil"></a>
                    <a  href="{{ route('delete.cargos',[$objCargo->car_id, $idEleicao])}}" class="glyphicon glyphicon-remove"></a>
                </td>
            @endif
            <td class="text-center">
                @if(Session::get('dataUser')->profile_id == 2)
                    <a href="{{route('lista.candidatos.comissao', [$objCargo->car_id])}}" class="glyphicon glyphicon-user" title="Candidatos" ></a>
                @else
                    <a href="{{ route('lista.candidatos',[$objCargo->car_id, $idEleicao])}}" class="glyphicon glyphicon-user" title="Candidatos" ></a>
                @endif
            </td>
          </tr>
          
        @endforeach
     </tbody>
 @endif
</table>
@endsection
