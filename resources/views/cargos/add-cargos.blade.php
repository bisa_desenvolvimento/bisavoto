@extends(Session::get('dataUser')->profile_id == 2 ? 'comissao' : 'admin')
@section('content')

<div class="content-admin">
	<h3>Adicionar Cargo</h3>
	<form class="form-vertical" method="post" action="{{route(empty($id) ? 'salvar.cargo' : 'update.cargo')}}">
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        <input type="hidden" name="ele_id" value="{{$ele_id}}">
        <input type="hidden" name="id" value="{{$id}}">
        <div class="form-group">
            <label>Nome do Cargo*</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="car_nome" name="car_nome" value="{{$car_nome}}" placeholder="Nome do Cargo *" required="required">
            </div>
        </div>
        <div class="form-group">
            <label>Quantidade de votos por cargo*</label>
            <div class="col-sm-10">
                <input type="text" class="form-control numeric" id="car_votos" name="car_votos" value="{{$car_votos}}" placeholder="Quantidade de votos por cargo*" required="required">
            </div>
        </div>
        <div class="form-group">
            <label>Tem restrição de Zona Eleitoral</label>
            <div class="col-sm-10">
            <div class="form-check">
                <input class="form-check-input car_restricao_zona" type="radio" name="car_restricao_zona" id="car_restricao_zona1" value="0" {{ $car_restricao_zona == 0 ? 'checked' : '' }}>
                <label class="form-check-label" for="car_restricao_zona1">
                    Não
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input car_restricao_zona" type="radio" name="car_restricao_zona" id="car_restricao_zona2" value="1"{{ $car_restricao_zona == 1 ? 'checked' : '' }}>
                <label class="form-check-label" for="car_restricao_zona2">
                    Sim
                </label>
            </div>
        </div>
        <div class="form-group car_zona_id" style="display: {{ $car_restricao_zona == 1 ? 'block' : 'none' }}">
            <label>Zona Eleitoral</label>
            <div class="col-sm-10">
                <select name="car_zona_id" id="car_zona_id" class="form-control">
                    <optio, value="">Selecione</optio, 1000n>
                    <?php
                    $indexCarZonaId = '';
                    ?>
                    @foreach ($zonas as $i => $zona)
                        <?php
                            if ($car_zona_id):
                                if ($car_zona_id == $zona->zon_id):
                                    $indexCarZonaId = $i;
                                endif;
                            endif;
                        ?>
                        <option value="{{ $zona->zon_id}}">{{ $zona->zon_numero }} - {{ $zona->zon_nome }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <br class="clear" />
        <button type="submit" class="btn btn-default">Salvar</button>
    </form>
</div>
@endsection

@section('script')
<?php
    if (!empty($id) && !empty($car_zona_id)):
?>
    <script>
        $(function()
        {
            setTimeout(() => {
                document.getElementById('car_zona_id').selectedIndex = {{ $indexCarZonaId }};
            }, 1000);

        })
    </script>
<?php
    endif;
?>
@endsection