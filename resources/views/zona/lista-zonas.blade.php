<?php 
$perfil = Auth::user()->profile_id;
?>
@extends($perfil == 2 ? 'comissao' : 'admin')
@section('content')

<!-- @if(($perfil == 2))
    <a href="{{route('add.zona.comissao')}}" class="btn btn-primary right" style="margin-right: 20px">Adicionar Zona Eleitoral</a>
@else
    <a href="{{route('add.zona', [Request::segment(3)])}}" class="btn btn-primary right" style="margin-right: 20px">Adicionar Zona Eleitoral</a>
@endif -->

@if(($perfil == 1))
    <a href="{{route('add.zona', [Request::segment(3)])}}" class="btn btn-primary right" style="margin-right: 20px">Adicionar Zona Eleitoral</a>
@endif

<table class="table table-striped" style="margin: 0 50px 0 20px; width: 96%">

    <thead>
        <tr>
            <th>Número</th>
            <th>Nome</th>
            @if(($perfil == 1))
                <th>Ações</th>
            @endif
        </tr>
    </thead>
  @if ( !$listaZonas->count() )
    <tr>
        <td colspan="3">Nenhuma Zona Eleitoral cadastrada</td>
    </tr>
  @else
      @foreach( $listaZonas as $objZona )
          <tr>
            <td>{{$objZona->zon_numero}}</td>
            <td>{{$objZona->zon_nome}}</td>
            <!-- <td>
                @if($perfil == 2)
                    <a href="{{ route('edit.zona.comissao',[$objZona->zon_id])}}" class="glyphicon glyphicon-pencil"></a>
                    <a href="{{ route('delete.zona.comissao',[$objZona->zon_id])}}" class="glyphicon glyphicon-remove"></a>
                    <a href="javascript:;" id="zona_{{$objZona->zon_id}}" class="classZona glyphicon glyphicon-save-file" style="color:grey" title="Imprimir Boletim de Urna (BU) por Zona" ></a>
                @else
                    <a href="{{ route('edit.zona',[$objZona->zon_id, Request::segment(3)])}}" class="glyphicon glyphicon-pencil"></a>
                    <a href="{{ route('delete.zona',[$objZona->zon_id, Request::segment(3)])}}" class="glyphicon glyphicon-remove"></a>
                @endif
            </td> -->

            @if($perfil == 1)
                <td>
                    <a href="{{ route('edit.zona',[$objZona->zon_id, Request::segment(3)])}}" class="glyphicon glyphicon-pencil"></a>
                    <a href="{{ route('delete.zona',[$objZona->zon_id, Request::segment(3)])}}" class="glyphicon glyphicon-remove"></a>
                </td>
            @endif

          </tr>
     @endforeach
 @endif
</table>

@endsection