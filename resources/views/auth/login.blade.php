@extends('app')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
@section('content')
@if (session()->has('error'))
<script type="text/javascript">
$(document).ready(function(){
    modal('alert', '{{ session('error') }}');
});
</script>
{{ session()->forget('error') }} <!-- Remove a mensagem da sessão após exibir -->
@endif

<div id="tela04">

	<div id="votacaoTopo">
    <p>
        @if(isset($_SESSION['eleicao']) && $_SESSION['eleicao']->ele_logo)
            {{ $_SESSION['eleicao']->ele_nomenclatura }}<br>
            <!-- {{ $_SESSION['eleicao']->ele_nome }} -->
        @else
            Eleição
        @endif
    </p>
    </div>

    <div id="formVotacao">
        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)

            <script type="text/javascript">

                $(document).ready(function(){
                    modal('alert','{{ $error }}');
                });

            </script>
            @endforeach
        @endif

        <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
        						<input type="hidden" name="_token" value="{{ csrf_token() }}">
            @if(isset($eleicao))
                <input type="text" class="validate[required] left" name="matricula" value="{{ old('matricula') }}" placeholder="Login">
                <input type="hidden" name="ele_id" value="{{ $eleicao->ele_id }}">
            @else
                <input type="text" class="validate[required] left" name="cpf" value="{{ old('cpf') }}" placeholder="Login">
            @endif

            <!--<input type="password" name="senha" id="senha" class="validate[required] left clear" value="Senha" />-->
            <input type="password" class="validate[required] left clear" name="password" placeholder="Senha">

            <div class="clear"></div>

            <button type="submit" class="login clear" onclick="document.formLoginUsuario.submit();">login</button>
        </form>
        @if(isset($eleicao))
            @if($eleicao->exibir_botao_reenvio_senha)
                <a href="{{ route('link.eleicao.linkReenviarSenha', ['alias' => $eleicao->alias]) }}" style="font-size: 20px;color: #eb240f; margin-right: 140px;">Reenviar a senha? Clique Aqui</a>
            @endif
            @if($eleicao->habilitar_adicionareleitor)
                <a href="{{ route('adicionar.eleitor.durante.votacao', $eleicao->ele_id ) }}" style="font-size: 20px;color: #eb240f;">Se não tem login e senha, clique aqui.</a>
            @endif
        @endif
        
        <div class="container-ajuda">
            <span class="texto-ajuda">Precisa de ajuda?</span>
            <div class="links-ajuda">
                <a class="link-whatsapp" href="https://wa.me/558133127070" target="_blank"><i class="bi bi-whatsapp"></i></a>
                <a class="link-email" href="https://mail.google.com/mail/?view=cm&fs=1&to=suporte@bisa.com.br" target="_blank"><i class="bi bi-envelope-at"></i></a>
            </div>
        </div>

    </div>
</div><!-- /tela04 -->

@if(isset($_SESSION['eleicao']) && $_SESSION['eleicao']->ele_pausar ==1 )
    <script>
        $(document).ready(function (){
            let eleicaoId = '{{ $_SESSION['eleicao']->ele_id }}';

            if (!localStorage.getItem('votacao_pausada_' + eleicaoId)) {
                modal('alert', 'A votação está pausada no momento.');
                localStorage.setItem('votacao_pausada_' + eleicaoId, 'true');
            }
            $('form').on('submit', function() {
                event.preventDefault();
                modal('alert', 'A votação está pausada no momento. Tente novamente mais tarde.')
            })
        })
    </script>
@endif
<style>
    .container-ajuda{
        display: flex;
        flex-direction: column;
        align-items: center;
        margin-top: 30px;
    }
    
    .texto-ajuda{
        font-size: 18px;
        
        color: #e83414;
       
    }

    .links-ajuda{
        display: flex;
        flex-direction: row;
        justify-content: center;
        align-items: center;
    }

    a{  
        font-size: 30px;
        margin: 10px;
        transition: color 0.5s;
    }

    .link-whatsapp{
       color: #25D366; 
    }

    .link-whatsapp:hover{
       color: #075E54; 
    }
    
    .link-email{
        color: #6a696d;
    }
    
    .link-email:hover{
        color: #1A1A1A;
    }
</style>

@endsection

