@extends('app')
@section('content')

<div id="tela04">

	<div id="votacaoTopo">

    	<p>Eleições</p>

    </div>
    
    <div id="formVotacao">
        @if(isset($eleicao))
            <span style="font-size: 20px;margin-bottom: 30px;display: inline-flex;">Preencha os campos abaixo para receber uma nova senha</span>
            <form id="formReenvioSenha" class="form-horizontal" role="form" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="ele_id" value="{{ $eleicao->ele_id }}">
                <input type="hidden" name="ele_alias" value="{{ $eleicao->alias }}">
                <input type="hidden" name="ele_nome" value="{{ $eleicao->ele_nome }}">

                <input type="text" class="left clear" name="nome" placeholder="Nome">
                <input type="email" class="left clear" name="email" placeholder="E-mail*">
                <input type="text" class="left clear" name="matricula" placeholder="Matricula">
                <input type="text" class="left clear" name="cpf" placeholder="CPF*">
                <input type="text" class="left clear" name="telefone" placeholder="Telefone">

                <div class="clear"></div>
                <div id="btReenviarSenha">
                    <input style="background: url(../../img/button_reenviar-senha.png) no-repeat;width: 290px;height: 66px;display: inline-block;text-indent: -9999px;margin: 70px 0 40px;border: none;cursor: pointer;" class="login clear" value="Reenviar Senha" id="reenviarSenha">
                </div>


            </form>
        @endif
    </div>

    
    <div id="modalErro" class="modalErro" style="display: none;">
        <div class="sombraErro"></div>
            <div id="alertErro">
                <div id="confirmarErro" style="margin-top: 60px">
                    <p class="format-paragrafo">Prezado(a) eleitor(a), </p><br>
                    <p class="format-paragrafo">Gostaríamos de reforçar a importância de se seguir as regras estabelecidas no processo de votação, sem recorrer a práticas ilegais como a tentativa de fraude. Para garantir a integridade e transparência desse processo, informamos que todos os dados do seu computador e informações relacionadas à votação estão sendo registrados e podem ser utilizados em uma eventual sindicância para apurar qualquer irregularidade.</p><br>

                    <p class="format-paragrafo">Lembramos que cada usuário tem direito a apenas uma solicitação de voto, e qualquer tentativa de burlar esse processo será detectada e poderá acarretar em punições severas. É fundamental que todos respeitem as regras estabelecidas para que o resultado da votação seja justo e represente a vontade da maioria.</p><br>

                    <p class="format-paragrafo">Ressaltamos que estamos comprometidos em manter a integridade do processo de votação e garantir que todos os usuários possam participar de maneira justa e transparente. Portanto, contamos com a colaboração de todos para que possamos alcançar esse objetivo.</p><br>

                    <p class="format-paragrafo">Agradecemos a compreensão e reforçamos que qualquer ação irresponsável será investigada e tratada com seriedade. </p><br>

                </div><!-- /confirmar -->
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary center btn-confirmar" id="confirmarReenvio"  value="CONCORDAR E ENVIAR A SENHA" />
                    <button class="btn btn-default btn-cancelar" style=" margin-right: 20px;" id="cancelarReenvio">CANCELAR O ENVIO DA SENHA</button>

                </div>
      
            </div><!--/alert-->
        </div>
    </div><!-- /tela04 -->


<style>
    
    .format-paragrafo{
        font-size: 1.5em;
        text-align: justify;
    }

    .btn-confirmar{
        margin-right: 20px;
        background-image: linear-gradient(to bottom,#e99032 0,#c38b21 100%);
        width: 232px;
        height: 40px;
        margin-top: -50px;
        border-radius: 10px;
        color: #f7fafb;
        font-weight: bold;
        cursor:pointer;
        font-size: 1.1em;
    }

    .btn-cancelar{
        margin-right: 20px;
        background:#e0e0e0;
        width: 232px;
        height: 40px;
        margin-top: -50px;
        border-radius: 10px;
        color:#5d4f4f;
        font-weight: bold;
        cursor:pointer; 
        font-size: 1.1em;
        font-family: Helvetica, sans-serif;
        
    
    }

</style>



<script>

    $('#reenviarSenha').click(()=>{
        $('#modalErro').show();
	})
	$('#cancelarReenvio').click(()=>{
		$('#modalErro').hide();
	})

    $('#confirmarReenvio').click(()=>{
        
        $('#modalErro').hide();
        event.preventDefault();
        let form = $("#formReenvioSenha").serialize();
        let dados = new URLSearchParams(form);
        let email = dados.get('email');
        let cpf = dados.get('cpf');


        if(email == '' || cpf == ''){

            modal('alert','As informações necessárias não foram preenchidas. Por favor, verifique e tente novamente.');

        } else {

            $.ajax({
            url: "{{ route('link.eleicao.reenviarsenha', ['alias' => $eleicao->alias]) }}", 
            type: "POST",
            data: form,
            dataType: 'json',
            beforeSend : function(){
                $("#btReenviarSenha").html(`<img src="../../img/load.gif" style="width: 50px;">`);
            },
                success: function(data){
                    switch (data.resultado) {
                        
                        case 'envia_email':
                            modal('alert','Uma nova senha foi gerada e enviada para o e-mail informado!');
                            break;
                        case 'email_incorreto':
                            modal('alert','Com base nas informações passadas o seu e-mail cadastrado é: <span style="color:red;">'+data.emailCorreto+'</span> Por favor, entre em contato com a Comissão eleitoral para atualizar seu e-mail.');
                            break;
                        case 'inconsistencia':
                            modal('alert','Encontramos inconsistências nos dados informados. Por favor, entre em contato com o nosso suporte para obter ajuda.');
                            break;
                        case 'voto_confirmado':
                            modal('alert','Seu voto já foi computado. Você não pode votar novamente nesta eleição.');
                            break;
                        case 'reenvio_confirmado':
                            modal('alert','Você já solicitou o envio de senha anteriormente e esta rotina só pode ser usada uma vez. Para obter ajuda, entre em contato com nosso suporte.');
                            break;
            
                    }
                    $("#btReenviarSenha").html(`<input style="background: url(../../img/button_reenviar-senha.png) no-repeat;width: 290px;height: 66px;display: inline-block;text-indent: -9999px;margin: 70px 0 40px;border: none;cursor: pointer;" type="submit" class="login clear" value="Reenviar Senha">`);
                },
                
                error: function(request, status, error) {
                    // faço a chamada do modal aq pq o data, está como json quebrado por causa do retorno do envio do email
                    modal('alert','Uma nova senha foi gerada e enviada para o e-mail informado!');
                    $("#modOk").attr('href', '{{ route('link.eleicao', ['alias' => $_SESSION['eleicao']['alias']]) }}');
                    $("#btReenviarSenha").html('');
                }
            });


        }
    
    });
</script>
@endsection