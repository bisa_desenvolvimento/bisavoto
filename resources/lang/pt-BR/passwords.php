<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Linhas de Idioma para Lembrete de Senha
    |--------------------------------------------------------------------------
    |
    | As linhas a seguir são as mensagens padrão correspondentes aos motivos
    | que são fornecidos pelo broker de senhas para uma tentativa de
    | atualização de senha que falhou, como um token inválido ou uma nova
    | senha inválida.
    |
    */

    "password" => "As senhas devem ter pelo menos seis caracteres e corresponder à confirmação.",
    "user" => "Não conseguimos encontrar um usuário com esse endereço de e-mail.",
    "token" => "Este token de redefinição de senha é inválido.",
    "sent" => "Enviamos por e-mail o link para redefinição de senha!",
    "reset" => "Sua senha foi redefinida!",

];
