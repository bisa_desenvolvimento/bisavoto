<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Linhas de Idioma para Paginação
    |--------------------------------------------------------------------------
    |
    | As linhas a seguir são usadas pela biblioteca do paginador para construir
    | os links de paginação simples. Você pode alterá-las para qualquer coisa
    | que desejar para personalizar suas visualizações e adequá-las melhor à
    | sua aplicação.
    |
    */

    'previous' => '&laquo; Anterior',
    'next'     => 'Próximo &raquo;',

];
