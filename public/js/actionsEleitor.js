var statusEleitor;
$(document).ready(function () {
	$('#exportacaoEleitores').click(()=>{
		$('.modalExportacaoEleitores').show();
	})

    $("#cancelarExportacaoEleitores").click(
        function () {
            $('.modalExportacaoEleitores').hide();
        }
    )

    
    //   statusEleitor = setInterval('verificaStatus()',2000);
    var windowsActual = window.location.href;
    windowsActual = windowsActual.split('/');
    windowsActual = windowsActual[windowsActual.length - 1];

    document.onselectstart = new Function("return false")
    if (windowsActual == 'fim-voto') {
        myPlayer = $("#jquery_jplayer_1");
        myPlayer.jPlayer({
            ready: function () {},
            swfPath: "{{url('js')}}",
            supplied: "mp3",
            wmode: "window",
            cssSelectorAncestor: '#jp_container_1'
        });

        self.update_timer = function (event) {
            var status = event.jPlayer.status;
        };

        myPlayer.jPlayer("setMedia", {
            mp3: "{{url('mp3/finalvotacao.mp3')}}"
        }).jPlayer("play");
        myPlayer
            .jPlayer("play")
            .bind($.jPlayer.event.timeupdate, self.update_timer);
    }

    $(".letter-generate").click(
        function () {
            var urlCarta = $(this).attr('url');
            $(".gerandoCarta").show();
            gerarCarta(urlCarta);
        }
    )

    $(".mail-generate").click(
        function () {
            var url = $('#url').val();
            var urlPercentual = $("#url-percentual").attr('value');
            $('.modalTeste').hide();
            $(".enviarEmail").show();
            enviarEmailsEleitores(url);

            refreshIntervalId = setInterval(function (){
                $.ajax({
                    method: "GET",
                    url: urlPercentual
                })
                .done(function (percentual) {
                    percentual = Math.round(percentual * 100);
                    $(".progress-bar").attr("aria-valuenow", percentual);
                    $(".progress-bar").attr("style", "width:"+percentual+"%");
                    $(".progress-bar").html(percentual+"%");
                    
                    if (percentual == 100) {
                        clearInterval(refreshIntervalId);
                        $(".enviarEmail").hide();
                    } 
                });
            }, 4000);
        }
    )

});

function gerarCarta(url) {
    var zip = url.search('.zip');
    if (zip == -1) {
        $.ajax({
                method: "GET",
                url: url
            })
            .done(function (url) {
                var splitUrl = url.split('/'),
                    qtd = splitUrl.length,
                    id = splitUrl[qtd - 1];
                $(".letter_pdf").html(letraAlfabeto(id))
                gerarCarta(url);
            });
    } else {
        $(".gerandoCarta").hide();
        alert('Cartas geradas');
        location.href = url;
    }
}

function enviarEmail(url) {
    var http = url.search('http');
    if (http != -1) {
        $.ajax({
                method: "GET",
                url: url
            })
            .done(function (url) {
                console.log(url);
                var splitUrl = url.split('/'),
                    qtd = splitUrl.length,
                    id = splitUrl[qtd - 3];
                $(".letter_mail").html(letraAlfabeto(id));



                enviarEmail(url)
            });
    } else {
        $(".enviarEmail").hide();
        alert('Emails Enviados');

    }
}

function verificaStatus() {
    $.ajax({
        url: url + '/eleicao/verificarStatus/',
        success: function (data) {
            if (data.trim() == 'E') {
                window.location = url + '/urna/exibirCandidatos/';
            } else {
                console.log("Mesário");
            }
        }
    });
}



function disableselect(e) {
    return false
}

function reEnable() {
    return true
}

if (window.sidebar) {
    //document.onmousedown = disableselect Estava bloqueando o buscar do eleitor
    document.onclick = reEnable
}

function redirectToLogout() {
    location.href = referer;
}

function letraAlfabeto(id) {
    var alphabet = "abcdefghijklmnopqrstuvwxyz".split("");
    return alphabet[id];
}

function enviarEmailsEleitores(url) {
    
    var data = {
        optionPassword:  $("input:radio[name=optionPassword]:checked").val(),
        optionEmail: $("input:radio[name=optionEmail]:checked").val()
        }

    $.ajax({
        method: "GET",
        url: url,
        data: data
    }).done(function (dado) {
        
        if(dado == 1){
            alert('Ação concluída com sucesso!');
        }else if(dado < 1){
            enviarEmailsEleitores(url);
        }else if(JSON.parse(dado).length > 0){
            var naoEnviados = dado;
            const form = document.createElement('form');
            form.method = "post";
            form.action = $("#url-nao-notificados").attr("value");
          
            const hiddenField = document.createElement('input');
            hiddenField.type = 'hidden';
            hiddenField.name = "naoEnviados";
            hiddenField.value = naoEnviados;
        

            form.appendChild(hiddenField);
            form.appendChild(document.getElementById("_token"));
            document.body.appendChild(form);
            console.log(form);
            form.submit();
        }
    });
}

