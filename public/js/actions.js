    $(document).ready(function()
    {

        $('.updatePassword').off('click');
        $('.updatePassword').on('click', function(e)
        {
            e.preventDefault();
            $.ajax({
                type: 'get',
                url: $(this).attr('link'),

                success: function(response) { alert(response); }
                ,
                error: function(e) {
                    console.log(e.responseText);
                }
            });

        });

        var windowsActual = window.location.href;
        // var splitSearchAdd = windowsActual.search('add');
        // var splitSearchEdit = windowsActual.search('edit');

        if (typeof nameRoute !== "undefined") {
            if(nameRoute == 'add.eleicoes' || nameRoute == 'edit.eleicoes') {
            eventoDatePickerCadastroEleicao();
            eventoTimePickerTempoSessaoCadastroEleicao();
            }
        }
        /*if(nameRoute == 'add.eleicoes' || nameRoute == 'edit.eleicoes') {
            eventoDatePickerCadastroEleicao();
            eventoTimePickerTempoSessaoCadastroEleicao();
        }*/

        windowsActual = windowsActual.split('/');
        windowsActual = windowsActual[ windowsActual.length - 1 ];
        setInterval("verificaVotacaoEncerrada()", 25000);
        eventoValorPadraoHtmlSelect();
        confirmarExclusao();

        //$("#ele_tempo").mask('99:99');
        $(document).on("input", ".numeric", function() {
          this.value = this.value.replace(/[^0-9\.]/g,'');
      });

        $("#modCancela, #modOkErro").click(
            function()
            {
                closeModal();
            }
        )
        $("#modOk").click(
            function()
            {
                closeModal();
                location.reload();
            }
        )
        $(".glyphicon-remove").click(
            function(e)
            {
                var urlDel = $(this).attr('href');
                e.preventDefault();
                $.fn.jAlert.defaults.confirmQuestion = 'Deseja excluir este registro?';
                $.fn.jAlert.defaults.confirmBtnText = 'Sim';
                $.fn.jAlert.defaults.denyBtnText = 'Não';
                confirm(function(e,btn){ //event + button clicked
                    location.href = urlDel;
                }, function(e,btn){
                });
                return false;
            }
        )

        $('.car_restricao_zona').off('click');
        $('.car_restricao_zona').on('click', function() {
            $('.car_zona_id').toggle();
        });
    });

    function verificaVotacaoEncerrada()
    {
        var req = '';
        req = $.getJSON("verifica-votacao-encerrada");

        req.done(function(data){

            if(data.resp)
            {
               $(".classZona").attr("style", "color:blue");

               $(".classZona").click(function()
               {
                    var zona = $(this).attr("id");
                    var id = zona.split("_");


               });

                $(".apuracaofinal").show();
            }
        });

     }

    function verificarStatusExibicaoBu()
    {
        $(".classZona").click(function()
        {
            console.debug($(this));
        });
    }

    function eventoDefaultLightBox()
    {

        $('#importarCsv').ekkoLightbox();

    }
    function confirmarExclusao()
    {
        $("#exclusao").click(function(e)
        {
            e.preventDefault();

            var url = $(this).attr('href');

            var dialog = $("#dialog-confirm");
            dialog.show();

            dialog.dialog({
                resizable: false,
                height:175,
                modal: true,

            buttons:
            {
                "OK": function()
                {
                    /**
                    $.ajax({
                        type: 'post',
                        url: url,
                        data: { _method: 'DELETE' },

                        success: function(response) { alert(); }
                        ,
                        error: function(e) {
                            console.log(e.responseText);
                        }
                    });*/
                    window.location.href = url;

                    $( this ).dialog( "close" );
                },

                Cancelar: function() {
                    $( this ).dialog( "close" );
                }
            }
    });
        });
    }

    function eventoTimePickerTempoSessaoCadastroEleicao()
    {
        $("#ele_tempo").datetimepicker({
                format: 'HH:mm'
            }
        );
    }
    function eventoDatePickerCadastroEleicao()
    {
        $(".datepicker").datetimepicker({
          format: 'DD/MM/YYYY HH:mm',
          sideBySide: true
        });
    }
    function eventoValorPadraoHtmlSelect()
    {
        var htmlSelect = $("select");

        if(htmlSelect.length){
            $("select").val("");
        }
    }

    function modal(tipo,mensagem,classConfirma) {

        if(tipo == 'alert') {
            $('#modAlert').show();
            $('#confirmar').hide();
        } else {
            $('#modAlert').hide();
            $('#confirmar').show();
            $('.modConfirma').addClass(classConfirma)
        }

        $(".modMensagem").empty().html(mensagem)

        $('#modal').show();

    }


    function modalErro(tipo,mensagem) {

        console.log(mensagem)

        if(tipo == 'alert') {
            $('#modAlertErro').show();
            $('#confirmar').show();
        }

        $("#modMensagemErro").empty().html(mensagem)

        $('#modalErro').show();

    }


    function showModal(obj, data) {
        $("#modal").fadeIn(0);
        $("#all").fadeOut(0);
    }

    function closeModal() {
        $("#modal").fadeOut(0);
        $("#modalErro").fadeOut(0);
        $("#all").fadeIn(0);
    }

    function statusUrna()
    {
         $.ajax({
            url:'/urna/statusUrna',
            success: function(data) {
                if (data.trim() == 'false'){
                    $(".errorMirror").show();
                    $("#procurar").hide();
                    $("#encerrarVotacao").hide();
                } else {
                    $("#procurar").show();
                    $(".errorMirror").hide();
                    pegarHora();
                }
            }
        });

    }

    function redirectToUrl()
    {
        location.href = window.validUrl;
    }

    function reloadPage()
    {
        window.location.reload();
    }

    function UTCFromBRDate(brdate, timezone){
  		var timeUTC,
  			timeOffset;

  		if(!brdate){
  			return false;
  		}

  		timeOffset = (timezone) ? 11000000 : 0;

      brdatetime = brdate.split(' ');
      brdate = brdatetime[0].split('/');
  		brdate[2] = Math.round(brdate[2]);
  		brdate[1] = (Math.round(brdate[1]) - 1);
  		brdate[0] = Math.round(brdate[0]);
      brtime = brdatetime[1].split(' ');
      brtime[0] = Math.round(brtime[0]);
      brtime[1] = Math.round(brtime[1]);

  		timeUTC = Date.UTC(brdate[2], brdate[1], brdate[0], brtime[0], brtime[1], 0);
  		timeUTC = timeUTC + timeOffset;

  		return timeUTC;
  }

  function dateDiffEleicoes()
  {
    var one_day=1000*60*60*24,
        startDate = $('#ele_horaInicio').val(),
        endDate = $('#ele_horaTermino').val(),
        ms = moment(endDate,"DD/MM/YYYY HH:mm:ss").diff(moment(startDate,"DD/MM/YYYY HH:mm:ss")),
        d = moment.duration(ms),
        s = Math.floor(d.asHours()) + moment.utc(ms).format("ss");
    if(s < 0){
      alert('A data de início de ser menor que a data de início');
    } else {
      return true;
    }
  }

  function ocultarLink(){
    $("#linkEmissaoZeresima").attr("style", "display:none");
  }

  function showLoading(){
    $("#loadingOverlay").fadeIn();
  }

  function hideLoading(){
    $("#loadingOverlay").fadeOut();
  }