$(function()
{
	atualizarOrdemVotacaoCargos();
});

function atualizarOrdemVotacaoCargos()
{
		// Return a helper with preserved width of cells
var fixHelper = function(e, ui) {
	ui.children().each(function()
	{
		$(this).width($(this).width());
	});
	return ui;
};

$("#tabelaCargos tbody").sortable({
	items: 'tr',
	placeholder: "ui-sortable-placeholder" 
	//, helper: fixHelper
	, update: function (event, ui) 
	{
        var data = {  
        			  ordem: $(this).sortable('serialize'),
                     _token: $('meta[name="csrf-token"]').attr('content') 
    			   }

        if(data)
        {
			$.ajax(
			{
            	data: data,
            	type: 'POST',
            	url: '/update-ordem-cargo'
        	}); 
        }
       
    }
});

}
