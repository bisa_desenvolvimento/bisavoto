<?php

return [
    'app_title_default' => 'Bisavoto',
    'voters_export' => [
        'sucess' => 'Eleitores exportados com sucesso!',
        'error' => 'Erro ao exportar a lista de eleitores',
    ],
];