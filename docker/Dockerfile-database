# Imagem base
FROM mysql:8.0.36

# Definição das variáveis de ambiente feita externamente via arquivo .env
ARG MYSQL_ROOT_PASSWORD
ARG MYSQL_ROOT_HOST
ARG MYSQL_USER
ARG MYSQL_PASSWORD

# Comandos adicionados ao arquivo setup.sql para configurar os usuários como mysql_native_password (o cliente MySQL não suporta o método de autenticação caching_sha2_password)
RUN echo "ALTER USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY '${MYSQL_ROOT_PASSWORD}';" >> /docker-entrypoint-initdb.d/setup.sql
RUN echo "ALTER USER '${MYSQL_USER}'@'%' IDENTIFIED WITH mysql_native_password BY '${MYSQL_PASSWORD}';" >> /docker-entrypoint-initdb.d/setup.sql

# Comandos adicionados ao arquivo setup.sql para adicionar os privilégios ao usuário monetaweb_user
RUN echo "GRANT ALL PRIVILEGES ON . TO '${MYSQL_USER}'@'%';" >> /docker-entrypoint-initdb.d/setup.sql
RUN echo "REVOKE DROP ON . FROM '${MYSQL_USER}'@'%';" >> /docker-entrypoint-initdb.d/setup.sql
RUN echo "FLUSH PRIVILEGES;" >> /docker-entrypoint-initdb.d/setup.sql

# Comandos para definir os modos SQL
RUN echo "[mysqld]" >> /etc/mysql/my.cnf
RUN echo "sql_mode=" >> /etc/mysql/my.cnf

# Exposição da porta 3306 do container
EXPOSE 3306