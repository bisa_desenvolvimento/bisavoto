# Use a imagem base do PHP com Apache
FROM php:7.4-apache

# Definição do diretório principal
WORKDIR /var/www/html/bisavoto/public

# Atualizações e instalações necessárias para o ambiente
RUN apt-get update && \
    apt-get install -y \
    nano \
    openssl \
    libzip-dev && \
    docker-php-ext-configure zip && \
    docker-php-ext-install zip mysqli pdo_mysql && \
    apt-get clean

# Configuração do php.ini
RUN { \
    echo "allow_url_fopen = On"; \
    echo "display_errors = Off"; \
    echo "enable_dl = Off"; \
    echo "file_uploads = On"; \
    echo "max_execution_time = 0"; \
    echo "max_input_time = 60"; \
    echo "max_input_vars = 10000"; \
    echo "memory_limit = 512M"; \
    echo "post_max_size = 64M"; \
    echo "session.gc_maxlifetime = 1440"; \
    echo "upload_max_filesize = 64M"; \
    echo "zlib.output_compression = On"; \
    echo 'session.save_path = "/var/lib/php/sessions"'; \
} >> $PHP_INI_DIR/php.ini 

# Definição de um novo diretório root para o Apache
RUN sed -i 's|DocumentRoot /var/www/html|DocumentRoot /var/www/html/bisavoto/public|' /etc/apache2/sites-available/000-default.conf

# Atualização das configurações do Apache para permitir o uso de arquivos .htaccess
RUN sed -i '/<FilesMatch "\^\\.ht">/,/<\/FilesMatch>/ s/Require all denied/Require all granted/' /etc/apache2/apache2.conf
RUN sed -i '/<Directory \/var\/www\/>/,/<\/Directory>/ s/AllowOverride None/AllowOverride All/' /etc/apache2/apache2.conf

# Definição do usuário e grupo do apache
RUN echo 'APACHE_RUN_USER=webuser' >> /etc/apache2/envvars
RUN echo 'APACHE_RUN_GROUP=webgroup' >> /etc/apache2/envvars

# Ativação de módulos Apache 
RUN a2enmod headers
RUN a2enmod rewrite

# Criação de grupo com GID 1000
RUN groupadd -g 1000 webgroup

# Criação de usuário com UID 1000 e associação ao grupo webgroup
RUN useradd -u 1000 -g webgroup -m -s /bin/bash webuser

# Criação de diretório para salvar seções
RUN mkdir -p /var/lib/php/sessions \
    && chown -R webuser:webgroup /var/lib/php/sessions

# Exposição da porta 80 
EXPOSE 80

# Inicialização do Apache
CMD ["apache2-foreground"]
