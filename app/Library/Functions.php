<?php
/**
 * Created by PhpStorm.
 * User: Roque
 * Date: 23/09/2015
 * Time: 01:35
 */

namespace App\Library;
use Carbon\Carbon;

class Functions {

    public static function alfabeto($id)
    {
        $letras = range('a', 'z');
        if (count($letras) == $id){
            return ['total' => count($letras), 'letra' =>$letras[$id - 1]];
        } else {
            return ['total' => count($letras), 'letra' =>$letras[$id]];
        }
    }

    public static function convertDateTime($date)
    {
        if (trim($date) != ''):
            if (strstr($date, '-')): // Formato x-x-x
                $date = explode(" ", $date);
                $hora = $date[1];
                $date = explode('-', $date[0]);
                $date = $date[2].'/'.$date[1].'/'.$date[0].' '.$hora;
            else:
                $date = explode(" ", $date);
                $hora = $date[1];
                $date = explode('/', $date[1]);
                $date = $date[2].'-'.$date[1].'-'.$date[0].' '.$hora;
            endif;

        endif;

        return $date;

    }

    public static function getDaysInterval($date_start, $date_end, $skipdays = array(), $skipdates = array())
    {
        $date_start_timestamp = strtotime($date_start);
        $date_end_timestamp = strtotime($date_end);
        $days_diff = ceil(($date_end_timestamp - $date_start_timestamp) / 86400);
        if (count($skipdays) > 0 || count($skipdates) > 0) {
            $n_weekends = round($days_diff / 7, 0);
            if ((date("N", $date_start_timestamp) == 6 && $days_diff % 7 == 0)){
              $n_weekends++;
            }
            if($n_weekends > 1){
              $days_diff -= count($skipdays) * $n_weekends;
            }
        }
        return $days_diff;
    }

    public static function aliasGenerate($string) {
        $table = array(
            'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
            'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
            'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
            'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
            'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
            'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
            'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r', '/' => '-', ' ' => '-'
    );

    // -- Remove duplicated spaces
    $stripped = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $string);

    // -- Returns the slug
    return strtolower(strtr($string, $table));
	}

    public static function isValidAlias($string) {
		// Define the pattern based on the aliasGenerate function's output format
		$pattern = '/^[a-z0-9]+(?:-[a-z0-9]+)*$/';
	
		// Check if the string matches the pattern
		return preg_match($pattern, $string);
	}

    public static function removeAcentos($string) {
        $table = array(
            'á' => 'a', 'à' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a',
            'Á' => 'A', 'À' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A',
            'é' => 'e', 'è' => 'e', 'ê' => 'e', 'ë' => 'e',
            'É' => 'E', 'È' => 'E', 'Ê' => 'E', 'Ë' => 'E',
            'í' => 'i', 'ì' => 'i', 'î' => 'i', 'ï' => 'i',
            'Í' => 'I', 'Ì' => 'I', 'Î' => 'I', 'Ï' => 'I',
            'ó' => 'o', 'ò' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o',
            'Ó' => 'O', 'Ò' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O',
            'ú' => 'u', 'ù' => 'u', 'û' => 'u', 'ü' => 'u',
            'Ú' => 'U', 'Ù' => 'U', 'Û' => 'U', 'Ü' => 'U',
            'ç' => 'c', 'Ç' => 'C',
            'ñ' => 'n', 'Ñ' => 'N'
        );

        return strtr($string, $table);
    }

    public static function formatarTelefone($telefone){
        $telefone = preg_replace('/\D/', '', $telefone);
        
        if(strlen($telefone) == 11) {
            return preg_replace('/(\d{2})(\d{5})(\d{4})/', '($1) $2-$3', $telefone);
        }

        elseif (strlen($telefone) == 10) {
            return preg_replace('/(\d{2})(\d{4})(\d{4})/', '($1) $2-$3', $telefone);
        }

        return $telefone;
    }

    public static function desformatarTelefone($telefone){
        return preg_replace('/\D/', '', $telefone);
    }
    public static function formatarDataDateTime($data)
	{
		$retorno = false;

		if(strlen($data))
		{
		   $data    = str_replace("/","-",$data);
		   $retorno = date('Y-m-d H:i:s', strtotime($data) );
		}
		return $retorno;
	}

}
