<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use App\Http\Controllers\EleicoesController;

Route::get('/', 'HomeController@index');

Route::get('home', 'HomeController@index');
Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);
Route::group(['middleware' => 'auth'], function () {
    Route::get('votar', ['as' => 'votar', 'uses' => 'EleitorController@votar']);
    $user = Auth::user();

    // Eleições
    // # POST
    Route::post('salvar-eleicoes', ['as' => 'salvar.eleicoes', 'uses' => 'EleicoesController@store']);
    Route::post('update-eleicoes', ['as' => 'update.eleicoes', 'uses' => 'EleicoesController@update']);
    Route::get('filtro-eleicoes', ['as' => 'filtro.eleicoes', 'uses' => 'EleicoesController@filtroEleicoes']);

    // # GET
    Route::get('lista-eleicoes', ['as' => 'lista.eleicoes', 'uses' => 'EleicoesController@lista']);
    Route::get('eleicao/{idEleicao}/pausar', ['as' => 'pausar.eleicoes', 'uses' => 'EleicoesController@setPausaEleicao']);
    Route::get('addeleicoes', ['as' => 'add.eleicoes', 'uses' => 'EleicoesController@add']);

    Route::get('editeleicoes/idEleicao/{idEleicao}', ['as' => 'edit.eleicoes', 'uses' => 'EleicoesController@edit']);

    Route::get(
        'delete-eleicao/idEleicao/{idEleicao}',
        [
            'as' => 'delete.eleicao',
            'uses' => 'EleicoesController@destroy'
        ]
    );

    Route::get('pg-eleicao/{idEleicao}', ['as' => 'pagina.eleicao', 'uses' => 'EleicoesController@index']);

    Route::get('fim-voto', ['as' => 'fim.voto', 'uses' => 'EleicoesController@end']);

    Route::get(
        'verifica-votacao-encerrada',
        ['as' => 'verifica.votacao.encerrada', 'uses' => 'EleicoesController@verificarEleicaoEncerrada']
    );
    // Usuários
    Route::get('usuarios', ['as' => 'lista.usuarios', 'uses' => 'UsersController@list']);
    Route::get('addusuarios', ['as' => 'add.usuarios', 'uses' => 'UsersController@add']);
    Route::get('editusuarios', ['as' => 'edit.usuarios', 'uses' => 'UsersController@edit']);
    Route::get('saveusuarios', ['as' => 'save.usuarios', 'uses' => 'UsersController@save']);

    // Zona
    Route::get('zona', ['as' => 'lista.zona', 'uses' => 'ZonasController@index']);
    Route::get('addzona', ['as' => 'add.zona', 'uses' => 'ZonasController@add']);
    Route::get('editzona', ['as' => 'edit.zona', 'uses' => 'ZonasController@adit']);
    Route::get('savezona', ['as' => 'save.zona', 'uses' => 'ZonasController@save']);
    Route::get('savezona', ['as' => 'save.zona', 'uses' => 'ZonasController@save']);

    // Candidatos
    Route::get('lista-candidatos', ['as' => 'lista.candidatos.eleitor', 'uses' => 'CandidatosController@listaParaEleitor']);

    Route::get('candidatos', ['as' => 'lista.candidatos', 'uses' => 'CandidatosController@lista']);
    Route::post('salvar-votos', ['as' => 'salvar.votos', 'uses' => 'EleicoesController@salvarVotos']);

    Route::get('edit-candidato/idCargo/{idCargo}/idCandidato/{idCandidato}', ['as' => 'edit.candidato.comissao', 'uses' => 'CandidatosController@edit']);
    Route::get('edit-candidato/idCargo/{idCargo}/idCandidato/{idCandidato}/idEleicao/{idEleicao}', ['as' => 'edit.candidato', 'uses' => 'CandidatosController@edit']);
    Route::get('lista-candidato/idCargo/{idCargo}', ['as' => 'lista.candidatos.comissao', 'uses' => 'CandidatosController@lista']);
    Route::get('lista-candidato/idCargo/{idCargo}/idEleicao/{idEleicao}', ['as' => 'lista.candidatos', 'uses' => 'CandidatosController@lista']);
    Route::get('add-candidato/idCargo/{idCargo}', ['as' => 'add.candidato.comissao', 'uses' => 'CandidatosController@add']);
    Route::get('add-candidato/idCargo/{idCargo}/idEleicao/{idEleicao}', ['as' => 'add.candidato', 'uses' => 'CandidatosController@add']);
    Route::get('delete-candidato/idCargo/{idCargo}/idCandidato/{idCandidato}', ['as' => 'delete.candidato.comissao', 'uses' => 'CandidatosController@destroy']);
    Route::get('delete-candidato/idCargo/{idCargo}/idCandidato/{idCandidato}/idEleicao/{idEleicao}}', ['as' => 'delete.candidato', 'uses' => 'CandidatosController@destroy']);

    Route::post('salvar-candidatos', ['as' => 'salvar.candidato', 'uses' => 'CandidatosController@store']);
    Route::post('update-candidato', ['as' => 'editar.candidato', 'uses' => 'CandidatosController@update']);


    // Eleitores
    Route::post('exportar-eleitores', ['as' => 'exportar.eleitores', 'uses' => 'EleitorController@exportarEleitores']);

    Route::get('lista-eleitores', ['as' => 'lista.eleitores.comissao', 'uses' => 'EleitorController@index']);
    Route::get('lista-eleitores/idEleicao/{idEleicao}', ['as' => 'lista.eleitores', 'uses' => 'EleitorController@index']);

    Route::get('add-eleitores', ['as' => 'add.eleitor.comissao', 'uses' => 'EleitorController@add']);
    Route::get('add-eleitores/idEleicao/{idEleicao}', ['as' => 'add.eleitor', 'uses' => 'EleitorController@add']);

    Route::get('notificar-eleitores/letra/{letra}', ['as' => 'notificar.eleitor.comissao', 'uses' => 'EleicoesController@enviarEmails']);
    Route::get('notificar-eleitores/idEleicao/{idEleicao}', ['as' => 'notificar.eleitor', 'uses' => 'EleicoesController@enviarEmailsEleitores']);
    Route::get('notificar-eleitores/{idEleicao}/sms/{optionPassword}', ['as' => 'notificar.eleitor.sms', 'uses' => 'EleicoesController@gerarExcelSMS']);
    Route::get('notificar-eleitores/percentual-eleitores-notificados/{idEleicao}', ['as' => 'percentual.eleitores.notificados', 'uses' => 'EleicoesController@percentualDeEleitoresDeNotificados']);
    Route::post('notificar-eleitores/nao-notificados/{idEleicao}', ['as' => 'notificar.naonotificados', 'uses' => 'EleicoesController@naoNotificados']);

    Route::get('link/{idEleicao}', ['as' => 'redirecionar.link.eleicao', 'uses' => 'EleicoesController@redirecionarLinkEleicao']);

    Route::get('edit-eleitor/idEleicao/{idEleicao}/idEleitor/{idEleitor}', ['as' => 'edit.eleitor.comissao', 'uses' => 'EleitorController@edit']);
    Route::post('salvar-eleitor', ['as' => 'salvar.eleitor', 'uses' => 'EleitorController@store']);
    Route::post('update-eleitor', ['as' => 'editar.eleitor', 'uses' => 'EleitorController@update']);

    Route::get('link/{idEleicao}', ['as' => 'redirecionar.link.eleicao', 'uses' => 'EleicoesController@redirecionarLinkEleicao']);
    Route::post('gerar-nova-senha-eleitor', ['as' => 'gerar.nova.senha.eleitor', 'uses' => 'EleicoesController@gerarNovaSenhaEleitor']);

    Route::get('importar-eleitores/idEleicao/{idEleicao}', ['as' => 'importar.eleitores', 'uses' => 'EleitorController@importarEleitores']);
    Route::post('processarArquivoEleitores', ['as' => 'importar.arquivo.eleitores', 'uses' => 'EleitorController@processarArquivoEleitores']);
    Route::post('importarArquivo', ['as' => 'importar2.arquivo.eleitores', 'uses' => 'EleitorController@importarArquivo']);
    Route::get('processararquivojax', ['as' => 'importar2.arquivo.eleitores.processararquivojax', 'uses' => 'EleitorController@processararquivojax']);
    Route::get('resultadoImportacao', ['as' => 'importar2.arquivo.eleitores.resultado', 'uses' => 'EleitorController@resultadoImportacao']);

    Route::get(
        'delete-eleitor/iEleicao/{idEleicao}/idEleitor/{idEleitor}',
        [
            'as' => 'delete.eleitor.comissao',
            'uses' => 'EleitorController@destroy'
        ]
    );

    Route::get('carta-eleitores/letra/{letra}', ['as' => 'carta.eleitor.comissao', 'uses' => 'EleitorController@gerarCarta']);
    Route::get('carta-eleitores/letra/{letra}/idEleicao/{idEleicao}', ['as' => 'carta.eleitor', 'uses' => 'EleitorController@gerarCarta']);
    // ******* Cargos ******** //
    // # GET

    Route::get('lista-cargos', ['as' => 'lista.cargos.comissao', 'uses' => 'CargosController@index']);
    Route::get('lista-cargos/idEleicao/{idEleicao}', ['as' => 'lista.cargos', 'uses' => 'CargosController@index']);

    Route::get('add-cargos', ['as' => 'add.cargos.comissao', 'uses' => 'CargosController@add']);
    Route::get('add-cargos/idEleicao/{idEleicao}', ['as' => 'add.cargos', 'uses' => 'CargosController@add']);

    Route::get('edit-cargos/idCargo/{idCargo}/idEleicao/{idEleicao}', ['as' => 'edit.cargos', 'uses' => 'CargosController@edit']);
    Route::get('edit-cargos/idCargo/{idCargo}', ['as' => 'edit.cargos.comissao', 'uses' => 'CargosController@edit']);

    Route::get(
        'delete-cargos/idCargo/{idCargo}/idEleicao/{idEleicao}',
        [
            'as' => 'delete.cargos',
            'uses' => 'CargosController@destroy'
        ]
    );
    Route::get(
        'delete-cargos/idCargo/{idCargo}',
        [
            'as' => 'delete.cargos.comissao',
            'uses' => 'CargosController@destroy'
        ]
    );
    // # POST
    Route::post(
        'salvar-cargo',
        [
            'as' => 'salvar.cargo',
            'uses' => 'CargosController@store'
        ]
    );

    Route::post(
        'update-cargo',
        [
            'as' => 'update.cargo',
            'uses' => 'CargosController@update'
        ]
    );

    Route::post(
        'update-ordem-cargo',
        [
            'as' => 'update.ordem.cargo',
            'uses' => 'CargosController@updateOrdemCargo'
        ]
    );

    Route::get(
        'enviar-email/idEleicao/{idEleicao}',
        [
            'as' => 'enviar.email',
            'uses' => 'EleicoesController@enviar'
        ]
    );

    Route::get(
        'enviar-email',
        [
            'as' => 'enviar.email.comissao',
            'uses' => 'EleicoesController@enviar'
        ]
    );

    //Zonas
    // #GET
    Route::get('lista-zonas/idEleicao/{idEleicao}', ['as' => 'lista.zonas', 'uses' => 'ZonaController@index']);
    Route::get('lista-zonas', ['as' => 'lista.zonas.comissao', 'uses' => 'ZonaController@index']);
    Route::get('add-zona', ['as' => 'add.zona.comissao', 'uses' => 'ZonaController@add']);
    Route::get('add-zona/idEleicao/{idEleicao}', ['as' => 'add.zona', 'uses' => 'ZonaController@add']);
    Route::get('edit-zona/idZona/{idZona}', ['as' => 'edit.zona.comissao', 'uses' => 'ZonaController@edit']);
    Route::get('edit-zona/idZona/{idZona}/idEleicao/{idEleicao}', ['as' => 'edit.zona', 'uses' => 'ZonaController@edit']);
    Route::get('delete-zona/idZona/{idZona}/idEleicao/{idEleicao}', ['as' => 'delete.zona', 'uses' => 'ZonaController@destroy']);
    Route::get('delete-zona/idZona/{idZona}', ['as' => 'delete.zona.comissao', 'uses' => 'ZonaController@destroy']);

    // #POST
    Route::post('salvar-zona', ['as' => 'salvar.zona', 'uses' => 'ZonaController@store']);
    Route::post(
        'update-zona',
        [
            'as' => 'update.zona',
            'uses' => 'ZonaController@update'
        ]
    );

    Route::get(
        'delete-zona/idZona/{idZona}',
        [
            'as' => 'delete.zona.comissao',
            'uses' => 'ZonaController@destroy'
        ]
    );

    // Comissão Eleitoral
    Route::get('lista-comissao/idEleicao/{idEleicao}', ['as' => 'lista.comissao', 'uses' => 'ComissaoController@index']);
    Route::get('lista-comissao', ['as' => 'lista.comissao.comissao', 'uses' => 'ComissaoController@index']);
    Route::get('add-comissao/idEleicao/{idEleicao}', ['as' => 'add.comissao', 'uses' => 'ComissaoController@add']);
    Route::get('add-comissao', ['as' => 'add.comissao.comissao', 'uses' => 'ComissaoController@add']);

    Route::get('edit-comissao/idEleicao/{idEleicao}/idComissao/{idComissao}', ['as' => 'edit.comissao', 'uses' => 'ComissaoController@edit']);
    Route::get('edit-comissao/', ['as' => 'edit.comissao.comissao', 'uses' => 'ComissaoController@edit']);
    Route::get('delete-comissao/idEleicao/{idEleicao}/idComissao/{idComissao}', ['as' => 'delete.comissao', 'uses' => 'ComissaoController@destroy']);
    Route::get('delete-comissao', ['as' => 'delete.comissao.comissao', 'uses' => 'ComissaoController@delete']);

    Route::post('salvar-comissao', ['as' => 'salvar.comissao.comissao', 'uses' => 'ComissaoController@store']);
    Route::post('update-comissao', ['as' => 'update.comissao.comissao', 'uses' => 'ComissaoController@update']);

    Route::get('resumo/idEleicao/{idEleicao}', ['as' => 'resumo', 'uses' => 'ResumoController@index']);
    Route::get('resumo', ['as' => 'resumo.comissao', 'uses' => 'ResumoController@index']);
    /*
    Route::get('apuracao/idEleicao/{idEleicao}', ['as' => 'apuracao', 'uses' => 'ResumoController@apuracao']);
    Route::get('apuracao', ['as' => 'apuracao.comissao', 'uses' => 'ResumoController@apuracao']);
    */
    Route::get('apuracao/idEleicao/{idEleicao}', ['as' => 'apuracaofinal', 'uses' => 'ResumoController@apuracaofinal']);
    Route::get('apuracao', ['as' => 'apuracaofinal.comissao', 'uses' => 'ResumoController@apuracaofinal']);

    Route::get('apuracao-eleicao/idEleicao/{idEleicao}', ['as' => 'resumo.apuracao.eleicao', 'uses' => 'ResumoController@apuracaofinaleleicao']);
    Route::get('apuracao-eleicao', ['as' => 'resumo.apuracao.eleicao.comissao', 'uses' => 'ResumoController@apuracaofinaleleicao']);

    //Relatorios
    Route::get('apuracao-eleicao/relatorio-aptos/{idEleicao}', ['as' => 'resumo.relatorio.aptos', 'uses' => 'ResumoController@relatorioAptos']);
    Route::get('apuracao-eleicao/relatorio-aptos-zona/{idEleicao}', ['as' => 'resumo.relatorio.aptos.zona', 'uses' => 'ResumoController@relatorioAptosZona']);
    Route::get('apuracao-eleicao/relatorio-votantes/{idEleicao}', ['as' => 'resumo.relatorio.votantes', 'uses' => 'ResumoController@relatorioVotantes']);
    Route::get('apuracao-eleicao/relatorio-votantes-final/{idEleicao}', ['as' => 'resumo.relatorio.votantes.final', 'uses' => 'ResumoController@relatorioVotantesFinal']);

    Route::get('apuracao-zona/idEleicao/{idEleicao}', ['as' => 'resumo.apuracao.zona', 'uses' => 'ResumoController@apuracaofinal']);
    Route::get('apuracao-zona', ['as' => 'resumo.apuracao.zona.comissao', 'uses' => 'ResumoController@apuracaofinal']);

    // New refactored routes

    Route::get('eleitor/importacao/erros/', ['as' => 'eleitor.importacao.erros', 'uses' => 'EleitorController@importacaoErros']);
});

Route::get('adicionar-eleitor-durante-votacao/{idEleicao}', ['as' => 'adicionar.eleitor.durante.votacao', 'uses' => 'EleicoesController@adicionarEleitorDuranteVotacao']);
Route::post('salvar-eleitor-durante-votacao', ['as' => 'salvar.eleitor.durante.votacao', 'uses' => 'EleitorDuranteVotacaoController@store']);
Route::post('/verificar-duplicado-ou-votou', 'EleitorDuranteVotacaoController@verificarDuplicadoOuVotou');

Route::get('enviar-email/idEleicao/{idEleicao}',
    ['as' => 'enviar.email',
        'uses' => 'EleicoesController@enviarEmails']);

Route::get(
    'enviar-email/idEleicao/{idEleicao}',
    [
        'as' => 'gerar.carta',
        'uses' => 'EleicoesController@gerarCarta'
    ]
);

Route::get(
    'enviar-email',
    [
        'as' => 'enviar.email.comissao',
        'uses' => 'EleicoesController@enviarEmails'
    ]
);

Route::get('logout', ['as' => 'user.logout', 'uses' => 'Auth\AuthController@getLogout']);
Route::get('reenviarsenha', ['as' => 'user.reenviarsenha', 'uses' => 'EleicoesController@linkReenviarsenhaFora']);

Route::get('eleicao/{alias}', ['as' => 'link.eleicao', 'uses' => 'EleicoesController@votacao']);
Route::get('eleicao/{alias}/reenviarsenha', ['as' => 'link.eleicao.linkReenviarSenha', 'uses' => 'EleicoesController@linkReenviarsenha']);
Route::post('eleicao/{alias}/salvarReenviarsenha', ['as' => 'link.eleicao.reenviarsenha', 'uses' => 'EleicoesController@salvarReenviarsenha']);

Route::get('eleitor/reenviar-dados-acesso/{idEleitor}/{idEleicao}', ['as' => 'user.resetar.acesso', 'uses' => 'EleicoesController@enviarEmailEleitor']);

Route::get('eleitor/reenviar-dados-acesso2/{idEleitor}/{idEleicao}', ['as' => 'user.resetar2', 'uses' => 'EleicoesController@enviarEmailEleitor2']);

Route::get('eleitor/reenviar-dados-acesso2/{idEleitor}/{idEleicao}/{valoresForm}', ['as' => 'user.reenvioSenha', 'uses' => 'EleicoesController@enviarEmailReenvioSenha']);

Route::get('eleicao/emitir-zeresima/{idEleicao}', ['as' => 'eleicao.emitir.zeresima', 'uses' => 'ResumoController@emitirZeresima']);

// End Old Routes

Route::get('apuracao-eleicao/relatorio-votantes-zona/{idEleicao}', ['as' => 'resumo.relatorio.votantes.zona', 'uses' => 'ResumoController@relatorioVotantesPorZona']);
Route::get('ocorrencias/{idEleicao}', ['as' => 'resumo.relatorio.ocorrencias', 'uses' => 'ResumoController@relatorioOcorrencias']);
Route::get('email_alterados/{idEleicao}', ['as' => 'resumo.relatorio.email_alterados', 'uses' => 'ResumoController@eleitoresEmailsAlterados']);

Route::get('apuracao-eleicao-zona/idEleicao/{idEleicao}', ['as' => 'resumo.relatorio.apuracaofinalporzona', 'uses' => 'ResumoController@apuracaofinaleleicaoporzona']);

Route::get('apuracao-eleicao-zona', ['as' => 'resumo.relatorio.apuracaofinalporzona.comissao', 'uses' => 'ResumoController@apuracaofinaleleicaoporzona']);

Route::get('{nomeEleicao}/parcial/{idEleicao}', ['as' => 'resumo.relatorio.resultadoparcial', 'uses' => 'ResumoController@resultadoParcial']);