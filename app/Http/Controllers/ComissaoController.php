<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
use Input;
use Hash;
use Illuminate\Http\Request;
use App\Http\Models\Comissao as Comissao;

class ComissaoController extends Controller {

    private $model;
    private $profile_id;
    private $ele_id;

    public function __construct()
    {
        if(is_null($this->model)){
            $this->model = new Comissao();
        }
        $this->profile_id = Auth::user()->profile_id;
        $this->ele_id = Auth::user()->ele_id;
    }

    public function index($ele_id = null)
    {
        if ($this->profile_id == 2){
            $ele_id = $this->ele_id;
        }
		
		Session::forget('ele_id');
		Session::put('ele_id', $ele_id);
        $listaComissao = $this->model->listComissao($ele_id);

        $data = [
            'listaComissao' => $listaComissao,
            'ele_id' => $ele_id
        ];
        return view('comissao.lista-comissao', with($data) );
    }

    public function add($ele_id = null)
    {
        if ($this->profile_id == 2){
            $ele_id = $this->ele_id;
        }
		Session::forget('ele_id');
		Session::put('ele_id', $ele_id);
    	$data = [
            'id'    => '',
            'name'=> '',
            'login'=> '',
            'senha'=>'',
            'ele_id' => $ele_id
        ];

    	return view('comissao.add-comissao', with($data) );
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		
	}
	private function hashSenha($senha)
	{
		if (strlen($senha) && Hash::needsRehash($senha))
		{
    		$senha = Hash::make('secret');
		}
		return $senha;
	}
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$valoresForm = array_map('trim', Input::all());

		// dd($valoresForm);

		$objComissao 			 = new Comissao();
		$objComissao->id         = $valoresForm['id'];
		$objComissao->ele_id     = $valoresForm['ele_id'];
		$objComissao->profile_id = $valoresForm['perfil_id'];
		$objComissao->name       = $valoresForm['name'];
		$objComissao->login      = $valoresForm['login'];
		$objComissao->matricula  = $valoresForm['login'];
		$objComissao->cpf  = $valoresForm['login'];

		$objComissao->password   = Hash::make($valoresForm['senha']);

		$verificarLoginExiste = $this->verificarLoginExiste($objComissao);

		if($verificarLoginExiste)
		{
			Session::put('msgErro', 'Login já existente! Por favor tente novamente');
			
			return redirect()->back();
		}
		else
		{
			$objComissao->save();
			
			Session::forget('msgErro');

			return redirect()->route('lista.comissao', [$valoresForm['ele_id']] );
		}

		
	}
	
	public function verificarLoginExiste($objComissao)
	{
		$retorno = false;

		if(strlen($objComissao->login))
		{
			$retorno = $this->model->verificarLoginExiste($objComissao);
		}
		return $retorno;
	}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($idEleicao, $idComissao)
	{
		$comissao = $this->model->get($idEleicao, 
									  $idComissao);

		if($comissao[0]->id)
		{
			$data = ['id'=>$comissao[0]->id, 
					 'name'=>$comissao[0]->name,
					 'login'=>$comissao[0]->login,
					 'senha'=>$comissao[0]->password,
					 'ele_id'=>$comissao[0]->ele_id,
					 'perfil_id'=>$comissao[0]->profile_id
					];

			return view('comissao.add-comissao', with($data) );		
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		Session::forget('msgErro');

		$valoresForm = array_map('trim', Input::all());

		$idEleicao = $valoresForm['ele_id'];
		$idComissao = $valoresForm['id'];

		$comissao = $this->model->get($idEleicao, 
									  $idComissao);
		if($comissao[0]->id)
		{
			$comissao[0]->name       = $valoresForm['name'];
			$comissao[0]->login      = $valoresForm['login'];
			$comissao[0]->password   = Hash::make($valoresForm['senha']);;

			

			$verificarLoginExiste = $this->verificarLoginExiste($comissao[0]);

		if($verificarLoginExiste)
		{
			Session::put('msgErro', 'Login já existente! Por favor tente novamente');
			
			return redirect()->back();
		}
		else
		{
			$comissao[0]->save();
			
			Session::forget('msgErro');

			return redirect()->route('lista.comissao', [$valoresForm['ele_id']] );
		}

		}	
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($idEleicao, $idComissao)
	{

		$comissao = Comissao::where('ele_id', $idEleicao)
        					->findOrFail($idComissao);
        if($comissao->delete())
        {
        	return redirect()->route('lista.comissao', [$idEleicao] );
        }					
	}

}
