<?php namespace App\Http\Controllers;

use Input;
use Redirect;

use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Auth;
use Illuminate\Http\Request;

use App\Http\Models\Cargo as Cargo;
use App\Http\Models\Zonas as Zonas;

class CargosController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

    private $model;
    private $profile_id;
    private $ele_id;
    private $zonas;

    public function __construct()
    {
        if(is_null($this->model)){
            $this->model = new Cargo();
        }
        $this->profile_id = Auth::user()->profile_id;
        $this->ele_id = Auth::user()->ele_id;
        $this->zonas = new Zonas();
    }


    public function index($ele_id = null)
	{
        if ($this->profile_id == 2){
            $ele_id = $this->ele_id;
        } else {
			Session::put('ele_id', $ele_id);
		}
		
     	$listaCargos = $this->model->listCargos($ele_id);
		 
        $data = [
            'listaCargos' => $listaCargos,
            'ele_id' => $ele_id
        ];

		return view('cargos.lista-cargos', with($data) );
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function add($ele_id = null)
	{
		$id = $car_nome = $car_votos = $car_zona_id = '';
		$car_restricao_zona = 0;
        if ($this->profile_id == 2){
            $ele_id = $this->ele_id;
		}

		$zonas = $this->zonas->listZonas($ele_id);

        return view('cargos.add-cargos', compact('id', 'ele_id', 'car_nome', 'car_votos', 'car_restricao_zona', 'car_zona_id', 'zonas'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$valoresForm = array_map('trim', Input::all());

		$valoresForm['car_ordem'] = 1;


		Cargo::create($valoresForm);
		
        if ($this->profile_id == 2){
            return redirect()->route('lista.cargos.comissao');
        }
        return redirect()->route('lista.cargos', [$valoresForm['ele_id']] );

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($idCargo, $idEleicao = null)
	{
        if ($this->profile_id == 2){
            $idEleicao = $this->ele_id;
        }
        $cargo = $this->model->get($idCargo, $idEleicao);
        
        $id = $cargo[0]->car_id;
        $car_nome = $cargo[0]->car_nome;
        $car_votos = $cargo[0]->car_votos;
        $car_restricao_zona = $cargo[0]->car_restricao_zona;
        $car_zona_id = $cargo[0]->car_zona_id;
        
		$ele_id = $idEleicao;
		$zonas = $this->zonas->listZonas($ele_id);

        return view('cargos.add-cargos', compact('id', 'ele_id', 'car_nome', 'car_votos', 'car_restricao_zona', 'car_zona_id', 'zonas'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
        $valoresForm = array_map('trim', Input::all());

        if ($valoresForm['car_restricao_zona'] == 0){
        	$valoresForm['car_zona_id'] = null;
        }

        $this->model->updateData($valoresForm);

        if ($this->profile_id == 2){
            return redirect()->route('lista.cargos.comissao');
        }
        
        return redirect()->route('lista.cargos', [$valoresForm['ele_id']] );

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($idCargo, $idEleicao = null)
	{
        if ($this->profile_id == 2)
        {
            $idEleicao = $this->ele_id;
        }
		$cargo = Cargo::where('ele_id', $idEleicao)
						->where('car_id', $idCargo)
						->firstOrFail();
        
        if($cargo->delete())
        {
            if ($this->profile_id == 2)
            {
                return redirect()->route('lista.cargos.comissao');
            }

            return redirect()->route('lista.cargos',
								 	 [$idEleicao] );
        } 
	}

	public function updateOrdemCargo()
	{
		$valores = array_map('trim', Input::only("ordem"));

		$this->model->updateOrdemCargo($valores);
	}


}
