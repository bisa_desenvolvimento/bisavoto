<?php

namespace App\Http\Controllers;

use App\Http\Models\Eleicao;
use App\Http\Models\Eleitor;
use App\Http\Models\Endereco;
use App\Http\Models\Log as Log;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class EleitorDuranteVotacaoController extends Controller
{
    private $model;
    private $modelEndereco;
    private $log;
    private $eleicoesController;
    private $modelUser;
    private $path;

    public function __construct()
    {
        if (is_null($this->model)) {
            $this->model = new Eleitor();
            $this->modelEndereco = new Endereco();
            $this->log = new Log();
        }

        $this->eleicoesController = new EleicoesController();
        $this->modelUser = new User();

        $this->path = public_path();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function validarEleitor($eleId, $cpf, $email){
        $validacao = [];
        $validacao["status"] = true;

        $jaVotou = $this->modelUser->jaVotou($cpf, $eleId);
        $cpfDuplicado = $this->modelUser->verificarCpfDuplicado($cpf, $eleId);

        if($cpfDuplicado){
            $emailBase = $this->modelUser->pegarEmailPorCpf($cpf, $eleId);
            $validacao["status"] = false;
            $validacao["mensagens"][] = "Você já está cadastrado no banco de eleitores. Enviamos para o e-mail ". $emailBase . "";
        }
        
        if($cpfDuplicado && $jaVotou){
             $validacao["status"] = false;
             $validacao["mensagens"][] = "Você já votou nesta eleição. Caso tenha esquecido sua senha, entre em contato com o suporte. ";
        }

        $eleicaoModel = new Eleicao();
        $eleicaoPermiteEmailDuplicado = $eleicaoModel->verificarOpcaoEmailDuplicado($eleId);

        if($eleicaoPermiteEmailDuplicado !== 1 && $email != "sememail@sememail.com.br"){
            if(isset($this->model->getByEmailAndEleId($email, $eleId)->email)){
                $validacao["status"] = false;
                $validacao["mensagens"][] = "E-mail já cadastrado. Use um e-mail diferente. ";
            }
        }

        return $validacao;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {       

        try{
            $valoresForm = array_map('trim', Input::all());

            $eleicaoModel = new Eleicao();
            $eleicao = $eleicaoModel->get($valoresForm['ele_id'])[0];
        
            $validacao = $this->validarEleitor($valoresForm['ele_id'], $valoresForm['cpf'],$valoresForm['email']);

            if(!$validacao["status"]){
                return response()->json(['sucesso' => false, "message" => $validacao["mensagens"]]);
            }

            $objEleitor 			  = new Eleitor();
            $objEleitor->ele_id 	  = $valoresForm['ele_id'];
            $objEleitor->name   	  = $valoresForm['name'];
            $objEleitor->matricula    = $valoresForm['matricula'];
            $objEleitor->email  	  = $valoresForm['email'];
            $objEleitor->cpf  	  	  = $valoresForm['cpf'];
            $objEleitor->profile_id   = $valoresForm['profile_id'];
            $objEleitor->zona_id      = $valoresForm['zona_id'];

            $objEleitor->save();

            $idEleitor = $objEleitor->id;


            if ($idEleitor) {
                $objEndereco = new Endereco();
                $objEndereco->users_id = $idEleitor;
                $objEndereco->logradouro = $valoresForm['logradouro'] ?? '';
                $objEndereco->cidade = $valoresForm['cidade'] ?? '';
                $objEndereco->uf = $valoresForm['uf'] ?? '';
                $objEndereco->cep = $valoresForm['cep'] ?? '';
                $objEndereco->telefone = $valoresForm['telefone'] ?? '';

                $objEndereco->save();

                $idUsuario = $idEleitor;
                $idRegistro = $objEleitor->ele_id;
                if ($valoresForm['zona_id'] == '999'){
                    $this->log->salvar('ELEITOR_MODEL_ELEITOR::INCLUSÃO DE ELEITOR APÓS INÍCIO DA ELEIÇÃO', $idRegistro, $idUsuario, array('Nome do Eleitor' => $objEleitor->name),$this->termo());
                } else {
                    $this->log->salvar('ELEITOR_MODEL_ELEITOR::INCLUSÃO DE ELEITOR', $idRegistro, $idUsuario, array('Nome do Eleitor' => $objEleitor->name));
                }
            }
          
            if ($valoresForm['zona_id'] == '999'){
                ob_start(); // Inicia o buffer de saída
                $this->eleicoesController->enviarEmailEleitor2($idEleitor, $valoresForm['ele_id'],$valoresForm['zona_id'],$this->termo());     
                ob_clean(); // Limpa o buffer de saída para enviar o json de resposta.
            }
            
            session(['eleicaoAlias' => $eleicao->alias]);

            return response()->json(['sucesso' => true]);
        }catch(Exception $e){
            return response()->json(["message"=>$e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function erroPath()
    {
        return $_SERVER['HTTP_REFERER'];
    }

    public function termo()
    {
        return "<p>Declaração de Veracidade e Compromisso do Eleitor</p><p>Ao prosseguir com o processo de cadastro e votação eletrônica oferecido pela Bisa, eu, como eleitor, declaro e confirmo que todas as informações fornecidas durante este processo são verdadeiras, precisas e pertencem exclusivamente a mim. Compreendo que a autenticidade e a propriedade dessas informações são fundamentais para a integridade do processo de votação.</p><p>Estou ciente de que minha participação neste processo de votação eletrônica está sujeita às legislações aplicáveis, incluindo, mas não se limitando a legislações específicas relacionadas à votação eletrônica, fraude eleitoral, falsidade ideológica, etc, bem como quaisquer outros termos jurídicos e regulamentos que governam este ato eleitoral.</p><p>Reconheço que a violação destas declarações pode resultar em consequências legais, incluindo penalidades sob as leis aplicáveis. Comprometo-me a cumprir com todos os termos, condições, leis e regulamentos relevantes durante minha participação neste processo de votação.</p><p>Ao fornecer meu endereço de e-mail e demais informações solicitadas, dou meu consentimento para receber o link e a senha de votação neste endereço eletrônico e aceito que esta será a forma de comunicação oficial para qualquer assunto relacionado à votação.</p><p>Por este meio, reafirmo meu compromisso com a integridade e a veracidade do processo eleitoral conduzido pela Bisa, garantindo que meu voto e minha participação sejam realizados de maneira ética e responsável.</p>";
    }

    public function verificarDuplicadoOuVotou(Request $request)
{
    $cpf = $request->input('cpf');
    $ele_id = $request->input('ele_id');

    $duplicado = $this->modelUser->verificarCpfDuplicado($cpf, $ele_id);
    $jaVotou = $this->modelUser->jaVotou($cpf, $ele_id);
    $emailBase = $this->modelUser->pegarEmailPorCpf($cpf, $ele_id);

    $response = [];

    if ($jaVotou && $duplicado) {
        $response['message'] = "Você já votou nesta eleição. Caso tenha esquecido sua senha, entre em contato com o suporte.";
        $response['status'] = 'error';
    } else if ($duplicado) {
        $response['message'] = "Você já está cadastrado no banco de eleitores. Enviamos para o e-mail " . $emailBase;
        $response['status'] = 'error';
    } else {
        $response['status'] = 'success';
    }

    return response()->json($response);
}
}
