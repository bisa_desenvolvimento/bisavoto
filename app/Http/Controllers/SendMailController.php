<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use PHPMailer;

class SendMailController extends Controller {

    public $subject;
    public $body;
    public $from;
    public $fromName;
    public $to;
    public $mail;
    
    public function configAWS($subject = null, $body = null, $from = null, $fromName = null, $to = null)
    {
        $this->subject = $subject;
        $this->body = $body;
        $this->from = $from;
        $this->fromName = $fromName;
        $this->to = $to;

        $this->mail = new PHPMailer();
        $this->mail->isSMTP();
        // Set mailer to use SMTP
        $this->mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $this->mail->Host = 'email-smtp.us-east-1.amazonaws.com';  // Specify main and backup SMTP servers
        $this->mail->SMTPAuth = true;                               // Enable SMTP authentication
        $this->mail->Username = 'AKIA4GCZDZAEWHRDINJL';                 // SMTP username
        $this->mail->Password = 'BB3mzUgA7zQRGuso3JigmrtbgC8ZPsgu+mAZyFCpy0VQ';                           // SMTP password
        $this->mail->Port = 587;

        /*
        $this->mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $this->mail->Host = 'smtp.zoho.com';  // Specify main and backup SMTP servers
        $this->mail->SMTPAuth = true;                               // Enable SMTP authentication
        $this->mail->Username = 'conectividade@conectividade.inf.br';                 // SMTP username
        $this->mail->Password = 'c0n3ct1v1d4d3';                           // SMTP password
        $this->mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $this->mail->Port = 465;
        */

        $this->mail->From = $this->from;
        $this->mail->FromName = $this->fromName;
        $this->mail->Subject = $this->subject;
        $this->mail->Body    = $this->body;
        $this->mail->addAddress($to);
        $this->mail->isHTML(true);
        $this->mail->CharSet = 'UTF-8';
    }

    public function enviar()
    {
        if(!$this->mail->send()) {

            //return $this->msgError().' '.$this->mail->ErrorInfo;
            return $this->mail->ErrorInfo;
        } else {
            return $this->msgOk();
        }
    }

    public function msgError()
    {
        return 'Erro ao enviar mensagem!';
    }

    public function msgOk()
    {
        return true;
    }

    public function isValidEmail($email) {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }


    public function config($subject = null, $from = null, $fromName = null, $to = null, $body = null)
    {
        // Verificação de parâmetros obrigatórios
        if (is_null($subject) || is_null($from) || is_null($fromName) || is_null($to) || is_null($body)) {
            return "Parâmetro(s) obrigatório(s) faltando";
        }
    
        $curl = curl_init();
    
        $postFields = json_encode(array(
            "host_smtp" => "server04.mailgrid.com.br",
            "usuario_smtp" => "bisavoto@bisavoto.com.br",
            "senha_smtp" => "JNLTyZilf9as",
            "emailRemetente" => $from,
            "nomeRemetente" => $fromName,
            "emailDestino" => array($to),
            "assunto" => $subject,
            "mensagem" => $body
        ));
    
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.mailgrid.net.br/send/',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $postFields,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));
    
        $resp = curl_exec($curl);
        if (curl_errno($curl)) {
            // Saída de erro detalhada do cURL
            return 'Erro cURL: ' . curl_error($curl);
        }
    
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
    
        if ($httpCode != 200) {
            // Saída de resposta da API
            return "Erro na API: Código HTTP $httpCode, Resposta: $resp";
        }
    
        return $resp;
    }
    

}
