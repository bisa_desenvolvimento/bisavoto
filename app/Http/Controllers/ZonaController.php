<?php namespace App\Http\Controllers;


use Session;
use Input;
use Redirect;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Models\Zonas as Zona;

class ZonaController extends Controller {

	private $model;
    private $profile_id;
    private $ele_id;

    public function __construct()
    {
        if(is_null($this->model)){
            $this->model = new Zona();
        }
        $this->profile_id = Auth::user()->profile_id;
        $this->ele_id = Auth::user()->ele_id;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($ele_id = null)
	{
        if (is_null($ele_id)){
            $ele_id = $this->ele_id;
        } else {
			Session::put('ele_id', $ele_id);
		}
		$listaZonas = $this->model->listZonas($ele_id);
        $data = [
            'listaZonas' => $listaZonas,
            'ele_id' => $ele_id
        ];

        return view('zona.lista-zonas', with($data));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function add($ele_id = null)
	{
       	$id = $zon_nome = $zon_numero = '';
        if ($this->profile_id == 2){
            $ele_id = $this->ele_id;
        }
        return view('zona.add-zona', compact('id', 'ele_id', 'zon_nome', 'zon_numero'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$valoresForm = array_map('trim', Input::all());
        if($this->profile_id == 2){
            $valoresForm['ele_id'] = $this->ele_id;
        }
		Zona::create($valoresForm);
        if($this->profile_id == 2){
            return redirect()->route('lista.zonas.comissao');
        }
        return redirect()->route('lista.zonas',
            [$valoresForm['ele_id']]);

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id = null)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$zona = $this->model->get($id);

        $id 	  = $zona[0]->zon_id;
        $zon_numero = $zona[0]->zon_numero;
        $zon_nome = $zona[0]->zon_nome;
        $ele_id   = $zona[0]->ele_id;

        return view('zona.add-zona', compact('id', 'ele_id', 'zon_numero', 'zon_nome'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$valoresForm = array_map('trim', Input::all());

        $id 	    = $valoresForm['id'];
        $zon_nome   = $valoresForm['zon_nome'];
        $zon_numero = $valoresForm['zon_numero'];

        $zona = Zona::where('zon_id', '=', $id)->firstOrFail();

        $zona->zon_nome   = $zon_nome;
        $zona->zon_numero = $zon_numero;

        $zona->save();

        if($this->profile_id == 2){
            return redirect()->route('lista.zonas.comissao');
        }
        return redirect()->route('lista.zonas',
								 [$valoresForm['ele_id']] );
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id, $ele_id)
	{
		if (is_null($ele_id)){
            $ele_id = $this->ele_id;
        }

        $zona = Zona::where('ele_id',$ele_id)
        			 ->findOrFail($id);

        if($zona->delete())
        {
            if($this->profile_id == 2){
                return redirect()->route('lista.zonas.comissao');
            }
            return redirect()->route('lista.zonas',
								      $ele_id );
        }
	}

}
