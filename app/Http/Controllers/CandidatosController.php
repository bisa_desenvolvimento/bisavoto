<?php namespace App\Http\Controllers;

use App\Http\Models\Cargo;
use Input;
use Redirect;

use Auth;
use Session;

use File;
use App\Http\Models\Eleicao as Eleicao;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use Illuminate\Http\Request;
use App\Http\Models\Candidato as Candidato;

class CandidatosController extends Controller {

	private $model;
    private $profile_id;
    private $ele_id;

	  public function __construct()
    {
        if(is_null($this->model)){
            $this->model = new Candidato();
        }
        $this->profile_id = Auth::user()->profile_id;
        $this->ele_id = Auth::user()->ele_id;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function listaParaEleitor()
	{
        $idEleicao = $this->ele_id;
        $eleicao = new Eleicao();
        $_SESSION['eleicao'] = $eleicao->get($idEleicao)[0];
        $tempo = Session::get('dataEleicao')->ele_tempo;
        $parsed = date_parse($tempo);
        $segundos = $parsed['hour'] * 3600 + $parsed['minute'] * 60;
        Session::get('dataEleicao')->ele_tempo = $segundos;
        if (Session::get('dataEleicao')->ele_tempo == 0){
            return redirect(url($_SERVER['HTTP_REFERER']));
            exit();
        }
        $cargos = new Cargo();
        $listaCargos = $cargos->listCargos($idEleicao);
        $xCount = 0;
        $candidatos_tmp = [];
        
        if($listaCargos):
            foreach ($listaCargos as $cargo):
                $listaCandidatos = $this->model->listaCandidatos($idEleicao, $cargo->car_id);
                $candidatos = [];
                if($listaCandidatos):
                    foreach ($listaCandidatos as $candidato):
                            $candidatos[] =
                            [
                                'cdt_id' => $candidato->cdt_id,
                                'cdt_nome' => $candidato->cdt_nome,
                                'cdt_numeroCandidatura' => $candidato->cdt_numeroCandidatura,
                                'cdt_fotocandidato' => $candidato->cdt_fotocandidato
                            ];
                    endforeach;
                    if(count($candidatos) > 0):
                        $candidatos_tmp[$cargo->car_id] = [
                            'car_id' => $cargo->car_id,
                            'car_nome' => $cargo->car_nome,
                            'car_votos' => $cargo->car_votos,
                            'car_restricao_zona' => $cargo->car_restricao_zona,
                            'car_zona_id' => $cargo->car_zona_id,
                            'candidatos' => $candidatos
                        ];
                        $xCount++;
                    endif;
                endif;
            endforeach;

        endif;

        $listaCandidatos = $candidatos_tmp;

        $data = [
            'listaCandidatos' => $listaCandidatos,
            'zona_id' => Auth::user()->zona_id
        ];
        return view('candidatos.listaparausuarios')->with($data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
    public function index()
    {
        return view('candidatos.listaparausuarios');
    }

    public function lista($idCargo, $idEleicao = null)
    {
        if (is_null($idEleicao)){
            $idEleicao = $this->ele_id;
        }
        $listaCandidatos = $this->model->listaCandidatos($idEleicao, $idCargo);

        $data = [
            'listaCandidatos' => $listaCandidatos,
            'ele_id' => $idEleicao,
            'car_id' => $idCargo
        ];

		return view('candidatos.lista-candidatos', with($data));
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function add($idCargo, $idEleicao = null)
	{
        if (is_null($idEleicao)){
            $idEleicao = $this->ele_id;
        }
		$ele_id = $idEleicao;
		$car_id = $idCargo;

		$cdt_id = $cdt_nome = $cdt_numeroCandidatura = $cdt_fotocandidato = '';

        return view('candidatos.add-candidatos', compact('cdt_id',
        												 'car_id', 
        												 'ele_id',
        												 'cdt_nome',
        												 'cdt_numeroCandidatura',
        												 'cdt_fotocandidato'));
	}

	public function store()
	{

		$valoresForm = array_map('trim', Input::all());

        $arquivo = Input::file('cdt_fotocandidato');

        if (!$arquivo){
            $valoresForm["cdt_fotocandidato"] = null;
        } else {

            $valoresForm["cdt_fotocandidato"] = $this->uploadImagem($arquivo, $valoresForm['car_id'], $valoresForm['ele_id']);
            $msgRetorno = 'Houve erro na inclusão do registro';
        }



        if(Candidato::create($valoresForm))
        {
            $msgRetorno = 'Inclusão efetuada com sucesso!';
        }

        if ($this->profile_id == 2)
        {
            return Redirect::route('lista.candidatos.comissao', array('msgExclusao' => $msgRetorno, 'car_id' => $valoresForm['car_id']));
        }

        return Redirect::route('lista.candidatos', array('car_id' => $valoresForm['car_id'], 'ele_id' => $valoresForm['ele_id']));
	}

	private function apagarFoto($caminhoFoto)
	{
		if(File::isFile($caminhoFoto))
      	{
            File::delete($caminhoFoto);
        }
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($idCargo, $idCandidato, $idEleicao = null)
	{
        if (is_null($idEleicao)){
            $idEleicao = $this->ele_id;
        }

		$candidato = $this->model->get($idEleicao, $idCargo, $idCandidato);
       
		if($candidato)
		{
			     $cdt_id   = $candidato[0]->cdt_id; 
				 $cdt_nome = $candidato[0]->cdt_nome;
				 $cdt_numeroCandidatura = $candidato[0]->cdt_numeroCandidatura;
				 $cdt_fotocandidato = $candidato[0]->cdt_fotocandidato;
				 $car_id = $candidato[0]->car_id;
				 $ele_id = $candidato[0]->ele_id;  
		}

           return view('candidatos.add-candidatos', compact('cdt_id',
           													'car_id', 
        												 'ele_id',
        												 'cdt_nome',
        												 'cdt_numeroCandidatura',
        												 'cdt_fotocandidato'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$valoresForm = array_map('trim', Input::all());

        $cdt_id = $valoresForm["cdt_id"];
        $ele_id = $valoresForm["ele_id"];
        $car_id = $valoresForm["car_id"];

        $cdt_nome                = $valoresForm['cdt_nome'];
        $cdt_numeroCandidatura   = $valoresForm['cdt_numeroCandidatura'];

        $candidato = Candidato::where('car_id', '=', $car_id)
        					  ->where('ele_id','=', $ele_id)
        					  ->where('cdt_id','=', $cdt_id)
        						->firstOrFail();				  

        $candidato->cdt_nome = $cdt_nome;
        $candidato->cdt_numeroCandidatura = $cdt_numeroCandidatura;

        if(!empty($valoresForm['cdt_fotocandidato']))
        {
            $arquivo = Input::file('cdt_fotocandidato');
            $candidato->cdt_fotocandidato     = $this->uploadImagem($arquivo, $valoresForm['car_id'], $valoresForm['ele_id']);
        }

        $msgRetorno = 'Houve erro na inclusão do registro';

        if($candidato->save())
        {
            $msgRetorno = 'Alteração efetuada com sucesso!';
        }

        if ($this->profile_id == 2)
        {
            return Redirect::route('lista.candidatos.comissao', array('msgExclusao' => $msgRetorno, 'car_id' => $valoresForm['car_id']));
        }

        return Redirect::route('lista.candidatos', array('car_id' => $valoresForm['car_id'], 'ele_id' => $valoresForm['ele_id']));

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($car_id, $cdt_id, $ele_id = null)
	{
        if (is_null($ele_id)){
            $ele_id = $this->ele_id;
        }
		$candidato = Candidato::where('car_id', '=', $car_id)
        					  ->where('ele_id','=', $ele_id)
        					  ->where('cdt_id','=', $cdt_id)
        					  ->firstOrFail();		
        
        $caminhoFoto = ($candidato) ? $candidato['cdt_fotocandidato'] : '';

        $msgRetorno = 'Houve erro na exclusão do registro';

        if($candidato->delete())
        {
            $this->apagarFoto($caminhoFoto);
            $msgRetorno = 'Exclusão efetuada com sucesso!';
        }

        if ($this->profile_id == 2)
        {
            return Redirect::route('lista.candidatos.comissao', array('msgExclusao' => $msgRetorno, 'car_id' => $car_id));
        }

        return Redirect::route('lista.candidatos', array('car_id' => $car_id, 'ele_id' => $ele_id));

	}

    private function uploadImagem($arquivo, $car_id, $ele_id)
    {
        $arquivo = array('image' => $arquivo);

        $validacao = array('image' => 'required',);

        $validador = Validator::make($arquivo, $validacao);

        if ($validador->fails())
        {
            if($this->profile_id == 2):
                return Redirect::to('add.eleicoes.comissao')->withInput()->withErrors($validador);
            endif;

            return Redirect::to('add.eleicoes')->withInput()->withErrors($validador);

        } 
        else 
        {
            // checking file is valid.
            if (Input::file('cdt_fotocandidato')->isValid()){
                
                $destino = 'uploads/imgs/candidatos/eleicao-'.$ele_id.'/cargo-'.$car_id . '/'; // upload path
                
                File::makeDirectory($destino, $mode = 0777, true, true);
             
                $extensaoArquivo = Input::file('cdt_fotocandidato')->getClientOriginalExtension();
                $nomeArquivo = rand(11111,99999).'cdt_fotocandidato.'.$extensaoArquivo; // renameing image

                Input::file('cdt_fotocandidato')->move($destino, $nomeArquivo); // uploading file to given path
                return $destino. $nomeArquivo;
            } else {
                // sending back with error message.
                Session::flash('Erro', 'Upload inválido de arquivo');
                
                if($this->profile_id == 2):
                    return Redirect::to('add.eleicoes.comissao')->withInput()->withErrors($validador);
                else:
                    return Redirect::to('add.eleicoes')->withInput()->withErrors($validador);
                endif;
            }

        }
    }

}
