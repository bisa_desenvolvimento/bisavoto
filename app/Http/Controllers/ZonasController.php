<?php namespace App\Http\Controllers;

use Input;
use Redirect;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Auth;
use Illuminate\Http\Request;
use App\Zonas as Zona;

class ZonasController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    private $model;

    public function __construct()
    {
        if(is_null($this->model)){
            $this->model = new Zona();
        }
    }

    public function index()
    {
        $ele_id = Auth::user()->ele_id;
        $listaZonas = $this->model->listZonas($ele_id);
        $data = [
            'listaZonas' => $listaZonas,
            'ele_id' => $ele_id
        ];

        return view('zona.lista-zonas', with($data));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function add()
    {
        return view('zona.add-zona');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $valoresForm = array_map('trim', Input::all());
        Zona::create($valoresForm);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
