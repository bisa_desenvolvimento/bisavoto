<?php namespace App\Http\Controllers;

use App\Http\Models\Eleitor;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Input;
use Redirect;
use Session;
use File;
use App\Http\Models\Eleicao as Eleicao;
use App\Zona;
use App\Cargo;
use App\Http\Models\Voto as Voto;
use App\Http\Models\Cargo as CargoModel;
use Auth;
use App\Http\Models\Eleitor as Eleitores;
use View;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\SendMailController as SendMailController;
use App\Http\Models\Endereco;
use App\Http\Models\Log as Log;
use Illuminate\Http\Request;
use App\Http\Requests\EleicaoRequest;
use Hash;
use Carbon\Carbon;
use App\Library\Functions as Functions;
use Carbon\Carbon as CarbonCarbon;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Response;
use App\User as User;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class EleicoesController extends Controller {

	private $model;
	private $zona_id;
	private $ele_id;
	private $valoresForm;
	public $naoEnviados;
	public $enviados;
	private $profile_id;
	private $percentualDeNotificados = 0;
	private $log;
	private $modelEndereco;


	public function __construct()
	{
		if(is_null($this->model)){
			$this->model = new Eleicao();
			$this->log = new Log();
			//$this->id = Auth::user()->id;
			if(Auth::user()){
				$this->profile_id = Auth::user()->profile_id;
				$this->log = new Log();
				$this->id = Auth::user()->id;
			}
		}
		if(Auth::user()){
			$this->zona_id = Auth::user()->zona_id;
			$this->ele_id = Auth::user()->ele_id;
			$this->log = new Log();
			$this->id = Auth::user()->id;
		}
		$this->enviados = [];
		$this->naoEnviados = [];
		$this->modelEndereco = new Endereco();

	}

	public function index($id){
		$eleicao = $this->model->getById($id);
		
		$ele_id = $eleicao->ele_id;
		$ele_nome = $eleicao->ele_nome;
		$ele_descricao = $eleicao->ele_descricao;
		$ele_nomenclatura = $eleicao->ele_nomenclatura;
		$ele_logo = $eleicao->ele_logo;
		$ele_qtdVotosCandidatos = $eleicao->ele_qtdVotosCandidatos;
		$ele_tempo = $eleicao->ele_tempo;
		$ele_categoriaEleitor = $eleicao->ele_categoriaEleitor;
		$ele_qtdBu = $eleicao->ele_qtdBu;
		$ele_qtdSessoes = $eleicao->ele_qtdSessoes;
		$ele_horaInicio = Functions::convertDateTime($eleicao->ele_horaInicio);
		$ele_horaTermino = Functions::convertDateTime($eleicao->ele_horaTermino);
		$ele_tipo = $eleicao->ele_tipo;
		$exibir_branco = $eleicao->exibir_branco;
		$exibir_nulo = $eleicao->exibir_nulo;
		$enviar_comprovante = $eleicao->enviar_comprovante;
		$ele_alias = $eleicao->alias;
		$ele_nome_zona = $eleicao->ele_nome_zona;
		$ele_qtdZeresima = $eleicao->ele_qtdZeresima;
		$subject_email_votacao = $eleicao->subject_email_votacao;
		$texto_email_votacao = $eleicao->texto_email_votacao;
		$subject_email_confirmacao = $eleicao->subject_email_confirmacao;
		$texto_email_confirmacao =  $eleicao->texto_email_confirmacao;

	
		Session::forget('ele_id');
		Session::put('ele_id', $eleicao->ele_id);
		
		return view('eleicao.pg-eleicao', compact('ele_id',
													'ele_nome',
													'ele_nome_zona',
													'ele_descricao',
													'ele_nomenclatura',
													'ele_horaInicio',
													'ele_horaTermino',
													'ele_qtdVotosCandidatos',
													'ele_qtdBu',
													'ele_qtdZeresima',
													'ele_logo',
													'ele_tempo',
													'ele_categoriaEleitor',
													'ele_qtdSessoes',
													'ele_tipo',
													'exibir_branco',
													'exibir_nulo',
													'enviar_comprovante',
													'ele_alias',
													'subject_email_votacao',
													'texto_email_votacao',
													'subject_email_confirmacao',
													'texto_email_confirmacao'));
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function lista()
	{	
		$listaEleicoes = Eleicao::listar();
		$listaEleicoes->setPath("");
		return view('eleicao.lista-eleicoes', compact('listaEleicoes'));
	}

	public function add()
	{
		$ele_id = '';
		$ele_nome = '';
		$ele_nome_zona = '';
		$ele_descricao = '';
		$ele_nomenclatura = '';
		$ele_horaInicio = '';
		$ele_horaTermino = '';
		$ele_qtdVotosCandidatos = '';
		$ele_qtdBu = '';
		$ele_qtdZeresima = '';
		$ele_logo = '';
		$ele_tempo = '';
		$ele_categoriaEleitor = '';
		$ele_qtdSessoes = '';
		$ele_tipo = 1;
		$exibir_branco = null;
		$exibir_nulo = null;
		$enviar_comprovante = null;
		$exibir_sequenciaVotos = null;
		$habilitar_exclusao = null;
		$aceitar_email_duplicado = null;
		$habilitar_adicionareleitor = null;
		$ele_alias = '';
		$subject_email_votacao =  '';
		$texto_email_votacao = "Olá [NOME_ELEITOR],\n\nInicio da Eleição: [DATA_INICIO]\nTérmino da Eleição: [DATA_TERMINO]";
		$subject_email_confirmacao =  'COMPROVANTE DE VOTAÇÃO -';
		$texto_email_confirmacao = "Olá [NOME_ELEITOR],\n\nAgradecemos pela sua participação no processo eleitoral.\nAtenciosamente, Comissão Eleitoral\n\nDúvidas: suporte@bisa.com.br";
		$exibir_botao_reenvio_senha = 1;

		return view('eleicao.add-eleicao', compact('ele_id',
												   'ele_nome',
														 'ele_nome_zona',
														 'ele_descricao',
														 'ele_nomenclatura',
														 'ele_horaInicio',
														 'ele_horaTermino',
														 'ele_qtdVotosCandidatos',
														 'ele_qtdBu',
														 'ele_qtdZeresima',
														 'ele_logo',
														 'ele_tempo',
														 'ele_categoriaEleitor',
														 'ele_qtdSessoes',
														 'ele_tipo',
														 'exibir_branco',
														 'exibir_nulo',
														 'enviar_comprovante',
														 'exibir_sequenciaVotos',
														 'habilitar_exclusao',
														 'aceitar_email_duplicado',
														 'ele_alias',
														 'habilitar_adicionareleitor',
														 'subject_email_votacao',
														 'texto_email_votacao',
														 'subject_email_confirmacao',
														 'texto_email_confirmacao',
														 'exibir_botao_reenvio_senha'
													));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(){}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(EleicaoRequest $request)
	{		
		$valoresForm = $request->all();
	
		if (empty($valoresForm['ele_alias'])) {
			$valoresForm['ele_alias'] = Functions::aliasGenerate($valoresForm['ele_nome']);
		} 

		$arquivo = Input::file('ele_logo');
		$valoresForm["ele_logo"] = $this->uploadImagem($arquivo);
		$valoresForm["alias"] = $valoresForm["ele_alias"];
		
		$msgRetorno = 'Houve erro na inclusão do registro';
		if(Eleicao::create($valoresForm))
		{
			$msgRetorno = 'Inclusão efetuada com sucesso!';
		}
		return Redirect::route('lista.eleicoes', array('msgExclusao' => $msgRetorno));
	}
	
	public function end()
	{
		Session::forget('dataEleicao');
		Session::flush();
		
		$opcaoEmail= $this->model->verificaOpcaoComprovanteEmail($this->ele_id);

		if($opcaoEmail == 1){
			return view('eleicao.fim');
		} else if($opcaoEmail== 0){
			return view('eleicao.fim-Alt');
		}
		
	}

	private function uploadImagem($arquivo)
	{
		$arquivo = array('image' => $arquivo);

		$validacao = array('image' => 'required',);

		$validador = Validator::make($arquivo, $validacao);

		if ($validador->fails())
		{
			return Redirect::to('add.eleicoes')->withInput()->withErrors($validador);
		}
		else
		{
			// checking file is valid.
			if (Input::file('ele_logo')->isValid())
			{
				$destino = "uploads/imgs/eleicao/"; // upload path

				File::makeDirectory($destino, $mode = 0777, true, true);

				$extensaoArquivo = Input::file('ele_logo')->getClientOriginalExtension();
				$nomeArquivo = rand(11111,99999).'ele_logo.'.$extensaoArquivo; // renameing image

				Input::file('ele_logo')->move($destino, $nomeArquivo); // uploading file to given path
				return $destino. $nomeArquivo;
			}
			else
			{
				// sending back with error message.
				Session::flash('Erro', 'Upload inválido de arquivo');
				return Redirect::to('add.eleicoes');
			}

		}
	}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$eleicao = $this->model->getById($id);
		$ele_id = $eleicao->ele_id;
		$ele_nome = $eleicao->ele_nome;
		$ele_descricao = $eleicao->ele_descricao; 
		$ele_nomenclatura = $eleicao->ele_nomenclatura;
		$ele_logo = $eleicao->ele_logo;
		$ele_qtdVotosCandidatos = $eleicao->ele_qtdVotosCandidatos;
		$ele_tempo = $eleicao->ele_tempo;
		$ele_categoriaEleitor = $eleicao->ele_categoriaEleitor;
		$ele_qtdBu = $eleicao->ele_qtdBu;
		$ele_qtdSessoes = $eleicao->ele_qtdSessoes;
		$ele_horaInicio = Functions::convertDateTime($eleicao->ele_horaInicio);
		$ele_horaTermino = Functions::convertDateTime($eleicao->ele_horaTermino);
		$ele_tipo = $eleicao->ele_tipo;
		$exibir_branco = $eleicao->exibir_branco;
		$exibir_nulo = $eleicao->exibir_nulo;
		$enviar_comprovante = $eleicao->enviar_comprovante;
		$exibir_sequenciaVotos = $eleicao->exibir_sequenciaVotos;
		$habilitar_exclusao = $eleicao->habilitar_exclusao;
		$aceitar_email_duplicado = $eleicao->aceitar_email_duplicado;
		$habilitar_adicionareleitor = $eleicao->habilitar_adicionareleitor;
		$ele_nome_zona = $eleicao->ele_nome_zona;
		$ele_qtdZeresima = $eleicao->ele_qtdZeresima;
		$ele_alias = $eleicao->alias;
		$subject_email_votacao = $eleicao->subject_email_votacao;
		$texto_email_votacao = $eleicao->texto_email_votacao;
		$subject_email_confirmacao = $eleicao->subject_email_confirmacao;
		$texto_email_confirmacao = $eleicao->texto_email_confirmacao;
		$ele_tipo_envio = $eleicao->ele_tipo_envio;
		$exibir_botao_reenvio_senha = $eleicao->exibir_botao_reenvio_senha;
		
		return view('eleicao.add-eleicao', compact(
											'ele_id',
											'ele_nome',
											'ele_nome_zona',
											'ele_descricao',
											'ele_nomenclatura',
											'ele_horaInicio',
											'ele_horaTermino',
											'ele_qtdVotosCandidatos',
											'ele_qtdBu',
											'ele_qtdZeresima',
											'ele_logo',
											'ele_tempo',
											'ele_categoriaEleitor',
											'ele_qtdSessoes',
											'ele_tipo',
											'exibir_branco',
											'exibir_nulo',
											'enviar_comprovante',
											'exibir_sequenciaVotos',
											'habilitar_exclusao',
											'aceitar_email_duplicado',
											'ele_alias',
											'habilitar_adicionareleitor',
											'subject_email_votacao',
											'texto_email_votacao',
											'subject_email_confirmacao',
											'texto_email_confirmacao',
											'ele_tipo_envio',
											'exibir_botao_reenvio_senha'
										));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(EleicaoRequest $request)
	{
		$valoresForm = $request->all();		
			
		$eleicao = $this->model->findOrFail($valoresForm['ele_id']);
									
		if ($arquivo = Input::file('ele_logo')) {
			$valoresForm['ele_logo'] = $this->uploadImagem($arquivo);
		}
			
		$eleicao->fill($valoresForm);
			
		$msgRetorno = $eleicao->save()
			? 'Alteração efetuada com sucesso!'
			: 'Houve erro na alteração do registro';
	
		return Redirect::route('lista.eleicoes', ['msgExclusao' => $msgRetorno]);
	}
	
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$eleicao = Eleicao::findOrFail($id);

		$msgRetorno = 'Houve erro na Exclusão do registro';

		if($eleicao->delete())
		{
			$msgRetorno = 'Exclusão efetuada com sucesso!';
		}

	   return Redirect::route('lista.eleicoes', array('msgExclusao' => $msgRetorno));

	}

	public function verificarEleicaoEncerrada()
	{

		$retorno = false;
		if($this->ele_id):
			$retorno = $this->model->verificarEleicaoEncerrada($this->ele_id);
			return response()->json(['resp' => $retorno]);
		endif;
	}

	public function salvarVotos()
	{
		ini_set("auto_detect_line_endings", true);
		$valoresForm = Input::all();
		$votos = $valoresForm['votos'];
		$cargo = $valoresForm['cargo'];
		$eleicao = CargoModel::getEleicaoByCargo($cargo[0]);

		// Verificar se o usuário já votou
		$user = User::find($this->id);
		if ($user->vote == 1) {
			return Redirect::route('fim.voto', ['alias' => $eleicao->alias])
				->with('msg', 'Você já registrou seu voto.');
		}

		$modelVoto = new Voto();
		$result = $modelVoto->addVoto($votos, $cargo);

		if (!$result) {

			$user->session_id = null;
            $user->save();

			$data = ['msg' => 'Erro ao salvar seu votos, tente novamente'];
			return view('lista.candidatos.eleitor', with($data));
		} else {

			$opcaoEmail = $this->model->verificaOpcaoComprovanteEmail($this->ele_id);
			if ($opcaoEmail == 1) {
				$this->enviarEmailConfirmaçãoVotação();
			}

			$user->session_id = null;
            $user->save();
		}

		return Redirect::route('fim.voto', ['alias' => $eleicao->alias]);
	}

	public function gerarCarta($idEleicao = null)
	{
		if(is_null($idEleicao)){
			$listaEleicoes = Eleicao::orderBy('ele_nome')->where('ele_enviar', '=', 1)->get();
		} else {
			$listaEleicoes = Eleicao::orderBy('ele_nome')->where('ele_id', '=', $idEleicao)->where('ele_enviar', '=', 1)->get();
		}

		if($listaEleicoes){
			foreach ($listaEleicoes as $eleicao) {
				return $this->imprimir($eleicao->ele_id);
			}
		}
	}

	public function enviarEmails($letra, $ele_id)
	{
		ini_set("auto_detect_line_endings", true);
		ignore_user_abort(true);
		set_time_limit(0);

		
		ini_set("auto_detect_line_endings", true);
		if ($this->profile_id == 2){
			$ele_id = $this->ele_id;
		}
		if (!empty($ele_id)){
			$model_eleicao = new Eleicao();;
			$dadosEleicao = $model_eleicao->get($ele_id);
			$dadosEleicao = $dadosEleicao[0];
		} else {
			$ele_id = $this->ele_id;
			$dadosEleicao = Session::get('dataEleicao');
			$dadosEleicao = $dadosEleicao[0];
		}

		$arrLetra = Functions::alfabeto($letra);
		if ($letra == $arrLetra['total']){
			return json_encode(['enviados' => $this->enviados, 'naoEnviados' => $this->naoEnviados]);
		}
		$eleitorModel = new Eleitores();
		$listaEleitores = $eleitorModel->eleitorPorLetra($arrLetra['letra'], $ele_id);
		$letra++;
		$this->bodyAll = '';
		$body = View::make('eleicao.base-envio')->render();
		$sendMailController = new SendMailController();

		if(count($listaEleitores) > 0){
			foreach ($listaEleitores as $eleitor) {
				
				$logo = $dadosEleicao->ele_logo;
				if (!file_exists($logo)) {
					$logo = '';
				} else {
					$logo = '<img src="http://www.bisavoto.com.br/eleicoes/public/'.$dadosEleicao->ele_logo.'" width="200" />';
				}
				$objEleitor = new Eleitor();
				$newPass = substr(md5(date("Y-m-d H:i:s") . preg_replace('/[^0-9]/', '', $eleitor->cpf) . $ele_id . $eleitor->id), 0, 6);
				//$objEleitor->where('id', '=', $eleitor->id)->update(['password' => Hash::make(preg_replace('/[^0-9]/', '', $eleitor->cpf))]);
				$objEleitor->where('id', '=', $eleitor->id)->update(['password' => Hash::make($newPass)]);
				
				$link_votacao = url('eleicao/' . $dadosEleicao->alias);
					
				$subject = $dadosEleicao->subject_email_votacao; 
				$body    = $dadosEleicao->texto_email_votacao;

				$body = str_replace("\r\n", "<br>", $body);
				$body = str_replace("[NOME_ELEITOR]", $eleitor->name, $body);
				$body = str_replace("[DATA_INICIO]", Functions::convertDateTime($dadosEleicao->ele_horaInicio), $body);
				$body = str_replace("[DATA_TERMINO]", Functions::convertDateTime($dadosEleicao->ele_horaTermino), $body);

				$fixedPart = "<br><br>Acesse: {$link_votacao}<br>Login: {$eleitor->matricula}<br>Senha: {$newPass}";
				$body .= $fixedPart;

				if($eleitor->email == "teste981390@gmail.com"){die("teste");}
				// $this->bodyAll .= $this->body;
				if (filter_var(trim($eleitor->email), FILTER_VALIDATE_EMAIL)) {
					if($sendMailController->config($subject,'contato@bisavoto.com.br', 'Bisa Voto', trim($eleitor->email), $body)){
						$this->enviados[] = ['nome' => $eleitor->name, 'email' => $eleitor->email];
					} else {
						$this->naoEnviados[] = ['nome' => $eleitor->name, 'email' => $eleitor->email];
					}
				}
				
			}
		}

		if($this->profile_id == 2){
			echo url("notificar-eleitores/letra/$letra");
		} else {
			echo url("notificar-eleitores/letra/$letra/idEleicao/$ele_id");
		}
	}

	/*
		Função só envia para um email para o eleitor

	*/


	public function getTextoFixo($login, $senha, $link) {
		return "Por favor, utilize as seguintes informações para acessar a votação:\n" .
			   "Login: {$login}\n" .
			   "Senha: {$senha}\n" .
			   "Link de votação: {$link}";
	}
	
	
	public function enviarEmailEleitor($idEleitor, $idEleicao)
	{
		$model_eleicao = new Eleicao();
		$model_eleitor = new Eleitores();
		$sendMailController = new SendMailController();
		
		$eleicao = $model_eleicao->get($idEleicao)[0];
		$eleitor = $model_eleitor->get($idEleicao, $idEleitor)[0];

		$operacao = 'RESUMO_MODEL_RESUMO::ENVIO EMAIL INDIVIDUAL';
		$dados = [ 'Nome'=>$eleitor->name, 'Email' =>$eleitor->email ];
		$idRegistro = $idEleicao;
		$idUsuario  = $this->id;
		
		$newPass = substr(md5(date("Y-m-d H:i:s") . $eleitor->cpf . $idEleicao . $eleitor->id), 0, 6);
		
		$eleitor->where('id', '=', $eleitor->id)->update(['password' => Hash::make($newPass)]);

		$logo = '<img src="http://www.bisavoto.com.br/eleicoes/public/'.$eleicao->ele_logo.'" width="200" />';
		$link_votacao = url('eleicao/' . $eleicao->alias);
		
		$subject =  $eleicao->subject_email_votacao; 
		$body = $eleicao->texto_email_votacao;

		$body = str_replace("\r\n", "<br>", $body);
		$body = str_replace("[NOME_ELEITOR]", $eleitor->name, $body);
		$body = str_replace("[DATA_INICIO]", Functions::convertDateTime($eleicao->ele_horaInicio), $body);
		$body = str_replace("[DATA_TERMINO]", Functions::convertDateTime($eleicao->ele_horaTermino), $body);

		$fixedPart = "<br><br>Acesse: {$link_votacao}<br>Login: {$eleitor->matricula}<br>Senha: {$newPass}";
		$body .= $fixedPart;

		if (filter_var(trim($eleitor->email), FILTER_VALIDATE_EMAIL)) {
			if($sendMailController->config($subject, 'contato@bisavoto.com.br', 'Bisa Voto', trim($eleitor->email), $body)){
				$this->log->salvar('RESUMO_MODEL_RESUMO::ENVIO EMAIL INDIVIDUAL', $idRegistro, $idUsuario, $dados);
				echo true;
			} else {
				$this->log->salvar('RESUMO_MODEL_RESUMO::ERRO ENVIO EMAIL INDIVIDUAL', $idRegistro, $idUsuario, $dados);
				echo false;
			}
		}
	}

	public function enviarEmailReenvioSenha($idEleitor, $idEleicao, $valoresForm)
	{
		$model_eleicao = new Eleicao();
		$model_eleitor = new Eleitores();
		$sendMailController = new SendMailController();
		
		$eleicao = $model_eleicao->get($idEleicao)[0];
		
		$eleitor = $model_eleitor->get($idEleicao, $idEleitor)[0];
		
		$operacao = 'RESUMO_MODEL_RESUMO::ENVIO EMAIL INDIVIDUAL';

		$dados = [ 'Nome'=>$valoresForm['nome'], 'Email' =>$valoresForm['email'], 'Matrícula' => $valoresForm['matricula'] , 'CPF' => $valoresForm['cpf'], 'Telefone' => $valoresForm['telefone']];
		$idRegistro = $idEleicao;
		$idUsuario = $idEleitor;
		
		$newPass = substr(md5(date("Y-m-d H:i:s") . $eleitor->cpf . $idEleicao . $eleitor->id), 0, 6);
		
		$eleitor->where('id', '=', $eleitor->id)->update(['password' => Hash::make($newPass)]);

		$logo = '<img src="http://www.bisavoto.com.br/eleicoes/public/'.$eleicao->ele_logo.'" width="200" />';
		$link_votacao = url('eleicao/' . $eleicao->alias);

		$subject =  $eleicao->subject_email_votacao; 
		$body = $eleicao->texto_email_votacao;

		$body = str_replace("\r\n", "<br>", $body);
		$body = str_replace("[NOME_ELEITOR]", $eleitor->name, $body);
		$body = str_replace("[DATA_INICIO]", Functions::convertDateTime($eleicao->ele_horaInicio), $body);
		$body = str_replace("[DATA_TERMINO]", Functions::convertDateTime($eleicao->ele_horaTermino), $body);

		$fixedPart = "<br><br>Acesse: {$link_votacao}<br>Login: {$eleitor->matricula}<br>Senha: {$newPass}";
		$body .= $fixedPart;

		if (filter_var(trim($eleitor->email), FILTER_VALIDATE_EMAIL)) {
			if($sendMailController->config($subject, 'contato@bisavoto.com.br', 'Bisa Voto', trim($eleitor->email), $body)){
				$this->log->salvar('RESUMO_MODEL_RESUMO::ENVIO EMAIL INDIVIDUAL', $idRegistro, $idUsuario, $dados);
				echo true;
			} else {
				$this->log->salvar('RESUMO_MODEL_RESUMO::ERRO ENVIO EMAIL INDIVIDUAL', $idRegistro, $idUsuario, $dados);
				echo false;
			}
		}

	}

	public function enviarEmailEleitor2($idEleitor, $idEleicao,$idZona = null,$termo = null)
	{
		$model_eleicao = new Eleicao();
		$model_eleitor = new Eleitores();
		$sendMailController = new SendMailController();
		
		$eleicao = $model_eleicao->get($idEleicao)[0];
		
		$eleitor = $model_eleitor->get($idEleicao, $idEleitor)[0];

		$dados = [ 'Nome'=>$eleitor->name, 'Email' =>$eleitor->email ];
		$idRegistro = $idEleicao;

        if($idZona == "999"){
            $idUsuario = $idEleitor;
        } else{
            $idUsuario = $this->id;
        }

		$newPass = substr(md5(date("Y-m-d H:i:s") . $eleitor->cpf . $idEleicao . $eleitor->id), 0, 6);
		$newPass = preg_replace("/[^0-9]/", rand(0,9), $newPass); 


		$eleitor->where('id', '=', $eleitor->id)->update(['password' => Hash::make($newPass)]);

		$logo = '<img src="http://www.bisavoto.com.br/eleicoes/public/'.$eleicao->ele_logo.'" width="200" />';
		$link_votacao = url('eleicao/' . $eleicao->alias);

		$subject =  $eleicao->subject_email_votacao; 

		if($idZona == "999")
		{
			$ele_inicio = Functions::convertDateTime($eleicao->ele_horaInicio);
			$ele_final = Functions::convertDateTime($eleicao->ele_horaTermino);

			$body = "<h3>{$eleicao->ele_nome}</h3>Olá {$eleitor->name},{$termo}<br /><br/>Inicio da Eleição:{$ele_inicio}<br/>Término da Eleição: {$ele_final}<br/><br/>Acesse: <a href={$link_votacao}>{$link_votacao}</a><br/>Login:{$eleitor->matricula}<br/>Senha:{$newPass}<br/><br/>Dúvidas: suporte@bisa.com.br";

		}else {

			$body = $eleicao->texto_email_votacao;

			$body = str_replace("\r\n", "<br>", $body);
			$body = str_replace("[NOME_ELEITOR]", $eleitor->name, $body);
			$body = str_replace("[DATA_INICIO]", Functions::convertDateTime($eleicao->ele_horaInicio), $body);
			$body = str_replace("[DATA_TERMINO]", Functions::convertDateTime($eleicao->ele_horaTermino), $body);

			$fixedPart = "<br><br>Acesse: {$link_votacao}<br>Login: {$eleitor->matricula}<br>Senha: {$newPass}";
			$body .= $fixedPart;
		}


		if (filter_var(trim($eleitor->email), FILTER_VALIDATE_EMAIL)) {
			if($sendMailController->config($subject,'contato@bisavoto.com.br', 'Bisa Voto', trim($eleitor->email), $body)){
				$this->log->salvar('RESUMO_MODEL_RESUMO::ENVIO EMAIL INDIVIDUAL', $idRegistro, $idUsuario, $dados);
				echo true;
			} else {
				$this->log->salvar('RESUMO_MODEL_RESUMO::ERRO ENVIO EMAIL INDIVIDUAL', $idRegistro, $idUsuario, $dados);
				echo false;
			}
		}
	}


	public function imprimir($ele_id)
	{
		if (!empty($ele_id)){
			$dadosEleicao = $this->model->get($ele_id);
			$dadosEleicao = $dadosEleicao[0];
		} else {
			$ele_id = $this->ele_id;
			$dadosEleicao = Session::get('dataEleicao');
			$dadosEleicao = $dadosEleicao[0];
		}
		$modelEleitor = new Eleitores();
		$listEleitores = $modelEleitor->getForSend($ele_id);
		$lastSend = $modelEleitor->lastSend($ele_id);
		$continue = true;

		$naoEnviados = [];
		if($listEleitores && $continue){
			foreach ($listEleitores as $eleitor) {
				$sendMailController = new SendMailController();
				if($sendMailController->isValidEmail($eleitor->email)) {
					$body = View::make('eleicao.base-carta')->render();
					$logo = $dadosEleicao->ele_logo;
					if (!file_exists($logo)) {
						$logo = '';
					}
					$objEleitor = new Eleitor();
					$newPass = substr(md5(date("Y-m-d H:i:s") . $eleitor->cpf . $ele_id . $eleitor->id), 0, 6);
					$objEleitor->where('id', '=', $objEleitor->id)->update(['password' => Hash::make($newPass)]);
					$body = str_replace('[logo]', $logo, $body);
					$body = str_replace('[eleicao]', $dadosEleicao->ele_nome, $body);
					$body = str_replace('[nome]', $eleitor->name, $body);
					$body = str_replace('[login]', $eleitor->matricula, $body);
					$body = str_replace('[senha]', $newPass, $body);
				}
			}

		}
		return $this->naoEnviados;
	}

	public function headerMail($email, $replay){
		$headers = 'From: '. $email . "\r\n" .
			'Reply-To: '.$replay . "\r\n" .
			'X-Mailer: PHP/' . phpversion();
		return $headers;
	}

	public function votacao($alias)
	{
		$eleicao = $this->model->getByAlias($alias);
		$_SESSION['eleicao'] = $eleicao;

		/** Mantis 16783 - Marcos Antonio */
		$emissaoZeresima = $this->model->verificarEmissaoZeresima($eleicao->ele_id);		
		$dataHoje = strtotime(date('Y-m-d H:i:s')); 
		$dataEleicao = strtotime($eleicao->ele_horaInicio);
		$diferencaEntreDatasEmSegundos = $dataEleicao - $dataHoje;
		if($diferencaEntreDatasEmSegundos > 0){
			$tempoParaIniciarAEleicaoEmMinutos = round(($diferencaEntreDatasEmSegundos/60), 2);
		}else{
			$tempoParaIniciarAEleicaoEmMinutos = 0;
		}
		/** FIM Mantis 16783 - Marcos Antonio */
		
		return view('auth.login', compact('eleicao', 'tempoParaIniciarAEleicaoEmMinutos', 'emissaoZeresima'));
	}

	public function enviarEmailConfirmaçãoVotação()
	{
		$model_eleicao = new Eleicao();
		$model_eleitor = new Eleitores();
		$sendMailController = new SendMailController();

		$eleicao = $model_eleicao->get(Auth::user()->ele_id)[0];
		$eleitor = Auth::user();

		$logo = '<img src="http://www.bisavoto.com.br/eleicoes/public/'.$eleicao->ele_logo.'" width="200" />';
		$link_votacao = url('eleicao/' . $eleicao->alias);
					
		$subject = $eleicao->subject_email_confirmacao; 

		$body = $eleicao->texto_email_confirmacao;
		$body = str_replace("\r\n", "<br>", $body);
		$body = str_replace("[NOME_ELEITOR]", $eleitor->name, $body);

		if (filter_var(trim($eleitor->email), FILTER_VALIDATE_EMAIL)) 
		{
			if($sendMailController->config($subject, 'contato@bisavoto.com.br', 'Bisa Voto', trim($eleitor->email), $body))
			{
				$operacao   = 'RESUMO_MODEL_RESUMO::ENVIO DO EMAIL CONFIRMAÇÃO DE VOTAÇÃO';
				$idRegistro = $eleicao->ele_id;
				$idUsuario  = $eleitor->id;
				$dados      = array('Nome' => $eleitor->name, 'Email' => trim($eleitor->email));
				
				$this->log->salvar($operacao, $idRegistro, $idUsuario, $dados);
			}
	
		}
	}

	
	public function linkReenviarsenhaFora() {
		$hoje = Carbon::now();
		$sql = "SELECT * FROM eleicao where 
				(day(ele_horaInicio) = ".$hoje->day." and month(ele_horaInicio) = ".$hoje->month.")
				or (day(ele_horaTermino) = ".$hoje->day." and month(ele_horaTermino) = ".$hoje->month.")";
		$eleicoes = DB::select($sql);

		foreach ($eleicoes as $value) {
			if (Carbon::parse($value->ele_horaInicio)->format('d/m/Y') == Carbon::parse($value->ele_horaTermino)->format('d/m/Y')) {
				$value->dataInicio = Carbon::parse($value->ele_horaInicio)->format('d/m/Y - H:i')." às ".Carbon::parse($value->ele_horaTermino)->format('H:i');
			} else {
				$value->dataInicio = Carbon::parse($value->ele_horaInicio)->format('d/m/Y - H:i')." até ".Carbon::parse($value->ele_horaTermino)->format('d/m/Y - H:i');
			}
		}
		return view('auth.reenviarSenhaFora', compact('eleicoes'));
	}

	public function linkReenviarsenha($alias) {
		$eleicao = $this->model->getByAlias($alias);
		$_SESSION['eleicao'] = $eleicao;
		return view('auth.reenviarSenha', compact('eleicao'));
	}

	public function salvarReenviarsenha() {
		$emailCorreto = "";
		$valoresForm = array_map('trim', Input::all());
		$query = Eleitor::where([
			'ele_id' => $valoresForm['ele_id'],
			'email' => $valoresForm['email'],
			'cpf' => $valoresForm['cpf']
	
		]);
		
		$eleitor = $query->first();	
	
		if ($eleitor) {
			

			if($eleitor->vote == 1)
			{
				$resultado = "voto_confirmado";

			} else {
				
				if($eleitor->reenvioSenha == 1)
				{
				
					$resultado = "reenvio_confirmado";

				} else {
					
					Eleitor::where('id', '=', $eleitor->id)->update(['reenvioSenha' => '1']);
					$this->enviarEmailReenvioSenha($eleitor->id, $valoresForm['ele_id'], $valoresForm);
					$resultado = "envia_email";
				}
			
			}
		
		} else {
			
			$consulta1 = Eleitor::where(['ele_id' => $valoresForm['ele_id'], 'cpf' => $valoresForm['cpf']])->first();
			$consulta2 = Eleitor::where(['ele_id' => $valoresForm['ele_id'],'email' => $valoresForm['email'], 'cpf' => $valoresForm['cpf']])->first();
			
			$nomeEmailMatricula = count($consulta1) > 0 ? true : false;
			$emailNomeEmailMatricula = count($consulta2) > 0 ? true : false;

			if ($nomeEmailMatricula && !$emailNomeEmailMatricula) {
				// pop-up informando o email e recomenendando entrar em contato com a Comissão eleitoral para atualizar o email
				$resultado = "email_incorreto";
				$emailCorreto = $consulta1->email;
				// $this->enviarEmailReenvioSenhaSuporteBisa($valoresForm, 'O usuário abaixo tentou criar uma nova senha, mas o e-mail informado no formulário não é o mesmo cadatrado no banco de dados.');
			} else {
				// pop-up informando que á inconsistências nos dados e entrar em contato com a Comissão eleitoral
				$resultado = "inconsistencia";
				// $this->enviarEmailReenvioSenhaSuporteBisa($valoresForm, 'O usuário abaixo tentou criar uma nova senha, mas existem inconsistências nos campos informados.');

			}
		}
		return json_encode(['resultado' => $resultado, 'emailCorreto' => $emailCorreto]);
	}

	/**
	 * O método abaixo é usado quando o eleitor esqueceu sua senha.
	 * Será enviado para o contato@bisavoto.com.br um email que o eleitor X alterou sua senha
	 */
	public function enviarEmailReenvioSenhaSuporteBisa($eleitor, $msg) {
		$sendMailController = new SendMailController();

		// $body = View::make('eleicao.reenvio-senha')->render();
		// $body = str_replace('[msg]', $msg, $body);
		// $body = str_replace('[eleicao]', $eleitor['ele_nome'], $body);
		// $body = str_replace('[nome]', $eleitor['nome'], $body);
		// $body = str_replace('[email]', $eleitor['email'], $body);
		// $body = str_replace('[matricula]', $eleitor['matricula'], $body);
		// $body = str_replace('[cpf]', $eleitor['cpf'], $body);
		// $body = str_replace('[telefone]', $eleitor['telefone'], $body);

		$objEle = [
			"tipo" => "reenvio-senha",
			"msg" => $msg,
			"ele_nome" =>  $eleitor['ele_nome'],
			"eleitor_name" => $eleitor['nome'],
			"email" => $eleitor['email'],
			"matricula" => $eleitor['matricula'],
			"cpf" => $eleitor['cpf'],
			"telefone" => $eleitor['telefone'],
		];


		$emailDestino = "desenvolvimento6@bisa.com.br";

		if (filter_var(trim($emailDestino), FILTER_VALIDATE_EMAIL)) {
			$sendMailController->config("ALTERAÇÃO DE SENHA - ".$eleitor['ele_nome'], 'contato@bisavoto.com.br', 'Bisa Voto', trim($emailDestino), $objEle);
			//$sendMailController->enviar();
		}
	}

	public function enviarEmailsEleitores(Request $request, $ele_id){
		$optionEmail = $_GET['optionEmail'];
		$optionPassword = $_GET['optionPassword'];
		$opcaoSenha = 0;

		ini_set("auto_detect_line_endings", true);
		ignore_user_abort(true);
		set_time_limit(0);


		ini_set("auto_detect_line_endings", true);
		if ($this->profile_id == 2){
			$ele_id = $this->ele_id;
		}
		if (!empty($ele_id)){
			$model_eleicao = new Eleicao();;
			$dadosEleicao = $model_eleicao->get($ele_id);
			$dadosEleicao = $dadosEleicao[0];
		} else {
			$ele_id = $this->ele_id;
			$dadosEleicao = Session::get('dataEleicao');
			$dadosEleicao = $dadosEleicao[0];
		}

		
		$eleitorModel = new Eleitores();
		$listaEleitores = $eleitorModel->eleitoresPorIdEleicao($ele_id);

		
		$this->bodyAll = '';
		$sendMailController = new SendMailController();

		if(count($listaEleitores) > 0){

			$dados = [];
			$idRegistro = $ele_id;
			$idUsuario = $this->id;

			foreach ($listaEleitores as $eleitor) {		
				
				$logo = $dadosEleicao->ele_logo;
				if (!file_exists($logo)) {
					$logo = '';
				} else {
					$logo = '<img src="http://www.bisavoto.com.br/eleicoes/public/'.$dadosEleicao->ele_logo.'" width="200" />';
				}

				// Gerar senhas 
				$newPass = '';
				if($optionPassword == 'senhaAlfanumerica'){
					$newPass = substr(md5(date("Y-m-d H:i:s") . preg_replace('/[^0-9]/', '', $eleitor->cpf) . $ele_id . $eleitor->id), 0, 6);
				} else if($optionPassword == 'senhaNumerica') {
					for($i = 0; $i < 6; $i++){
						$newPass .= mt_rand(0,9);
					}
					$opcaoSenha = 1;
				}

				/** Os IFs e ELSEs a seguir consideram as seguintes situações:
				 *  1- Enviar emails: criar os usuários dos eleitores e enviar a todos via e-mail.
				 *  2- Gerar Senha: apenas criar os usuários sem enviar os e-mails.
				 *  3- Ambos: criar os usuários dos eleitores e enviar a todos via e-mail, com o acréscimo de preencher o campo "serie" com a senha não criptografada.
				 *  Autor: Marcos Antonio.
				 */


				if(($optionEmail == 'enviarEmails' || $optionEmail == 'ambos') && $eleitor->email !== "sememail@sememail.com.br"){ 
					/*	Retirado para as eleições do condominio;*/
					$this->body = View::make('eleicao.base-envio')->render();
					$this->body = str_replace('[logo]', $logo, $this->body);
					$this->body = str_replace('[eleicao]', $dadosEleicao->ele_nome, $this->body);
					$this->body = str_replace('[nome]', $eleitor->name, $this->body);
					$this->body = str_replace('[dataInicio]', Functions::convertDateTime($dadosEleicao->ele_horaInicio), $this->body);
					$this->body = str_replace('[dataTermino]', Functions::convertDateTime($dadosEleicao->ele_horaTermino), $this->body);
					$this->body = str_replace('[login]', $eleitor->matricula, $this->body);
					$this->body = str_replace('[senha]', $newPass, $this->body);
					$this->body = str_replace('[alias]', $dadosEleicao->alias, $this->body);
					$this->body = str_replace('[alias]', $dadosEleicao->alias, $this->body);

					$link_votacao = url('eleicao/' . $dadosEleicao->alias);
					
					$subject = $dadosEleicao->subject_email_votacao; 
					$body    = $dadosEleicao->texto_email_votacao;

					$body = str_replace("\r\n", "<br>", $body);
					$body = str_replace("[NOME_ELEITOR]", $eleitor->name, $body);
					$body = str_replace("[DATA_INICIO]", Functions::convertDateTime($dadosEleicao->ele_horaInicio), $body);
					$body = str_replace("[DATA_TERMINO]", Functions::convertDateTime($dadosEleicao->ele_horaTermino), $body);

					$fixedPart = "<br><br>Acesse: {$link_votacao}<br>Login: {$eleitor->matricula}<br>Senha: {$newPass}";
					$body .= $fixedPart;

					// $this->bodyAll .= $this->body;
					if (filter_var(trim($eleitor->email), FILTER_VALIDATE_EMAIL)) {
						if($sendMailController->config($subject, 'contato@bisavoto.com.br', 'Bisa Voto', trim($eleitor->email), $body)){
							$this->enviados[] = ['nome' => $eleitor->name, 'email' => $eleitor->email];
							$objEleitor = new Eleitor();
							$objEleitor->where('id', '=', $eleitor->id)->update(['password' => Hash::make($newPass), 'opcaoSenha' => $opcaoSenha]);
							$dadosForeach = ['nome' => $eleitor->name, 'email' => $eleitor->email];
							array_push($dados, $dadosForeach);

							if($optionEmail == 'ambos'){
								$objEleitor->where('id', '=', $eleitor->id)->update(['serie' => $newPass]);
						
								$this->log->salvar('RESUMO_MODEL_RESUMO::ENVIO DE EMAILS E GERAÇÃO DE SENHA', $idRegistro, $idUsuario, $dadosForeach);
							}else{
								
								$this->log->salvar('RESUMO_MODEL_RESUMO::ENVIO DE EMAILS GERAL', $idRegistro, $idUsuario, $dadosForeach);
								
							}
							$dadosForeach = [];
						}else {
							$this->naoEnviados[] = ['nome' => $eleitor->name, 'email' => $eleitor->email];
						}
					}else{
						$this->naoEnviados[] = ['nome' => $eleitor->name, 'email' => $eleitor->email];
					}
				} else if($optionEmail == 'gerarSenha' || $eleitor->email == "sememail@sememail.com.br") {
					$objEleitor = new Eleitor();
					$objEleitor->where('id', '=', $eleitor->id)->update(['serie' => $newPass]);
					$objEleitor->where('id', '=', $eleitor->id)->update(['password' => Hash::make($newPass)]);
					$dadosForeach = ['nome' => $eleitor->name, 'id' => $eleitor->id ];
					array_push($dados, $dadosForeach);

					$this->log->salvar('RESUMO_MODEL_RESUMO::GERAÇÂO DE SENHA SEM ENVIO DE EMAIL', $idRegistro, $idUsuario, $dadosForeach);
					
					$dadosForeach = [];
				} 

				
			}
			
			if(count($this->naoEnviados)>0){
				$this->log->salvar('RESUMO_MODEL_RESUMO::ERRO ENVIO DE EMAILS GERAL', $idRegistro, $idUsuario, $this->naoEnviados);
				return json_encode($this->naoEnviados);
			}else{

				return $eleitorModel->percentualDeEleitoresDeNotificados($ele_id);
				
			}

		}
		
		
	}

	public function gerarExcelSMS(Request $request,  $ele_id, $optionPassword)
	{
		$opcaoSenha = 0;
		ini_set("auto_detect_line_endings", true);
		ignore_user_abort(true);
		set_time_limit(0);

		if ($this->profile_id == 2){
			$ele_id = $this->ele_id;
		}
		if (!empty($ele_id)){
			
			$dadosEleicao = $this->model->get($ele_id);
			$dadosEleicao = $dadosEleicao[0];
		} 
		else {
			$ele_id = $this->ele_id;
			$dadosEleicao = Session::get('dataEleicao');
			$dadosEleicao = $dadosEleicao[0];
		}
		
		$eleitorModel = new Eleitores();		
		$listaEleitores = $eleitorModel->eleitoresGeralPorIdEleicao($ele_id);

		$spreadsheet = new Spreadsheet();

		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'Nome');
		$sheet->setCellValue('B1', 'Telefone');
		$sheet->setCellValue('C1', 'Mensagem');

		$linha = 2;

		foreach ($listaEleitores as $eleitor) {
			
			$endereco = $this->modelEndereco->get($eleitor->id);

			// if(count($endereco)) {
			// 	$eleitor->telefone = Functions::formatarTelefone($endereco[0]->telefone);
			// }

			$eleitor->telefone = !empty($eleitor->telefone) ?  Functions::formatarTelefone($eleitor->telefone) : (!empty($endereco[0]->telefone) ?  Functions::formatarTelefone($endereco[0]->telefone) : '');

			$newPass = '';

			if($optionPassword == 'senhaAlfanumerica'){
				$newPass = substr(md5(date("Y-m-d H:i:s") . preg_replace('/[^0-9]/', '', $eleitor->cpf) . $ele_id . $eleitor->id), 0, 6);
			} else if($optionPassword == 'senhaNumerica') {
				for($i = 0; $i < 6; $i++){
					$newPass .= mt_rand(0,9);
				}
				$opcaoSenha = 1;
			}

			$mensagem = $dadosEleicao->texto_email_votacao;		
			$mensagem = str_replace("[NOME_ELEITOR]", $eleitor->name, $mensagem);	
			$mensagem = str_replace("[DATA_INICIO]", Functions::convertDateTime($dadosEleicao->ele_horaInicio), $mensagem);
			$mensagem = str_replace("[DATA_TERMINO]", Functions::convertDateTime($dadosEleicao->ele_horaTermino), $mensagem);
			$linkCurto = url('eleicao/' . $dadosEleicao->alias);
			// $linkCurto = url() . "/link/{$ele_id}";
			$mensagem .= " Link: {$linkCurto} Login: {$eleitor->matricula} Senha: {$newPass}";
			// Remoção de quebras de linha
			$mensagem = preg_replace("/\s+/", " ", $mensagem);

			$sheet->setCellValue('A' . $linha, $eleitor->name);
			$sheet->setCellValue('B' . $linha, $eleitor->telefone);
			$sheet->setCellValue('C' . $linha, $mensagem);
			$linha++;

			$eleitorModel->where('id', '=', $eleitor->id)->update(['serie' => $newPass, 'opcaoSenha' => $opcaoSenha]);
			$eleitorModel->where('id', '=', $eleitor->id)->update(['password' => Hash::make($newPass)]);


			
		}
		$nomeArquivo = "eleitoresSMS.xlsx";

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="' . $nomeArquivo . '"');
		header('Cache-Control: max-age=0');	

		$writer = new Xlsx($spreadsheet);
		$writer->save('php://output');
		exit;
	}

	public function redirecionarLinkEleicao($idEleicao){
		$dadosEleicao = $this->model->get($idEleicao);
		$dadosEleicao = $dadosEleicao[0];
		$linkVotacao = url('eleicao/' . $dadosEleicao->alias);
		return redirect($linkVotacao);
	}

	public function gerarNovaSenhaEleitor(Request $request)
	{
		try {
			$idEleitor = $request->input("idEleitor");
			$optionPassword = $request->input("optionPassword");
	
			// Busca o eleitor
			$eleitorModel = new Eleitores();
			$eleitor = $eleitorModel->where('id', $idEleitor)->first();
	
			if (!$eleitor) {
				return response()->json(["resultado" => "erro", "mensagem" => "Eleitor não encontrado."], 404);
			}
	
			// Geração da nova senha
			$opcaoSenha = 0;
			$newPass = "";
	
			if ($optionPassword === 'senhaAlfanumerica') {
				$newPass = substr(md5(date("Y-m-d H:i:s") . preg_replace('/[^0-9]/', '', $eleitor->cpf)  . $eleitor->ele_id . $eleitor->id), 0, 6);
			} else { // senhaNumerica
				$newPass = str_pad(mt_rand(0, 999999), 6, '0', STR_PAD_LEFT);
				$opcaoSenha = 1;
			}
	
			// Atualiza a senha e a opção no banco
			$eleitorModel->where('id', $eleitor->id)->update([
				'serie' => $newPass,
				'opcaoSenha' => $opcaoSenha,
				'password' => Hash::make($newPass)
			]);
	
			// Busca os dados da eleição
			$dadosEleicao = $this->model->get($eleitor->ele_id)->first();
	
			if (!$dadosEleicao) {
				return response()->json(["resultado" => "erro", "mensagem" => "Dados da eleição não encontrados."], 404);
			}
	
			// Monta a mensagem
			$mensagem = str_replace(
				["[NOME_ELEITOR]", "[DATA_INICIO]", "[DATA_TERMINO]"],
				[$eleitor->name, Functions::convertDateTime($dadosEleicao->ele_horaInicio), Functions::convertDateTime($dadosEleicao->ele_horaTermino)],
				$dadosEleicao->texto_email_votacao
			);
	
			$link_votacao = url('eleicao/' . $dadosEleicao->alias);
			$mensagem .= "\n\nAcesse: {$link_votacao}\nLogin: {$eleitor->matricula}\nSenha: {$newPass}";
	
			// Registra log da ação
			$this->log->salvar(
				'RESUMO_MODEL_RESUMO::GERAÇÃO DE SENHA E CÓPIA DA MENSAGEM DE E-MAIL PARA ÁREA DE TRANSFERÊNCIA',
				$eleitor->ele_id,
				$this->id,
				['nome' => $eleitor->name, 'id' => $eleitor->id]
			);
	
			return response()->json(["resultado" => "sucesso", "dados" => ["mensagem" => $mensagem]]);
		} catch (\Exception $e) {
			return response()->json(["resultado" => "erro", "mensagem" => "Erro ao gerar a senha.", "detalhes" => $e->getMessage()], 500);
		}
	}
	

	public function naoNotificados(Request $request, $ele_id){
		$naoNotificados = json_decode($request->naoEnviados);
		return view("eleitores.lista-eleitores-nao-notificados",  compact("naoNotificados"));
	}

	public function percentualDeEleitoresDeNotificados($ele_id){
		$eleitorModel = new Eleitores();
		return $eleitorModel->percentualDeEleitoresDeNotificados($ele_id);
	}

    public function setPausaEleicao($id) {
        try {
            $eleicao = Eleicao::findOrFail($id);

            $resultado = $eleicao->setPausaEleicao($id);

            $msgRetorno = "Ação feita com sucesso na eleição!";

            $this->log->salvar("ELEICAO_MODEL_ELEICAO::" . ($resultado == "1"? "PAUSAR" : "LIBERAR") . " ELEICAO", $id, $this->id, ['Status Eleicao Novo' => $resultado], );
        } catch (\Exception $e) {
            $msgRetorno = "erro: Eleição não encontrada ou falha na operação.";
        }

        return Redirect::route('lista.eleicoes', array('msgExclusao' => $msgRetorno));
    }


	
	public function filtroEleicoes()
	{
		$opcao = $_GET["filtro"];
		$listaEleicoes = Eleicao::listarEleicoesFiltro($opcao);
		$listaEleicoes->appends(['filtro' => $opcao]);
		$listaEleicoes->setPath("");
		return view('eleicao.lista-eleicoes', compact('listaEleicoes', 'opcao'));

	}

    public function adicionarEleitorDuranteVotacao($idEleicao)
    {
		$eleicao = $this->model->getById($idEleicao);
        return view("eleitores.add-eleitor-durante-votacao",  compact("idEleicao", "eleicao"));
    }

	public function erroPath()
	{
		return $_SERVER['HTTP_REFERER'];
	}

}
