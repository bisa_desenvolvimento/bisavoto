<?php namespace App\Http\Controllers;

use Session;
use App\Http\Controllers\Controller;
use App\Http\Models\Cargo;
use App\Http\Models\Resumo as Resumo;
use App\Http\Models\Log as Log;
use App\Http\Models\Eleicao as Eleicao;
use App\Http\Models\Voto as Voto;
use App\Http\Models\Eleitor as Eleitor;
use App\Http\Models\Zonas;
use Auth;
use Illuminate\Support\Facades\DB;
use PDF;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;


class ResumoController extends Controller {

    private $ele_id;
    private $model;
    private $log;
    private $modelResumo;

    public function __construct()
    {
        if(is_null($this->model)){
            $this->model = new Eleicao();
            $this->log = new Log();       
            $this->modelResumo = new Resumo();
        }
        if(Auth::user()){
            $this->zona_id = Auth::user()->zona_id;
            $this->ele_id = Auth::user()->ele_id;
            $this->profile_id = Auth::user()->profile_id;
            $this->id = Auth::user()->id;
        }
    }

	public function index($idEleicao = null)
	{
        $data = $this->getResumo($idEleicao);
        return view('resumo.lista', compact('data'));
	}

    public function getResumo($idEleicao = null)
    {
        if(is_null($idEleicao)){
            $idEleicao = $this->ele_id;
        }
        $data = $this->modelResumo->lista($idEleicao);
        return $data;
    }

    public function apuracao($idEleicao = null)
    {
        if(is_null($idEleicao)){
            $idEleicao = $this->ele_id;
        }

        $data = $this->modelResumo->apuracao($idEleicao);
    }

    public function apuracaofinal($idEleicao = null)
    {
        if(is_null($idEleicao)){
            $idEleicao = $this->ele_id;
        } else {
			Session::put('ele_id', $idEleicao);
		}
        $objEleicao = $this->model->get($idEleicao)[0];
        
        return view('resumo.apuracaofinal', compact('objEleicao'));
    }

    public function apuracaofinaleleicao($idEleicao = null)
    {
        if(is_null($idEleicao)){
            $idEleicao = $this->ele_id;
        }

        $totalVotos = $this->contabilizarVotos($idEleicao);

        $votos = [ 'candidatos' => $totalVotos ];

        $aptos = $this->modelResumo->listarAptos($idEleicao);

        $eleicao = $this->model->get($idEleicao)[0];
        $eleicao->nomeRelatorio = "Apuração Final Eleição";

        set_time_limit(0);

        //Padrão do Log
        $operacao = 'RESUMO_MODEL_RESUMO::RELATORIO FINAL';
        $idRegistro = $idEleicao;
        $idUsuario = $this->id;
        $dados = array('totalVotos' => $totalVotos, 'aptos' => count($aptos), 'votos' => $votos);
        $this->log->salvar($operacao, $idRegistro, $idUsuario, $dados);

        return view('resumo.apuracaofinaleleicao', compact('votos', 'eleicao', 'aptos'));
    }

    public function apuracaofinaleleicaoporzona($idEleicao = null)
    {
        if(is_null($idEleicao)){
            $idEleicao = $this->ele_id;
        }

        $eleicao = $this->model->get($idEleicao)[0];
        $eleicao->nomeRelatorio = "Apuração Final ".$eleicao->ele_nomenclatura." por Zona";

        $aptos = $this->modelResumo->listarAptos($idEleicao);

        $dados = array();

        $Zonas = new Zonas();
        $Cargos = new Cargo();
        $cargos = $Cargos->listaCargosComVotacao($idEleicao);
        $exibir_branco  = $this->model->verificaOpcaoVotoBranco($idEleicao);
        $exibir_nulo    = $this->model->verificaOpcaoVotoNulo($idEleicao);

        foreach ($cargos as $cargo) {
            $zonas = $Zonas->listaZonasComVotacao($idEleicao, $cargo->car_id);
            $arrayZonas = array();
            foreach ($zonas as $zona) {
                array_push($arrayZonas, $zona);
                $arrayCandidatos = array();
                $votosTotais = $this->modelResumo->resultadoEleicaoPorZona2($idEleicao, $cargo->car_id, $zona->zon_id);
                foreach ($votosTotais as $key => $value) {
                    array_push($arrayCandidatos, $value);
                }
                $zona->candidatos = $arrayCandidatos;

                if ($exibir_branco == 1) {
                    $zona->votos_branco = $this->modelResumo->getBrancoZona($idEleicao, $zona->zon_id, $cargo->car_id)->votos;
                }
                if ($exibir_nulo == 1) {
                    $zona->votos_nulo = $this->modelResumo->getNuloZona($idEleicao, $zona->zon_id, $cargo->car_id)->votos;
                }
                
            }
            $cargo->zona = $arrayZonas;

            $dadosForeach = ['Zonas' => $zona, 'Votos Totais' => $votosTotais];
			array_push($dados, $dadosForeach);
			$dadosForeach = [];
            
        }
        set_time_limit(0);

         //Padrão do Log
         $operacao = 'RESUMO_MODEL_RESUMO::RELATORIO FINAL ELEICAO POR ZONA';
         $idRegistro = $idEleicao;
         $idUsuario = $this->id;
         $this->log->salvar($operacao, $idRegistro, $idUsuario, $dados);

         return view('resumo.apuracaofinaleleicaoporzona', compact('eleicao', 'aptos', 'cargos'));
    }

    public function relatorioAptos($idEleicao)
    {      
        ini_set('memory_limit', '-1');

        $aptos = $this->modelResumo->listarAptos($idEleicao);
        $eleicao = $this->model->get($idEleicao)[0];
        $eleicao->nomeRelatorio = "Relatório Aptos";

		Session::forget('ele_id');
		Session::put('ele_id', $idEleicao);

        set_time_limit(0);
        
        /** MANTIS 16869 - Melhoria no Relatório de Ocorrências Bisavoto **/
        $this->log->salvar('RESUMO_MODEL_RESUMO::RELATORIO DE APTOS', $idEleicao, $this->id, null);
        /** FIM MANTIS 16869 **/

        return view('resumo.relatorioaptos', compact('aptos', 'eleicao'));

        return PDF::loadView('resumo.relatorioaptos', compact('aptos', 'eleicao'))
                // Se quiser que fique no formato a4 retrato: 
                ->setPaper('a4')
                ->download('relatorios_de_aptos - '.$eleicao->ele_nome.'.pdf');
    }

    public function relatorioAptosZona($idEleicao)
    {      
        ini_set('memory_limit', '-1');

        $eleicao = $this->model->get($idEleicao)[0];
        $eleicao->nomeRelatorio = "Relatório Aptos por Zona";

        $Zonas = new Zonas();
        $zonas = $Zonas->listZonas($idEleicao);

        foreach ($zonas as $zona) {
            $zona->aptos = $this->modelResumo->listarAptosPorZona($idEleicao, $zona->zon_id);
        }
        
        set_time_limit(0);
        
        return view('resumo.relatorioaptosporzona', compact('zonas', 'eleicao'));

        return PDF::loadView('resumo.relatorioaptoszona', compact('zonas', 'eleicao'))
                // Se quiser que fique no formato a4 retrato: 
                ->setPaper('a4')
                ->download('relatorios_de_aptos - '.$eleicao->ele_nome.'.pdf');
    }

    public function relatorioVotantes($idEleicao)
    {
        $votantes = $this->modelResumo->listarVotantes($idEleicao);
        $eleicao = $this->model->get($idEleicao)[0];
        $eleicao->nomeRelatorio = "Relatório Votantes";
        $aptos = count($this->modelResumo->listarAptos($idEleicao));

        //Padrão do Log
        $operacao = 'RESUMO_MODEL_RESUMO::RELATORIO PARCIAL';
        $idRegistro = $idEleicao;
        $idUsuario = $this->id;
        $dados = array('votantes' => $votantes, 'aptos' => $aptos);
        $this->log->salvar($operacao, $idRegistro, $idUsuario, $dados);


        return view('resumo.relatoriovotantes', compact('votantes', 'eleicao', 'aptos'));

        return PDF::loadView('resumo.relatoriovotantes', compact('votantes', 'eleicao', 'aptos'))
                // Se quiser que fique no formato a4 retrato: 
                ->setPaper('a4')
                ->download('relatorios_de_votantes.pdf');
    }    

    public function relatorioVotantesPorZona($idEleicao = null)
    {
        if(is_null($idEleicao)){
            $idEleicao = $this->ele_id;
        }

        $eleicao = $this->model->get($idEleicao)[0];
        $eleicao->nomeRelatorio = "Relatório Votantes por Zona";
        $votantes = $this->modelResumo->listarVotantesPorZona($idEleicao);

        $votantesPorZona = [];
        $zonaFiltrada = '';

        foreach ($votantes as $votante) {

            if ($votante->Zona != $zonaFiltrada){
                $zonaFiltrada = $votante->Zona;
                $votantesPorZona[$votante->Zona] = [];
            }

            array_push($votantesPorZona[$votante->Zona], $votante);
        }

        return view('resumo.relatoriovotantesporzona', compact('votantesPorZona', 'eleicao'));
    }

    
    public function relatorioOcorrencias($idEleicao = null)
    {
        if(is_null($idEleicao)){
            $idEleicao = $this->ele_id;
        }

        $eleicao = $this->model->get($idEleicao)[0];
        $eleicao->nomeRelatorio = "Relatório Ocorrências";

        $dados = DB::select("SELECT *, CONVERT_TZ(l.Log_Data, 'UTC', 'America/Sao_Paulo') AS Log_Data_Local 
                            FROM log l
                            INNER JOIN users u ON l.Log_Usuario = u.id
                            WHERE l.Log_IdRegistro = ".$idEleicao." 
                            ORDER BY l.Log_Data ASC ");

        return view('resumo.relatorioocorrencias', compact('eleicao', 'dados'));
    }

    public function relatorioVotantesFinal($idEleicao)
    {
        $votantes = $this->modelResumo->listarVotantes($idEleicao);
        $eleicao = $this->model->get($idEleicao)[0];
        $eleicao->nomeRelatorio = "Relatório Votantes Final";

        /** MANTIS 16869 - Melhoria no Relatório de Ocorrências Bisavoto **/
        $this->log->salvar('RESUMO_MODEL_RESUMO::RELATORIO DE VOTANTES', $idEleicao, $this->id, null);
        /** FIM MANTIS 16869 **/

        return view('resumo.relatoriovotantesfinal', compact('votantes', 'eleicao'));
    }

    public function emitirZeresima($idEleicao)
    {
        $eleicao = Eleicao::find($idEleicao);

        if (strtotime($eleicao->ele_horaInicio) > date('Y-m-d H:i:s')){
            Eleitor::where('ele_id', '=', $idEleicao)->update(['vote' => 0]);
            Voto::where('ele_id', '=', $idEleicao)->delete();

            $totalVotos = $this->contabilizarVotos($idEleicao);
            $votos = [ 'candidatos' => $totalVotos ];

            //Padrão do Log
            $operacao = 'RESUMO_MODEL_RESUMO::ZERESIMA';
            $idRegistro = $idEleicao;
            $idUsuario = $this->id;
            $dados = $votos;
            $this->log->salvar($operacao, $idRegistro, $idUsuario, $dados);

            return view('resumo.zeresima', compact('votos', 'eleicao'));
        }

        echo "Ação não permitida";
    }

    public function contabilizarVotos($idEleicao)
    {
        $totalVotos = array();
        $votosCandidatos = $this->modelResumo->listaEleicao($idEleicao);

        foreach ($votosCandidatos as $votos) {
            $totalVotos[$votos->car_id][] = $votos;
        }

        $exibir_branco = $this->model->verificaOpcaoVotoBranco($idEleicao);
        $exibir_nulo    = $this->model->verificaOpcaoVotoNulo($idEleicao);

        foreach ($totalVotos as $key => $value) {
            if ($exibir_branco == 1) {
                $totalVotos[$key]['branco'] = $this->modelResumo->getBranco($idEleicao, $key);
            }
            if ($exibir_nulo == 1) {
                $totalVotos[$key]['nulo'] = $this->modelResumo->getNulo($idEleicao, $key);
            }
        }

        return $totalVotos;
    }

    public function resultadoParcial($nomeEleicao, $idEleicao)
    {
        
        $eleicao = $this->model->get($idEleicao)[0];    

        $aptos = $this->modelResumo->listarAptos($idEleicao);
        $totalAptos = count($aptos);

        $votantes = $this->modelResumo->listarVotantes($idEleicao);
        $totalVotantes = count($votantes);
        
        $dataAtual = date('d/m/Y');
        $horaAtual = date('H:i:s');

        return view('resumo.resultadoparcial', compact('totalAptos', 'totalVotantes', 'dataAtual', 'horaAtual', 'eleicao'));


    }

    public function eleitoresEmailsAlterados($idEleicao)
    {
        $dados        = $this->log->consultarLogEmailsAlterados($idEleicao);
        $nome_eleicao = $this->model->getById($idEleicao);
        $nomeArquivo  = "emails-alterados-eleitores.xlsx";
        
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        
        $sheet->setCellValue('A1', $nome_eleicao->ele_nome);
        
        // Define os cabeçalhos das colunas
        $sheet->setCellValue('A2', 'NOME');
        $sheet->setCellValue('B2', 'E-MAIL ANTIGO');
        $sheet->setCellValue('C2', 'E-MAIL ALTERADO');
        $sheet->setCellValue('D2', 'DATA DE ALTERAÇÃO');
        
        // Mescla as células para o título
        $sheet->mergeCells('A1:D1');
    
        // Define a formatação para o título
        $sheet->getStyle('A1:D1')->getFont()->setBold(true);
        $sheet->getStyle('A1:D1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A1:D1')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $sheet->getStyle('A1:D1')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('d0dfe2');
    
        // Define a formatação para os cabeçalhos das colunas
        $sheet->getStyle('A2:D2')->getFont()->setBold(true);
        $sheet->getStyle('A2:D2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A2:D2')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
    
        // Ajusta a largura das colunas
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
    
        // Preenche os dados
        $linha = 3;
        foreach ($dados as $registro) {
            $logDados = json_decode($registro->log_dadosregistro, true);
            
            $sheet->setCellValue('A' . $linha, $logDados['Nome']);
            $sheet->setCellValue('B' . $linha, $logDados['Email Antigo']);
            $sheet->setCellValue('C' . $linha, $logDados['Email Novo']);
            $sheet->setCellValue('D' . $linha, \Carbon\Carbon::parse($registro->log_data)->format('d/m/Y H:i'));
            $linha++;
        }
        
        // Cria o writer e envia o arquivo para download
        $writer = new Xlsx($spreadsheet);
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment; filename=$nomeArquivo");
        $writer->save("php://output");
        
    }
    
}
