<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Support\Facades\Auth;

//use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\Http\Models\Eleicao as Eleicao;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	//use AuthenticatesAndRegistersUsers;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;

		$this->middleware('guest', ['except' => 'getLogout']);
	}

    public function getRegister()
    {
        return view('auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postRegister(Request $request)
    {
        $validator = $this->registrar->validator($request->all());

        if ($validator->fails())
        {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $this->auth->login($this->registrar->create($request->all()));

        return redirect($this->redirectPath());
    }

    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin()
    {
        return view('auth.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     public function postLogin(Request $request)
     {
         if ($request->ele_id) {
             // Valida os campos para login com 'ele_id'
             $this->validate($request, [
                 'matricula' => 'required', 
                 'password' => 'required', 
                 'ele_id' => 'required'
             ]);
             $credentials = $request->only('matricula', 'password', 'ele_id');
     
             // Remove espaços em branco
             $credentials['password'] = trim($credentials['password']);
             $credentials['matricula'] = trim($credentials['matricula']);
         } else {
             // Valida os campos para login com 'cpf'
             $this->validate($request, [
                 'cpf' => 'required', 
                 'password' => 'required',
             ]);
             $credentials = $request->only('cpf', 'password');
     
             // Remove espaços em branco
             $credentials['cpf'] = trim($credentials['cpf']);
             $credentials['password'] = trim($credentials['password']);
         }
     
         // Tenta autenticar o usuário
         if ($this->auth->attempt($credentials, $request->has('remember'))) {
             $user = $this->auth->user();
             $modelEleicao = new Eleicao();
             $eleicao = $modelEleicao->getById($user->ele_id);
     
             // Verifica se o usuário já tem um session_id e se já passaram 2 minutos
             if ($user->profile_id == 3 && $user->session_id && $user->session_id !== session()->getId()) {
                 $loginTime = strtotime($user->session_time); // Converte o tempo salvo no banco para timestamp
                 $currentTime = time(); // Pega o tempo atual em timestamp
                 $tempoEleicao = $eleicao->ele_tempo; // Exemplo: '00:02'
                 list($hours, $minutes) = explode(':', $tempoEleicao);
                 $totalSeconds = ($hours * 3600) + ($minutes * 60); // Converte para segundos

                 $timePassed = $currentTime - $loginTime;
                 // Verifica se já passaram mais de 2 minutos (120 segundos)
                 if ($timePassed < $totalSeconds) {


                    $remainingTime = $totalSeconds - $timePassed;

                    // Converte o tempo restante para horas e minutos
                    $remainingMinutes = floor($remainingTime / 60);
                    $remainingSeconds = $remainingTime % 60;
            
                    // Monta a mensagem de erro com o tempo restante
                    $remainingTimeFormatted = sprintf('%02d:%02d', $remainingMinutes, $remainingSeconds);
                     // Se o tempo for menor que 2 minutos, desloga a sessão anterior
                     $this->auth->logout();

                     
                     // Redireciona para a página de login com uma mensagem de erro
                     return redirect($this->loginPath())
                         ->withErrors(['message' => 'Esta conta já está logada espere '.$remainingTimeFormatted]);
                 } else {
                     // Se passaram mais de 2 minutos, libera o login
                     $user->session_id = null; // Limpa o session_id para permitir o novo login
                 }
             }
     
             // Atualiza o session_id e o tempo de login
             $user->session_id = session()->getId();
             $user->session_time = date('Y-m-d H:i:s'); // Salva o horário atual
             $user->save();
     
             return redirect()->intended($this->redirectPath()); // Redireciona após login
         }
    
        // Se a autenticação falhar
        if (isset($request->ele_id)) {
            return redirect($this->loginPath())
                ->withInput($request->only('matricula', 'ele_id', 'remember'))
                ->withErrors([
                    'matricula' => $this->getFailedLoginMessage(),
                ]);
        } else {
            return redirect($this->loginPath())
                ->withInput($request->only('cpf', 'remember'))
                ->withErrors([
                    'cpf' => $this->getFailedLoginMessage(),
                ]);
        }
    }
    
    /**
     * Get the failed login message.
     *
     * @return string
     */
    protected function getFailedLoginMessage()
    {
        return 'Dados inválidos.';
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */

    public function getLogout()
    { 
        $user = $this->auth->user();
        $this->auth->logout();
    
        // Remove o token de sessão no banco de dados (opcional, se aplicável)
        if ($user) {
            $user->session_id = null; // Ou um método específico para limpar o token
            $user->save();
        }
    
        if ($user->profile_id == 2 || $user->profile_id == 3) { // Se usuário for "comissão" ou "eleitor"
            $modelEleicao = new Eleicao();
            $eleicao = $modelEleicao->getById($user->ele_id);
            $eleAlias = $eleicao->alias;
            $urlEleicao = url('eleicao')."/$eleAlias";
            
            return redirect($urlEleicao);
        } else {
            return redirect()->action('HomeController@index');
        }
    }

    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectPath()
    {
        if (property_exists($this, 'redirectPath'))
        {
            return $this->redirectPath;
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/home';
    }

    /**
     * Get the path to the login route.
     *
     * @return string
     */
    public function loginPath()
    {
        return property_exists($this, 'loginPath') ? $this->loginPath : $_SERVER['HTTP_REFERER'];
    }
}
