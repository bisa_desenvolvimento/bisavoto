<?php

namespace App\Http\Requests;

use App\Enums\TiposEnviosEleicao;
use App\Library\Functions;
use Illuminate\Foundation\Http\FormRequest;

class EleicaoRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }
    
    public function all($keys = null)
    {
        $data = parent::all($keys);
    
        $data['ele_horaInicio'] = Functions::formatarDataDateTime($data['ele_horaInicio'] ?? '');
        $data['ele_horaTermino'] = Functions::formatarDataDateTime($data['ele_horaTermino'] ?? '');
        $data['ele_tipo_envio'] = $data['ele_tipo_envio'] ?? 'EMAIL';
    
        return $data;
    }
    

    public function rules()
    {
        return [
            'ele_nome' => 'required|string|max:255',
            'ele_descricao' => 'string|max:500',
            'ele_nomenclatura' => 'required|string|max:255',
            'ele_horaInicio' => 'required|date_format:Y-m-d H:i:s',
            'ele_horaTermino' => 'required|date_format:Y-m-d H:i:s|after:ele_horaInicio',
            'ele_tipo' => 'required|integer',
            'ele_tipo_envio' => 'in:EMAIL,SMS,AMBOS',
            'exibir_branco' => 'required|boolean',
            'exibir_nulo' => 'required|boolean',
            'enviar_comprovante' => 'required|boolean',
            'exibir_sequenciaVotos' => 'required|boolean',
            'habilitar_exclusao' => 'required|boolean',
            'aceitar_email_duplicado' => 'required|boolean',
            'habilitar_adicionareleitor' => 'required|boolean',
            'ele_alias' => 'required|string|max:500|unique_alias:' . $this->input('ele_id') . '|valid_alias',
        ];
    }


    public function messages()
    {
        return [
            "ele_horaTermino.after" => "A data de término deve ser posterior à data de início.",
            "ele_tipo_envio.in" => "O tipo de envio deve ser 'E-mail', 'SMS' ou 'Ambos'.",
        ];
    }
}
