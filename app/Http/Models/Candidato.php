<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
class Candidato extends Model {

	//
	protected $table = 'candidato';
	protected $primaryKey = 'cdt_id';
	
    protected $casts = ['cdt_id' => 'int',
    					'car_id' => 'int',
                        'ele_id' => 'int'];

	protected $fillable = array('cdt_nome',
								'car_id',		 
							    'cdt_numeroCandidatura',
							    'cdt_fotocandidato',
                                'ele_id');

    public function listaCandidatos($idEleicao, $idCargo)
    {

        $list = Candidato::orderBy('cdt_id')
            ->where('ele_id', $idEleicao)
            ->where('car_id', $idCargo)
            ->get();

        return $list;

    }

    public function get($idEleicao, $idCargo, $idCandidato)
    {
        $list = Candidato::where('ele_id', $idEleicao)
        				 ->Where('car_id',$idCargo)
        				 ->where('cdt_id', $idCandidato)
        				 ->get();

        return $list;

    }

    public function getCargo()
    {
        return $this->hasOne('\App\Http\Models\Cargo', 'car_id', 'car_id');
    }

}
