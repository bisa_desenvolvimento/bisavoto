<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Eleitor extends Model {

	//
	protected $table = 'users';
	protected $primaryKey = 'id';
	
    protected $casts = ['id' => 'int',
                        'ele_id' => 'int',
                        'profile_id'=> 'int',
                        'zona_id'=>'int',
                        'enviado'=>'boolean'];

	protected $fillable = array('name',		 
							    'car_votos',
                                'ele_id',
                                'profile_id',
                                'zona_id',
                                'matricula',
                                'enviado',
                                'email',
                                'data_envio',
                                'opcaoSenha');

	public function get($idEleicao, $idEleitor)
    {
        $list = Eleitor::where('id', $idEleitor)
        			   ->Where('ele_id',$idEleicao)->get();

        return $list;
    }

    public function getByEmailAndEleId($email, $eleId)
    {
        return Eleitor::where('email', $email)->where("ele_id", $eleId)->first();
    }

  public function inserirEleitorImportacao($arrInserirEleitorImportacao)
  {
       Eleitor::insert($arrInserirEleitorImportacao);  
  }
    

    public function existeEleitorEleicaoZona($idEleicao,
                                               $numZona, 
                                               $matricula)
    {         
        $idEleicao = trim($idEleicao);
        $numZona   = trim("".$numZona."");
        $matricula = trim($matricula);

        $retorno = DB::select("SELECT 
                                   COUNT(users.id) AS total
                               FROM 
                                   users 
                               WHERE 
                                   users.profile_id = 3
                               AND 
                                   users.ele_id = :ele_id
                               AND 
                                   users.matricula  = :matricula
                               AND 
                                  users.zona_id IN (SELECT zona.zon_id FROM zona WHERE zona.zon_id = :zon_num)", 
                                
                                ['ele_id' => $idEleicao, 
                                 'matricula'=>$matricula, 
                                 'zon_num'=>$numZona ]);



        return $retorno[0]->total;                    
    }


    public function existeZona($idEleicao,$numZona)
    {         
        $idEleicao = trim($idEleicao);
        $numZona   = trim("".$numZona."");

        $retorno = DB::select("SELECT 
                                   COUNT(zona.zon_id) AS total
                               FROM 
                                   zona 
                               WHERE 
                                  zona.ele_id = :ele_id
                               AND 
                                  zona.zon_numero = :zon_num", 
                                
                                ['ele_id' => $idEleicao, 
                                 'zon_num'=>$numZona ]);

        return $retorno[0]->total;                    
    }

    public function pegarZonaId($idEleicao,$numZona)
    {         
        $idEleicao = trim($idEleicao);
        $numZona   = trim("".$numZona."");

        $retorno = DB::select("SELECT 
                                   zona.zon_id AS id
                               FROM 
                                   zona 
                               WHERE 
                                  zona.ele_id = :ele_id
                               AND 
                                  zona.zon_numero = :zon_num", 
                                
                                ['ele_id' => $idEleicao, 
                                 'zon_num'=>$numZona ]);

        return $retorno[0]->id;                    
    }


    public function getForSend($ele_id = null)
    {
        if ($this->profile_id == 2){
            $ele_id = $this->ele_id;
        }

        $listaEleitores = DB::table('users')
            ->where('ele_id', '=', $ele_id)
            ->where('profile_id', '=', 3)
            ->where('enviado', '=', 0)
            ->orderBy('name')
            ->get();

        return $listaEleitores;
    }

    public function lastSend($ele_id = null)
    {
        if ($this->profile_id == 2){
            $ele_id = $this->ele_id;
        }

        $last = DB::table('users')
            ->where('enviado', '=', true)
            ->where('ele_id', '=', $ele_id)
            ->where('profile_id', '=', 3)
            ->orderBy('data_envio', 'DESC')
            ->get();
        return $last[0]->data_envio;
    }

    public function eleitorPorLetra($letra, $ele_id)
    {
        $listaEleitores = DB::select("select * from users where ele_id = :ele_id and profile_id = 3 and password = '' and LCASE(SUBSTR(name, 1, 1)) = :letra order by name asc",
            ['ele_id' => $ele_id,
             'letra'  => $letra]);
        return $listaEleitores;
    }

    public function eleitoresPorIdEleicao($ele_id)
    {
        return DB::select("
        select * from users where ele_id = :ele_id and 
        profile_id = 3 and password = ''
        LIMIT 10",
        ['ele_id' => $ele_id]);
    }

    public function eleitoresGeralPorIdEleicao($ele_id)
    {
        return DB::select("
        select * from users where ele_id = :ele_id and 
        profile_id = 3 and password = '' ",
        ['ele_id' => $ele_id]);
    }

    public function eleitoresNaoNotificados($ele_id)
    {
        return DB::select("
        select nome, email from users where ele_id = :ele_id and 
        profile_id = 3 and password = ''",
        ['ele_id' => $ele_id]);
    }

    public function percentualDeEleitoresDeNotificados($ele_id){
        $totalDeEleitores = count(DB::select("
        select * from users where ele_id = :ele_id and profile_id = 3",
        ['ele_id' => $ele_id]));

        $totalDeNotificados = count(DB::select("
        select * from users where ele_id = :ele_id and profile_id = 3 and password <> ''",
        ['ele_id' => $ele_id]));

        return $totalDeNotificados/$totalDeEleitores;
    }

}
