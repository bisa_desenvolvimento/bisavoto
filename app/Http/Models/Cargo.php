<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Cargo extends Model {

	//
	protected $table = 'cargo';
	protected $primaryKey = 'car_id';
	
    protected $casts = ['car_id' => 'int',
                        'ele_id' => 'int'];

	protected $fillable = array('car_nome',		 
							    'car_votos',
                                'ele_id',
                                'car_restricao_zona',
                                'car_zona_id');

    public function listCargos($ele_id = null){

        $list = Cargo::orderBy('car_ordem')->where('ele_id', $ele_id)->get();

        return $list;

    }

    public function listaCargosComVotacao($ele_id) {
        $dados = DB::select("SELECT cg.car_id, cg.car_nome
                                FROM candidato c 
                                LEFT JOIN voto v ON c.cdt_id = v.cdt_id 
                                INNER JOIN cargo cg ON cg.car_id = c.car_id
                                INNER JOIN zona z ON z.zon_id = v.zon_id
                            WHERE c.ele_id = :ele_id AND c.cdt_id NOT IN (98, 99)
                            GROUP BY cg.car_id
                            ORDER BY z.zon_nome ASC",
                            ['ele_id' => $ele_id]);
        return $dados;
    }

    public function get($idCargo, $idEleicao)
    {
        $list = Cargo::where('car_id', $idCargo)->Where('ele_id',$idEleicao)->get();

        return $list;
    }

    public function updateData($valoresForm)
    {
        $id = $valoresForm['id'];
        $car_nome = $valoresForm['car_nome'];
        $car_votos = $valoresForm['car_votos'];
        $car_restricao_zona = $valoresForm['car_restricao_zona'];
        $car_zona_id = $valoresForm['car_zona_id'];

        $cargo = Cargo::where('car_id', '=', $id)->firstOrFail();
        $cargo->car_nome = $car_nome;
        $cargo->car_votos = $car_votos;
        $cargo->car_restricao_zona = $car_restricao_zona;
        $cargo->car_zona_id = $car_zona_id;
        return $cargo->save();

    }

    public function updateOrdemCargo($valores)
    {
        $valores = explode("&",$valores["ordem"]);

        $tam = count($valores);

        for ($i=0; $i < $tam; $i++) 
        { 
            $idEleicao = substr($valores[$i],11,2);
            $idCargo   = substr($valores[$i],-2);

            Cargo::where('ele_id', $idEleicao)
                    ->where('car_id', $idCargo)
                    ->update(['car_ordem' => $i + 1]);

        }
    }

    public static function getEleicaoByCargo($id)
    {
        return Cargo::orderby('eleicao.alias')
        ->join('eleicao', 'cargo.ele_id', '=', 'eleicao.ele_id')
        ->where('cargo.car_id', $id)
        ->first();
    }

}
