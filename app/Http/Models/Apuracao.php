<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Apuracao extends Model {

    protected $table = 'candidato';
    protected $primaryKey = 'cdt_id';

    protected $fillable = array('cdt_nome',
        'car_id',
        'cdt_numeroCandidatura',
        'cdt_fotocandidato',
        'zon_nome');

    public function resultado($idEleicao){
        $list = Apuracao::orderby('votos DESC')
                ->join('voto', 'candidato.cdt_id', '=', 'voto.cdt_id')
                ->where('voto.ele_id', $idEleicao)
                ->groupby('zona.zon_id')
                ->get();
    }
}
