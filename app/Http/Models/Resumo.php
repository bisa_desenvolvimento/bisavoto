<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Resumo extends Model {

    public function lista($idEleicao)
    {
        $dados = DB::select("SELECT
                             COUNT(u.id) AS eleitores,
                             z.zon_nome AS zona,
                             (SELECT COUNT(v.zon_id) FROM voto v WHERE v.zon_id = u.zona_id AND v.cdt_id NOT IN (98, 99) ) AS votos
                             FROM users u
                             RIGHT JOIN zona z ON u.zona_id = z.zon_id
                             WHERE u.profile_id = 3
                             AND u.ele_id = :ele_id
                             GROUP BY u.zona_id",
            ['ele_id' => $idEleicao]);
        return $dados;
    }

    public function listaEleicao($idEleicao)
    {
        $dados = DB::select("SELECT 
                              c.cdt_nome,
                              c.cdt_numeroCandidatura,
                              c.cdt_fotocandidato,
                              c.car_id,
                              UPPER(cg.car_nome) AS car_nome,
                              CASE WHEN COUNT(v.cdt_id) > 0 THEN COUNT(v.cdt_id) ELSE 0 END AS votos
                            FROM
                              candidato c 
                            LEFT JOIN voto v 
                                ON c.cdt_id = v.cdt_id
                            INNER JOIN cargo cg
                                ON cg.car_id = c.car_id
                            WHERE c.ele_id = :ele_id
                              AND c.cdt_numeroCandidatura NOT IN (98, 99) 
                            GROUP BY c.car_id, c.cdt_id 
                            ORDER BY c.car_id, votos desc",
            ['ele_id' => $idEleicao]);

        return $dados;
    }

    public function apuracao($idEleicao)
    {
        $branco = $this->getBranco($idEleicao);
        $nullo = $this->getBranco($idEleicao);
        return null;
    }

    public function getBranco($idEleicao, $idCargo)
    {
        $dados = DB::select("SELECT
                                   count(v.cdt_id) AS votos
                            FROM candidato c
                            LEFT JOIN voto v ON c.cdt_id = v.cdt_id
                            WHERE v.ele_id = :ele_id
                              AND c.cdt_numeroCandidatura = 99
                              AND v.car_id = :car_id",
                ['ele_id' => $idEleicao, 'car_id' => $idCargo]);

        $branco = new \stdClass();
        $branco->votos = $dados[0]->votos;   
        $branco->cdt_nome = 'Branco';   
        $branco->cdt_numeroCandidatura = '99';   
        $branco->cdt_fotocandidato = '';   
        
        return $branco;
    }

    public function getNulo($idEleicao, $idCargo)
    {
        $dados = DB::select("SELECT
                                   count(v.cdt_id) AS votos
                            FROM candidato c
                            LEFT JOIN voto v ON c.cdt_id = v.cdt_id
                            WHERE v.ele_id = :ele_id
                              AND c.cdt_numeroCandidatura = 98
                              AND v.car_id = :car_id",
            ['ele_id' => $idEleicao, 'car_id' => $idCargo]);
        
        $nulo = new \stdClass();
        $nulo->votos = $dados[0]->votos;   
        $nulo->cdt_nome = 'Nulo';   
        $nulo->cdt_numeroCandidatura = '98';   
        $nulo->cdt_fotocandidato = '';   
        
        return $nulo;
    }

    public function getBrancoZona($idEleicao, $idZona, $idCargo)
    {
        $dados = DB::select("SELECT
                                   count(v.cdt_id) AS votos
                            FROM candidato c
                            LEFT JOIN voto v ON c.cdt_id = v.cdt_id
                            WHERE v.ele_id = :ele_id
                              AND c.cdt_numeroCandidatura = 99
                              AND v.car_id = :car_id
                              AND v.zon_id = :zon_id",
                ['ele_id' => $idEleicao, 'zon_id' => $idZona, 'car_id' => $idCargo]);

        $branco = new \stdClass();
        $branco->votos = $dados[0]->votos;   
        $branco->cdt_nome = 'Branco';   
        $branco->cdt_numeroCandidatura = '99';   
        $branco->cdt_fotocandidato = '';   
        
        return $branco;
    }

    public function getNuloZona($idEleicao, $idZona, $idCargo)
    {
        $dados = DB::select("SELECT
                                   count(v.cdt_id) AS votos
                            FROM candidato c
                            LEFT JOIN voto v ON c.cdt_id = v.cdt_id
                            WHERE v.ele_id = :ele_id
                              AND c.cdt_numeroCandidatura = 98
                              AND v.car_id = :car_id
                              AND v.zon_id = :zon_id",
            ['ele_id' => $idEleicao, 'zon_id' => $idZona, 'car_id' => $idCargo]);
        
        $nulo = new \stdClass();
        $nulo->votos = $dados[0]->votos;   
        $nulo->cdt_nome = 'Nulo';   
        $nulo->cdt_numeroCandidatura = '98';   
        $nulo->cdt_fotocandidato = '';   
        
        return $nulo;
    }


    public function listarAptos($idEleicao)
    {
      $dados = DB::select("SELECT 
                            `name` AS nome,
                            matricula,
                            email
                          FROM
                            users 
                          WHERE users.profile_id = 3
                          AND users.ele_id = :ele_id
                          ORDER BY users.name",
                          ['ele_id' => $idEleicao]);
      return $dados;
    }

    
    public function listarAptosPorZona($idEleicao, $idZona)
    {
      $dados = DB::select("SELECT 
                            `name` AS nome,
                            matricula,
                            email
                          FROM
                            users 
                          WHERE users.profile_id = 3
                          AND users.ele_id = :ele_id
                          AND users.zona_id = :zona_id
                          ORDER BY users.name",
                          ['ele_id' => $idEleicao, 'zona_id' => $idZona]);
      return $dados;
    }

    public function listarVotantes($idEleicao)
    {
      $dados = DB::select("SELECT 
                            `name` AS nome,
                            matricula,
                            email,
                            zona.zon_nome as 'zona'
                          FROM
                            users 

                          INNER JOIN zona
                          ON zona.zon_id = users.zona_id

                          WHERE users.profile_id = 3
                          AND users.ele_id = :ele_id
                          AND vote = 1
                          ORDER BY users.`name`",
                          ['ele_id' => $idEleicao]);
      return $dados;
    }

    public function listarVotantesPorZona($idEleicao)
    {
      $dados = DB::select("SELECT 
                            users.`name` AS 'Nome',
                            zona.`zon_nome` AS 'Zona'

                            FROM users

                            INNER JOIN zona
                            ON zona.`zon_id` = users.`zona_id`

                            WHERE users.`ele_id` = :ele_id 
                            AND vote = 1
                            AND users.profile_id = 3 

                            ORDER BY zona.`zon_numero`, users.name",
                            ['ele_id' => $idEleicao]);
      return $dados;
    }

    public function resultadoEleicaoPorZona($idEleicao)
    {
      $dados = DB::select("SELECT 
                            c.cdt_nome,
                            c.cdt_numeroCandidatura,
                            c.car_id,
                            v.zon_id,
                            z.zon_nome,
                            UPPER(cg.car_nome) AS car_nome,
                            CASE
                              WHEN COUNT(v.cdt_id) > 0 
                              THEN COUNT(v.cdt_id) 
                              ELSE 0 
                            END AS votos 
                          FROM
                            candidato c 
                            LEFT JOIN voto v 
                              ON c.cdt_id = v.cdt_id 
                            INNER JOIN cargo cg 
                              ON cg.car_id = c.car_id
                            INNER JOIN zona z
                            ON z.zon_id = v.zon_id
                          WHERE c.ele_id = :ele_id
                            AND c.cdt_id NOT IN (98, 99) 
                          GROUP BY v.zon_id,
                            c.car_id,
                            c.cdt_id 
                          ORDER BY c.car_id,
                            votos DESC",
                          ['ele_id' => $idEleicao]);
      return $dados;
    }

    
    public function resultadoEleicaoPorZona2($idEleicao, $idCargo, $idZona)
    {
      $dados = DB::select("SELECT c.cdt_nome, c.cdt_numeroCandidatura, c.car_id, z.zon_id, z.zon_nome,
                            UPPER(cg.car_nome) AS car_nome,
                            CASE
                              WHEN COUNT(v.cdt_id) > 0 
                              THEN COUNT(v.cdt_id) 
                              ELSE 0 
                            END AS votos  
                          FROM
                            candidato c 
                            LEFT JOIN voto v ON c.cdt_id = v.cdt_id AND v.zon_id = :zon_id
                            LEFT JOIN zona z ON z.zon_id = v.zon_id
                            INNER JOIN cargo cg ON cg.car_id = c.car_id
                          WHERE c.ele_id = :ele_id AND c.cdt_id NOT IN (98, 99) AND c.car_id = :car_id
                          GROUP BY c.cdt_nome, c.cdt_numeroCandidatura, c.car_id, z.zon_id, z.zon_nome, cg.car_nome
                          ORDER BY c.car_id, votos DESC",
                          ['ele_id' => $idEleicao, 'car_id' => $idCargo, 'zon_id' => $idZona]);
                          
      return $dados;
    }



}
