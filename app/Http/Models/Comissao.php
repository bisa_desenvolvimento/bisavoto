<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Comissao extends Model {

    protected $table = 'users';
    protected $primaryKey = 'id';

    protected $casts = ['id' => 'int',
                        'ele_id' => 'int', 
                        'profile_id' => 'int'];

    protected $fillable = array('id', 'name', 'login', 'password', 'email', 'ele_id', 'profile_id');

    public function verificarLoginExiste($objComissao)
    {
        if($objComissao->id == '')
        {
             $retorno = Comissao::where('login', '=', $objComissao->login)
                            ->where('ele_id', '=' , $objComissao->ele_id)
                            ->where('profile_id', '=' , $objComissao->profile_id)
                            ->exists();
        }
        else
        {
            $retorno = Comissao::where('login', '=', $objComissao->login)
                            ->where('ele_id', '=' , $objComissao->ele_id)
                            ->where('profile_id', '=' , $objComissao->profile_id)
                            ->where('id', '<>', $objComissao->id)
                            ->exists();   
        }
                            
        return $retorno;
    }    

    public function get($idEleicao, $idComissao)
    {
        $list = Comissao::where('id', $idComissao)
                       ->Where('ele_id',$idEleicao)
                       ->where('profile_id',2)
                       ->get();

        return $list;
    }

    public function listComissao($ele_id = null){
        $list = Comissao::orderBy('name')
                ->where('ele_id', $ele_id)
                ->where('profile_id', 2)
                ->get();

        return $list;
    }
}
