<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Endereco extends Model {

	//
	protected $table = 'endereco';
	protected $primaryKey = 'id';
	
    protected $casts = ['id' => 'int',
                        'users_id' => 'int'];

	protected $fillable = array('name',		 
							    'car_votos',
                                'ele_id',
								'telefone');

	public function get($idEleitor)
	{
		$list = Endereco::where('users_id', $idEleitor)
        			     ->get();

        return $list;
	}

    public function inserirEleitorEndereco($dadosEndereco)
    {
        Endereco::insert($dadosEndereco);  
    }

}
