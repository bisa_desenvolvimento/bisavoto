<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
class Log extends Model {

    protected $table = 'log';
    protected $primaryKey = 'Log_Id';


  public function salvar($operacao, $idRotina, $idUsuario, $dados='', $termo = null) {
          //echo "<pre>"; var_dump($operacao,$idRotina,$dados);exit();

        $usuario = $idUsuario;
        $dadosAdicionais = $_SERVER["REMOTE_ADDR"] . "|" . $_SERVER["HTTP_USER_AGENT"];
        $dadosAdicionais = str_replace("'","\'",$dadosAdicionais);
        $cliente = $_SERVER["HTTP_HOST"];
        $sistema = 'bisavoto';

        $dados = json_encode($dados, JSON_UNESCAPED_UNICODE);
        $dados = str_replace("'","\'",$dados);

        $dadosInsert = array(
                  'Log_Usuario' => $usuario,
                  'Log_Operacao' => $operacao,
                  'Log_IdRegistro' => $idRotina,
                  'Log_DadosRegistro' => $dados,
                  'Log_DadosAdicionais' => $dadosAdicionais,
                  'Log_Cliente' => $cliente,
                  'Log_Sistema' => $sistema,
                  'Log_Termo' => $termo);

         Log::insert($dadosInsert); 

  }

  static private function consultaLog(){

  }

  public function consultarLogEmailsAlterados($ele_id)
  {

    $result = DB::table('log')->select('log_dadosregistro', 'log_data')->where('log_idregistro', $ele_id)->where('log_operacao', 'RESUMO_MODEL_RESUMO::ALTERAÇÂO DE EMAIL')->get();

    return $result;
  }


}
