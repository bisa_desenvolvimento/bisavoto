<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Zonas extends Model {

    //
    protected $table = 'zona';
    protected $primaryKey = 'zon_id';

    protected $fillable = array('zon_nome',
                                'zon_numero',
                                'ele_id');

    public function listZonas($ele_id){

        $list = Zonas::orderBy('zon_nome')->where('ele_id', $ele_id)->get();

        return $list;

    }

    public function listaZonasComVotacao($ele_id, $idCargo) {
        $dados = DB::select("SELECT z.zon_id, z.zon_nome, cg.car_id, cg.car_nome
                                FROM candidato c 
                                LEFT JOIN voto v ON c.cdt_id = v.cdt_id 
                                INNER JOIN cargo cg ON cg.car_id = c.car_id
                                INNER JOIN zona z ON z.zon_id = v.zon_id
                            WHERE c.ele_id = :ele_id AND c.cdt_id NOT IN (98, 99) and cg.car_id = :car_id
                            GROUP BY v.zon_id,  cg.car_id
                            ORDER BY z.zon_nome ASC",
                            ['ele_id' => $ele_id, 'car_id' => $idCargo]);
        return $dados;
    }

    public function get($id)
    {
        $list = Zonas::where('zon_id', $id)->get();

        return $list;

    }

}
