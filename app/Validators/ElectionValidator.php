<?php 
namespace App\Validators;

use App\Library\Functions;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ElectionValidator
{
    public static function register()
    {
        Validator::extend('valid_alias', function ($attribute, $value, $parameters, $validator) {
			return Functions::isValidAlias($value);
		});

        Validator::extend('unique_alias', function ($attribute, $value, $parameters, $validator) {
            $eleId = $parameters[0] ?? null;

            $query = DB::table('eleicao')->where('alias', $value);

            if (!is_null($eleId)) {
                $query->where('ele_id', '<>', $eleId);
            }

            return !$query->exists();
        });
    }
}
