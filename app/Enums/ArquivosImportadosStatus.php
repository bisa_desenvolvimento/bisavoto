<?php

namespace App\Enums;

class ArquivosImportadosStatus {
    const AGUARDANDO = 'aguardando';
    const ERRO = 'erro';
    const PROCESSANDO = 'processando';
    const SUCESSO = 'sucesso';
}