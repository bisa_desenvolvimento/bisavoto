<?php

namespace App;

use App\Enums\ArquivosImportadosStatus;
use App\Http\Models\Eleicao;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ArquivoImportado extends Model
{
    protected $table = 'arquivos_importados';

    protected $fillable = [
        "nome",
        "caminho",
        "eleicao_id",
        "status",
        "error_payload"
    ];

    public function getStatusLabel(): string
    {
        switch ($this->status) {
            case ArquivosImportadosStatus::SUCESSO:
                return "Sucesso";
            case ArquivosImportadosStatus::ERRO:
                return "Erro";
            case ArquivosImportadosStatus::PROCESSANDO:
                return "Processando";
            case ArquivosImportadosStatus::AGUARDANDO:
                return "Aguardando";
            default:
                return "";
        };
    }

    public function getStatusLabelColor(): string
    {
        switch ($this->status) {
            case ArquivosImportadosStatus::SUCESSO:
                return "success";
            case ArquivosImportadosStatus::ERRO:
                return "danger";
            case ArquivosImportadosStatus::PROCESSANDO:
                return "warning";
            case ArquivosImportadosStatus::AGUARDANDO:
                return "info";
            default:
                return "";
        };
    }

    public function eleicao(): BelongsTo
    {
        return $this->belongsTo(Eleicao::class, 'eleicao_id');
    }
}
