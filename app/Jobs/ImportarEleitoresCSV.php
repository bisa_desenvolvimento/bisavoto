<?php

namespace App\Jobs;

use App\ArquivoImportado;
use App\Enums\ArquivosImportadosStatus;
use App\Http\Models\Eleicao;
use App\Http\Models\Eleitor;
use Exception;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use InvalidArgumentException;

class ImportarEleitoresCSV implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels, DispatchesJobs;

    CONST TTL_CACHE = 10;
    const SEPARATOR = ';';
    const COLUMN_ZONA = 0;
    const COLUMN_NOME = 1;
    const COLUMN_CPF = 2;
    const COLUMN_MATRICULA = 3;
    const COLUMN_EMAIL = 4;
    const COLUMN_TELEFONE = 5;

    private $arquivo;
    private $eleicao;

    private array $errors = [];
    private int $countDuplicateEmail = 0;
    private int $idArquivo;
    private int $idEleicao;


    public $tries = 3;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        int $idArquivo,
        int $idEleicao
    ) {
        $this->idEleicao = $idEleicao;
        $this->idArquivo = $idArquivo;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $eleitores = array();
            $this->arquivo = ArquivoImportado::find($this->idArquivo);
            $this->eleicao = Eleicao::find($this->idEleicao);

            $this->arquivo->update(['status' => ArquivosImportadosStatus::PROCESSANDO]);

            foreach ($this->getFileData() as $index => $row) {
                $rowData = str_getcsv(trim($row), static::SEPARATOR);

                if (empty($rowData[static::COLUMN_ZONA])) {
                    continue;
                }

                $this->validateRow($rowData, $index);

                $eleitores[] = $this->getEleitorData($rowData);
            }

            if (!empty($this->errors)) {
                throw new InvalidArgumentException(json_encode($this->errors));
            }

            DB::transaction(function () use ($eleitores) {
                foreach ($eleitores as $eleitor) {
                    // Verifica se eleitor já foi importado na eleição
                    $existingEleitor = Eleitor::where('cpf', $eleitor['cpf'])
                                                ->where('ele_id', $eleitor['ele_id'])
                                                ->first();

                    // Se não foi importado, insere o novo eleitor
                    if (!$existingEleitor) {
                        Eleitor::insert($eleitor);
                    }
                }
                
                $this->arquivo->update(['status' => ArquivosImportadosStatus::SUCESSO]);
            });
        } catch (FileNotFoundException $e) {
            $this->saveErrorPayload(['Arquivo não encontrado']);
        } catch (Exception $e) {
            $this->saveErrorPayload([$e->getMessage()]);
        }
    }

    private function getFileData(): array
    {
        $fileData = explode("\n", Storage::get($this->arquivo->caminho));

        if (!$fileData) {
            throw new InvalidArgumentException('Arquivo vazio');
        }

        /** Remove o cabeçalho */
        unset($fileData[0]);

        return $fileData;
    }

    private function validateRow(array $row, int $index): void
    {
        $this->hasEmptyFields($row, $index);
        $this->validateZone($row[static::COLUMN_ZONA], $index);
        $this->verifyIsValidEmail($row[static::COLUMN_EMAIL], $index);
        $this->verifyIsValidPhone($row[static::COLUMN_TELEFONE], $index);
    }

    private function validateZone($zoneNumber, $index): void
    {
        $cacheIndex = "verify_zone_exists_" . $this->idEleicao . "_" . $zoneNumber;

        $hasZone = Cache::remember($cacheIndex, static::TTL_CACHE, function() use ($zoneNumber) {
            return $this->eleicao->verifyZoneExists($zoneNumber);
        });

        if (!$hasZone) {
            $this->errors[] = 'linha: '. ($index + 1). ', erro: A zona informada não é válida.';
        }
    }

    private function hasEmptyFields(array $row, int $index): void
    {
        if (
            empty($row[static::COLUMN_ZONA]) ||
            empty($row[static::COLUMN_NOME]) ||
            empty($row[static::COLUMN_CPF]) ||
            empty($row[static::COLUMN_MATRICULA]) ||
            empty($row[static::COLUMN_EMAIL]) 
        ) {
            $this->errors[] = 'linha: ' . ($index + 1) . ', erro: Existe eleitor com campo obrigatório vazio. Corrija o arquivo e tente novamente.';
        }
    }

    private function verifyIsValidEmail(string $email, int $index): void
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->errors[] = 'linha: ' . ($index + 1) . ', erro: O Email informado não é valido.';
        }

        if (!$this->eleicao->isAllowedDuplicateEmail() && $this->isDuplicatedEmail($email)) {
            $this->errors[] = 'linha: ' . ($index + 1) . ', erro: O Email já existe duplicado.';
            $this->countDuplicateEmail++;
        }
    }

    private function isDuplicatedEmail($email): bool
    {
        return (bool) Eleitor::where([
            'email' => $email,
            'ele_id' => $this->eleicao->id
        ])->first();
    }

    private function verifyIsValidPhone(?string $telefone, int $index): void
    {
        if (!empty($telefone) && !preg_match('/^\d{10,11}$/', $telefone)) { // valida apenas se o telefone não estiver vazio
            $this->errors[] = 'linha: ' . ($index + 1) . ', erro: O telefone informado não é válido.';
        }
    }
    
    private function saveErrorPayload($payload): void
    {
        if (is_array($payload)) {
            $payload = json_encode($payload);
        }

        $this->arquivo->update([
            'status' => ArquivosImportadosStatus::ERRO,
            'error_payload' => $payload
        ]);
    }

    private function getEleitorData(array $row): array
    {
        return [
            'zona_id' => $this->getZoneId($row[static::COLUMN_ZONA]),
            'name' => $row[static::COLUMN_NOME],
            'cpf' => $row[static::COLUMN_CPF],
            'matricula' => $row[static::COLUMN_MATRICULA],
            'email' => $row[static::COLUMN_EMAIL],
            'telefone' => $row[static::COLUMN_TELEFONE] ?? null,
            'ele_id' => $this->eleicao->ele_id,
            'profile_id' => 3
        ];
    }

    private function getZoneId($zoneNumber): int
    {
        $cacheIndex = "verify_zone_exists_" . $zoneNumber;

        $zone = Cache::remember($cacheIndex, static::TTL_CACHE, function() use ($zoneNumber) {
            return $this->eleicao->zonas()->where('zon_numero', $zoneNumber)->first();
        });

        return $zone->zon_id;
    }
}
