<?php namespace App;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password', 'cpf', 'vote', 'profile_id', 'ele_id', 'zona_id', 'vot_data_hora'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	public function getZona()
	{
		return $this->hasOne('\App\Http\Models\Zona', 'zon_id', 'zona_id');
	}

    public function verificarCpfDuplicado($cpf, $ele_id)
    {
        $user = User::where('cpf', $cpf)->where('ele_id', $ele_id)->first();
		return $user !== null;
    }

	public function jaVotou($cpf, $ele_id)
    {
        $user = User::where('cpf', $cpf)->where('ele_id', $ele_id)->where('vote', 1)->first();
		return $user !== null;
    }
	public function verificarDuplicadoOuVotou(Request $request)
	{
		$cpf = $request->input('cpf');
		$ele_id = $request->input('ele_id');
	
		$duplicado = User::where('cpf', $cpf)->where('ele_id', $ele_id)->exists();
		$jaVotou = User::where('cpf', $cpf)->where('ele_id', $ele_id)->where('vote', 1)->exists();
		$emailBase = User::where('cpf', $cpf)->value('email');
	
		$response = [];
	
		if ($jaVotou && $duplicado) {
			$response['message'] = "Você já votou nesta eleição. Caso tenha esquecido sua senha, entre em contato com o suporte.";
			$response['status'] = 'error';
		} else if ($duplicado) {
			$response['message'] = "Você já está cadastrado no banco de eleitores. Enviamos para o e-mail " . $emailBase;
			$response['status'] = 'error';
		} else {
			$response['status'] = 'success';
		}
	
		return response()->json($response);
	}

	public function pegarEmailPorCpf($cpf, $ele_id)
    {
        $user = User::where('cpf', $cpf)->where('ele_id', $ele_id)->first();
        return $user ? $user->email : null;
    }
}
